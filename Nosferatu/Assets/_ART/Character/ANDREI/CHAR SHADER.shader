// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "BoW/CHAR SHADER"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		[Header(Translucency)]
		_Translucency("Strength", Range( 0 , 50)) = 1
		_TransNormalDistortion("Normal Distortion", Range( 0 , 1)) = 0.1
		_TransScattering("Scaterring Falloff", Range( 1 , 50)) = 2
		_TransDirect("Direct", Range( 0 , 1)) = 1
		_TransAmbient("Ambient", Range( 0 , 1)) = 0.2
		_TransShadow("Shadow", Range( 0 , 1)) = 0.9
		[HDR]_Color("Color", Color) = (1,1,1,0)
		[NoScaleOffset]_BaseColorA("BaseColor A", 2D) = "white" {}
		[NoScaleOffset]_BaseColorB("BaseColor B", 2D) = "white" {}
		[NoScaleOffset]_RMAOEA("RMAOE A", 2D) = "white" {}
		[NoScaleOffset]_RMAOEB("RMAOE B", 2D) = "white" {}
		_Metal("Metal", Float) = 0
		[NoScaleOffset][Normal]_Normal("Normal", 2D) = "bump" {}
		_Smooth("Smooth", Range( 0 , 1)) = 0
		_GeneralEmission("General Emission", Float) = 1
		[HDR]_EmissiveColor("Emissive Color", Color) = (1,1,1,0)
		_Emissive("Emissive", Float) = 0
		_Transformation("Transformation", Range( 0 , 1)) = 0
		_Noise("Noise", 2D) = "white" {}
		[HDR]_SSS("SSS", Color) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "AlphaTest+0" "IsEmissive" = "true"  }
		Cull Off
		CGPROGRAM
		#include "UnityPBSLighting.cginc"
		#pragma target 3.0
		#pragma surface surf StandardCustom keepalpha addshadow fullforwardshadows exclude_path:deferred 
		struct Input
		{
			float2 uv_texcoord;
		};

		struct SurfaceOutputStandardCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			half3 Translucency;
		};

		uniform sampler2D _Normal;
		uniform float4 _Color;
		uniform sampler2D _BaseColorA;
		uniform sampler2D _BaseColorB;
		uniform float _Transformation;
		uniform sampler2D _Noise;
		uniform float4 _Noise_ST;
		uniform float _GeneralEmission;
		uniform sampler2D _RMAOEA;
		uniform sampler2D _RMAOEB;
		uniform float4 _EmissiveColor;
		uniform float _Emissive;
		uniform float _Metal;
		uniform float _Smooth;
		uniform half _Translucency;
		uniform half _TransNormalDistortion;
		uniform half _TransScattering;
		uniform half _TransDirect;
		uniform half _TransAmbient;
		uniform half _TransShadow;
		uniform float4 _SSS;
		uniform float _Cutoff = 0.5;

		inline half4 LightingStandardCustom(SurfaceOutputStandardCustom s, half3 viewDir, UnityGI gi )
		{
			#if !DIRECTIONAL
			float3 lightAtten = gi.light.color;
			#else
			float3 lightAtten = lerp( _LightColor0.rgb, gi.light.color, _TransShadow );
			#endif
			half3 lightDir = gi.light.dir + s.Normal * _TransNormalDistortion;
			half transVdotL = pow( saturate( dot( viewDir, -lightDir ) ), _TransScattering );
			half3 translucency = lightAtten * (transVdotL * _TransDirect + gi.indirect.diffuse * _TransAmbient) * s.Translucency;
			half4 c = half4( s.Albedo * translucency * _Translucency, 0 );

			SurfaceOutputStandard r;
			r.Albedo = s.Albedo;
			r.Normal = s.Normal;
			r.Emission = s.Emission;
			r.Metallic = s.Metallic;
			r.Smoothness = s.Smoothness;
			r.Occlusion = s.Occlusion;
			r.Alpha = s.Alpha;
			return LightingStandard (r, viewDir, gi) + c;
		}

		inline void LightingStandardCustom_GI(SurfaceOutputStandardCustom s, UnityGIInput data, inout UnityGI gi )
		{
			#if defined(UNITY_PASS_DEFERRED) && UNITY_ENABLE_REFLECTION_BUFFERS
				gi = UnityGlobalIllumination(data, s.Occlusion, s.Normal);
			#else
				UNITY_GLOSSY_ENV_FROM_SURFACE( g, s, data );
				gi = UnityGlobalIllumination( data, s.Occlusion, s.Normal, g );
			#endif
		}

		void surf( Input i , inout SurfaceOutputStandardCustom o )
		{
			float2 uv_Normal2 = i.uv_texcoord;
			float3 NormalVar40 = UnpackNormal( tex2D( _Normal, uv_Normal2 ) );
			o.Normal = NormalVar40;
			float2 uv_BaseColorA1 = i.uv_texcoord;
			float4 tex2DNode1 = tex2D( _BaseColorA, uv_BaseColorA1 );
			float2 uv_BaseColorB20 = i.uv_texcoord;
			float2 uv_Noise = i.uv_texcoord * _Noise_ST.xy + _Noise_ST.zw;
			float clampResult77 = clamp( ( _Transformation + (1.0 + (step( _Transformation , tex2D( _Noise, uv_Noise ).r ) - 0.0) * (_Transformation - 1.0) / (1.0 - 0.0)) ) , 0.0 , 1.0 );
			float NoiseVar54 = clampResult77;
			float4 lerpResult18 = lerp( tex2DNode1 , tex2D( _BaseColorB, uv_BaseColorB20 ) , NoiseVar54);
			float4 BaseColorVar36 = ( _Color * lerpResult18 );
			o.Albedo = BaseColorVar36.rgb;
			float2 uv_RMAOEA3 = i.uv_texcoord;
			float4 tex2DNode3 = tex2D( _RMAOEA, uv_RMAOEA3 );
			float2 uv_RMAOEB19 = i.uv_texcoord;
			float4 tex2DNode19 = tex2D( _RMAOEB, uv_RMAOEB19 );
			float lerpResult24 = lerp( tex2DNode3.a , tex2DNode19.a , NoiseVar54);
			float E31 = lerpResult24;
			float4 EmissiveVar39 = ( ( BaseColorVar36 * _GeneralEmission ) + ( E31 * ( _EmissiveColor * _Emissive ) ) );
			o.Emission = EmissiveVar39.rgb;
			float lerpResult22 = lerp( tex2DNode3.g , tex2DNode19.g , NoiseVar54);
			float M30 = lerpResult22;
			float MetallicVar38 = ( _Metal * M30 );
			o.Metallic = MetallicVar38;
			float lerpResult21 = lerp( tex2DNode3.r , tex2DNode19.r , NoiseVar54);
			float R28 = ( 1.0 - lerpResult21 );
			float RoughnessVar37 = ( R28 * _Smooth );
			o.Smoothness = RoughnessVar37;
			float lerpResult23 = lerp( tex2DNode3.b , tex2DNode19.b , NoiseVar54);
			float AO29 = lerpResult23;
			o.Occlusion = AO29;
			o.Translucency = _SSS.rgb;
			o.Alpha = 1;
			clip( tex2DNode1.a - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16900
492;352;1290;707;1637.681;964.9897;2.964595;True;False
Node;AmplifyShaderEditor.CommentaryNode;78;-2573.175,-311.6498;Float;False;1260.354;556.6603;Comment;7;25;52;70;71;76;77;54;_Noise;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;52;-2488.06,-101.4307;Float;False;Property;_Transformation;Transformation;19;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;25;-2523.175,-25.97651;Float;True;Property;_Noise;Noise;20;0;Create;True;0;0;False;0;e24b2c680edaa90458d31f11544d79ca;e24b2c680edaa90458d31f11544d79ca;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StepOpNode;70;-2186.417,112.0104;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;71;-2031.417,-45.9895;Float;True;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;1;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;76;-1949.105,-261.6498;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;77;-1686.834,-49.21002;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;54;-1555.821,-75.91235;Float;False;NoiseVar;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;27;-718.1862,-868.1771;Float;False;1266.191;512.346;;7;36;20;1;18;8;7;53;_Base Color;1,1,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;20;-657.2968,-605.5477;Float;True;Property;_BaseColorB;BaseColor B;10;1;[NoScaleOffset];Create;True;0;0;False;0;6f6ef4390725be441a7757bf5aa971a4;aa46a153583d19b4abce88cf0fa72fbd;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;51;-1342.227,800.0933;Float;False;1121.066;646.1909;;12;22;23;24;21;3;19;31;29;28;4;30;55;_RMAOE;1,1,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;1;-668.1862,-818.1771;Float;True;Property;_BaseColorA;BaseColor A;9;1;[NoScaleOffset];Create;True;0;0;False;0;aca5971608013c842988d5f1e917121d;d775fd6defaa0de41b5449e1d2f0215d;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;53;-348.7448,-449.9072;Float;False;54;NoiseVar;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;55;-1232.154,1113.191;Float;False;54;NoiseVar;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;19;-1294.448,1245.556;Float;True;Property;_RMAOEB;RMAOE B;12;1;[NoScaleOffset];Create;True;0;0;False;0;2e0c36a6497740648b022ee47ce1d2db;07316c03e95e4d14ab7a16a8698e8960;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;18;-110.3421,-507.0312;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;3;-1287.223,832.3043;Float;True;Property;_RMAOEA;RMAOE A;11;1;[NoScaleOffset];Create;True;0;0;False;0;3f6fe96361ad6384d89d0b8baa04326b;4a0c01943a9e2fb46aaa375c0b7b2862;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;7;-108.8068,-750.3284;Float;False;Property;_Color;Color;8;1;[HDR];Create;True;0;0;False;0;1,1,1,0;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;47;-76.22188,615.1396;Float;False;1085.863;645.4335;Comment;10;16;15;17;6;10;9;5;34;39;46;_Emissive;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;8;147.0045,-498.1457;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;24;-791.2245,1290.284;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;21;-784.8769,850.0932;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;4;-629.1334,881.7302;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;31;-620.3776,1284.063;Float;False;E;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;22;-791.2249,1006.039;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;10;101.4763,1145.573;Float;False;Property;_Emissive;Emissive;18;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;6;-26.22187,922.4208;Float;False;Property;_EmissiveColor;Emissive Color;17;1;[HDR];Create;True;0;0;False;0;1,1,1,0;1,0,0.05638313,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;36;320.6801,-497.7165;Float;False;BaseColorVar;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;9;297.6134,980.9429;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;46;141.7758,665.1396;Float;False;36;BaseColorVar;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;16;151.3867,740.9348;Float;False;Property;_GeneralEmission;General Emission;16;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;28;-472.8899,868.8569;Float;False;R;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;34;296.5436,853.6878;Float;False;31;E;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;48;-76.38423,260.1481;Float;False;642.3148;234.0155;Comment;4;14;13;33;37;_Smooth;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;30;-464.1607,998.95;Float;False;M;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;49;-89.44841,-116.271;Float;False;722.5142;233.7498;;4;12;11;32;38;_Metal;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;13;-61.68859,418.2469;Float;False;Property;_Smooth;Smooth;15;0;Create;True;0;0;False;0;0;0.7;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;32;-39.44841,2.478846;Float;False;30;M;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;12;-21.80066,-66.27098;Float;False;Property;_Metal;Metal;13;0;Create;True;0;0;False;0;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;33;-26.38425,310.148;Float;False;28;R;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;50;-799.9008,-131.1155;Float;False;586.5419;280;;2;2;40;_Normal;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;15;381.0503,721.1782;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;5;477.2129,859.3146;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;23;-791.2249,1151.935;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;2;-749.9008,-81.11546;Float;True;Property;_Normal;Normal;14;2;[NoScaleOffset];[Normal];Create;True;0;0;False;0;b26cd02789724a04aa55d8734d031e2b;5453b0c39c121504e8b007c63880c947;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;14;198.7626,316.7997;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;17;650.2263,782.916;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;11;233.6785,-34.60056;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;40;-456.3587,-66.535;Float;False;NormalVar;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;39;766.6412,905.3598;Float;False;EmissiveVar;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;29;-622.1271,1153.457;Float;False;AO;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;38;390.0656,-38.1277;Float;False;MetallicVar;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;37;318.9306,354.392;Float;False;RoughnessVar;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;35;1225.268,305.3416;Float;False;29;AO;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;44;1207.201,163.0648;Float;False;38;MetallicVar;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;79;1155.843,447.6276;Float;False;Property;_SSS;SSS;21;1;[HDR];Create;True;0;0;False;0;0,0,0,0;5.992157,0,0.6588235,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;43;1178.926,-56.6955;Float;False;36;BaseColorVar;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;45;1186.201,235.0648;Float;False;37;RoughnessVar;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;42;1194.926,95.30448;Float;False;39;EmissiveVar;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;41;1203.837,24.74604;Float;False;40;NormalVar;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1548.658,52.86222;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;BoW/CHAR SHADER;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Masked;0.5;True;True;0;False;TransparentCutout;;AlphaTest;ForwardOnly;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;70;0;52;0
WireConnection;70;1;25;1
WireConnection;71;0;70;0
WireConnection;71;4;52;0
WireConnection;76;0;52;0
WireConnection;76;1;71;0
WireConnection;77;0;76;0
WireConnection;54;0;77;0
WireConnection;18;0;1;0
WireConnection;18;1;20;0
WireConnection;18;2;53;0
WireConnection;8;0;7;0
WireConnection;8;1;18;0
WireConnection;24;0;3;4
WireConnection;24;1;19;4
WireConnection;24;2;55;0
WireConnection;21;0;3;1
WireConnection;21;1;19;1
WireConnection;21;2;55;0
WireConnection;4;0;21;0
WireConnection;31;0;24;0
WireConnection;22;0;3;2
WireConnection;22;1;19;2
WireConnection;22;2;55;0
WireConnection;36;0;8;0
WireConnection;9;0;6;0
WireConnection;9;1;10;0
WireConnection;28;0;4;0
WireConnection;30;0;22;0
WireConnection;15;0;46;0
WireConnection;15;1;16;0
WireConnection;5;0;34;0
WireConnection;5;1;9;0
WireConnection;23;0;3;3
WireConnection;23;1;19;3
WireConnection;23;2;55;0
WireConnection;14;0;33;0
WireConnection;14;1;13;0
WireConnection;17;0;15;0
WireConnection;17;1;5;0
WireConnection;11;0;12;0
WireConnection;11;1;32;0
WireConnection;40;0;2;0
WireConnection;39;0;17;0
WireConnection;29;0;23;0
WireConnection;38;0;11;0
WireConnection;37;0;14;0
WireConnection;0;0;43;0
WireConnection;0;1;41;0
WireConnection;0;2;42;0
WireConnection;0;3;44;0
WireConnection;0;4;45;0
WireConnection;0;5;35;0
WireConnection;0;7;79;0
WireConnection;0;10;1;4
ASEEND*/
//CHKSM=950C7464BF561C34DC8A2D7EAB62238BA0400B74