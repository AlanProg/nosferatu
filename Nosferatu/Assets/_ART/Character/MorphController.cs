﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[ExecuteInEditMode]
[RequireComponent(typeof(SkinnedMeshRenderer))]
public class MorphController : MonoBehaviour
{
    [Range(0, 1.0f)]
    public float value;

    [Header("Blend Shapes")]
    [SerializeField]
    private SkinnedMeshRenderer skinMesh;
    
    [SerializeField]
    private int[] blendShapeIndex;

    //[Header("Material")]
    //[SerializeField]
    //private Material mat;


    void Start()
    {
        if (skinMesh == null || blendShapeIndex == null)
        {
            Debug.LogError("TA FALTANDO ALGO AQUI, AMIGO");
            return;
        }
        
    }

    private void Reset()
    {
        if (skinMesh ==null)
        {
            skinMesh = GetComponent<SkinnedMeshRenderer>();

            Mesh m = skinMesh.sharedMesh;

            blendShapeIndex = new int[m.blendShapeCount];

            for (int i = 0; i < blendShapeIndex.Length; i++)
            {
                blendShapeIndex[i] = i;
            }

           // mat = GetComponent<Material>();
        }

        
    }

    void Update()
    {
        for (int i = 0; i < blendShapeIndex.Length; i++)
        {
            skinMesh.SetBlendShapeWeight(i, value*100);
        }

    }


}
