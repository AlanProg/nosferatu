﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionUI : MonoBehaviour
{

    public Canvas canvas_Interactive;
    public float size = 1;
    private bool avaible = true;

    [HeaderAttribute("Animation Curve")]
    public AnimationCurve animationCurve;
    private Vector3 initialScale;
    private Vector3 finalScale;
    private float graphValue;

    private void Awake()
    {
        initialScale = transform.localScale;
        finalScale = Vector3.one;
        animationCurve.postWrapMode = WrapMode.PingPong;
    }

    void Start()
    {
        canvas_Interactive.enabled = false;
    }

   

    void Update()
    {

        graphValue = animationCurve.Evaluate(Time.time);
        canvas_Interactive.transform.localScale = Vector3.Distance(Camera.main.transform.position, transform.position) * new Vector3(size * finalScale.x * graphValue,size * finalScale.y * graphValue, size * finalScale.z * graphValue);


        canvas_Interactive.transform.LookAt(Camera.main.transform);

        if (!avaible)
        {
            canvas_Interactive.enabled = false;
        }

        
        //canvas_Interactive.transform.localScale = finalScale * graphValue;

    }

    private void OnTriggerStay(Collider col)
    {

        if (col.tag == ("Player"))
        {
            canvas_Interactive.enabled = true;
        }

    }

    private void OnTriggerExit(Collider col)
    {

        if (col.tag == ("Player"))
        {
            canvas_Interactive.enabled = false;
        }

    }

    public void Block()
    {
        avaible = false;
    }

    public void Unlock()
    {
        avaible = true;
    }

}
