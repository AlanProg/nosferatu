﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

//[ExecuteInEditMode]
[RequireComponent(typeof(RectTransform))]
public class UI_ChildrenFade : MonoBehaviour
{

    [Range(0, 1.0f)]
    public float alpha = 1;

    [SerializeField]
    private Image[] images;
    private float[] imageAlpha;
    [SerializeField]
    private TextMeshProUGUI[] texts;
    private float[] textAlpha;


    void Start()
    {
        //if (images == null)
        //{
        //    images = GetComponentsInChildren<Image>(true);
        //    imageAlpha = new float[images.Length];
        //    for (int i = 0; i < images.Length; i++)
        //    {
        //        imageAlpha[i] = images[i].color.a;
        //    }
        //}
        //else
        //{
        //images = GetComponentsInChildren<Image>(true);
        //}

        //if (texts == null)
        //{
        //    texts = GetComponentsInChildren<TextMeshProUGUI>(true);
        //    textAlpha = new float[texts.Length];
        //    for (int i = 0; i < texts.Length; i++)
        //    {
        //        textAlpha[i] = texts[i].color.a;
        //    }

        //}
        //else
        //{
        //texts = GetComponentsInChildren<TextMeshProUGUI>(true);
        //}

        images = GetComponentsInChildren<Image>(true);
        texts = GetComponentsInChildren<TextMeshProUGUI>(true);
        imageAlpha = new float[images.Length];
        textAlpha = new float[texts.Length];



        for (int i = 0; i < images.Length; i++)
        {
            imageAlpha[i] = images[i].color.a;
        }

        for (int i = 0; i < texts.Length; i++)
        {
            textAlpha[i] = texts[i].color.a;

        }

    }

    
    void LateUpdate()
    {
        if (images == null || texts == null)
            return;

        for (int i = 0; i < images.Length; i++)
        {
            
            images[i].color = new Color(images[i].color.r, images[i].color.g, images[i].color.b, imageAlpha[i] * alpha);
            
        }

        for (int i = 0; i < texts.Length; i++)
        {
            texts[i].color = new Color(texts[i].color.r, texts[i].color.g, texts[i].color.b, textAlpha[i] * alpha);
            
        }
        
    }

    private void Reset()
    {
        images = GetComponentsInChildren<Image>(true);
        texts = GetComponentsInChildren<TextMeshProUGUI>(true);
        imageAlpha = new float[images.Length];
        textAlpha = new float[texts.Length];

        

        for (int i = 0; i < images.Length; i++)
        {
            imageAlpha[i] = images[i].color.a;
        }

        for (int i = 0; i < texts.Length; i++)
        {
            textAlpha[i] = texts[i].color.a;
           
        }

    }
}
