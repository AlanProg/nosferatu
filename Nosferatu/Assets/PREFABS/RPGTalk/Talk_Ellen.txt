[title=Cutscene_0_begin]
Ellen: You… I’ve seen you… At the cemetery…
Andrei: You must be Ellen…
Ellen: [question=ellen1]What are you doing on this wretched place?
[choice]I have come to end your cursed life.[newtalk start=FollowUp_Cutscene_0_Choice_0_begin break=FollowUp_Cutscene_0_Choice_0_end]
[choice]You’re the reason this place is wretched![newtalk start=FollowUp_Cutscene_0_Choice_1_begin break=FollowUp_Cutscene_0_Choice_1_end]
[choice]So, you’re that shadow over the church, watching me…[newtalk start=FollowUp_Cutscene_0_Choice_2_begin break=FollowUp_Cutscene_0_Choice_2_end]
[title=Cutscene_0_end]

[title=FollowUp_Cutscene_0_Choice_0_begin]
Ellen: Oh… Is that so?
[title=FollowUp_Cutscene_0_Choice_0_end]
[title=FollowUp_Cutscene_0_Choice_2_begin]
Ellen: I’ve been watching you… I can feel it… You are like me… You’re cursed… I do not understand what is happening to me, what happened to everyone, why there is so much suffering? I can not remember, I only remember the pain and the fangs devouring me…
Andrei: Come with me, I can help you.
[title=FollowUp_Cutscene_0_Choice_2_end]
[title=FollowUp_FollowUp_Cutscene_0_Choice_1_Choice_0_begin]
Ellen: You’re just like me, I can feel it… So be it, let’s both be gone from this world…
[title=FollowUp_FollowUp_Cutscene_0_Choice_1_Choice_0_end]
[title=FollowUp_Cutscene_0_Choice_1_begin]
Ellen: [question=ellen2]Other men, violent men like you, came and said the same, they tried to stab me and put me on fire… Why am I the reason? Why am I alive? Why Thomas? I don’t know anything… I just know I’m here and they are not…
[choice]You’re soul is cursed, you’re no longer from this world and I’ll put you to rest. Die, demon![newtalk start=FollowUp_FollowUp_Cutscene_0_Choice_1_Choice_0_begin break=FollowUp_FollowUp_Cutscene_0_Choice_1_Choice_0_end]
[choice]You have a power you cannot control, and you are hurting the people around you… People you cared about before you died. I know this flow of emotions, angery, grief, sadness, I can help you…[newtalk start=FollowUp_FollowUp_Cutscene_0_Choice_1_Choice_1_begin break=FollowUp_FollowUp_Cutscene_0_Choice_1_Choice_1_end]
[title=FollowUp_Cutscene_0_Choice_1_end]
[title=FollowUp_FollowUp_Cutscene_0_Choice_1_Choice_1_begin]
Ellen: I’ve been watching you… I can feel it… You are like me… You’re cursed… I do not understand what is happening to me, what happened to everyone, why there is so much suffering? I can not remember, I only remember the pain and the fangs devouring me…
Andrei: Come with me, I can help you.
[title=FollowUp_FollowUp_Cutscene_0_Choice_1_Choice_1_end]