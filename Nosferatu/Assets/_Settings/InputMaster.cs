// GENERATED AUTOMATICALLY FROM 'Assets/_Settings/InputMaster.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputMaster : IInputActionCollection, IDisposable
{
    private InputActionAsset asset;
    public @InputMaster()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputMaster"",
    ""maps"": [
        {
            ""name"": ""PlayerControlls"",
            ""id"": ""0ca2dc42-6c18-4ae1-b1ef-34a84dcb6a7e"",
            ""actions"": [
                {
                    ""name"": ""MoveInputs"",
                    ""type"": ""Button"",
                    ""id"": ""c88b5b7e-651a-42cd-81b0-783a654a3d19"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Hold""
                },
                {
                    ""name"": ""AimInput"",
                    ""type"": ""Button"",
                    ""id"": ""b0a2845c-07c6-4ff7-80b1-23ceee3fd2f9"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""ShootInputs"",
                    ""type"": ""Button"",
                    ""id"": ""b1bcf38d-4e3d-43c9-8307-73ac39ed9642"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""CombatInputs"",
                    ""type"": ""Button"",
                    ""id"": ""d672ac35-11b7-41f1-b615-e6ac1671fed5"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""RunInputs"",
                    ""type"": ""Button"",
                    ""id"": ""e622a0d4-8ee9-4470-b2cb-8b1bcda47598"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""DashInputs"",
                    ""type"": ""Button"",
                    ""id"": ""a46ffa8b-822a-46df-b9aa-cb590475f252"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Fury"",
                    ""type"": ""Button"",
                    ""id"": ""2ce4b42b-012b-423b-ab29-d5937990744e"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Stealth"",
                    ""type"": ""Button"",
                    ""id"": ""18700818-0c9d-4f36-8ed5-719891b567b5"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Interaction"",
                    ""type"": ""Button"",
                    ""id"": ""780eb820-00d2-4470-b796-93f783d41480"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""RightStick"",
                    ""type"": ""Button"",
                    ""id"": ""0176979c-4818-471b-bfa4-9787def5f2dd"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""arrowKeys"",
                    ""id"": ""53362505-60a3-48e4-8b69-9726af39b51b"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""3e237b66-15f2-4b9d-816b-4f3aa075bc64"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""f06a6a57-0165-4f71-812c-cbdb3f56a481"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""e98a0246-cffa-4f04-b555-25f384f342c5"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""183f4dd0-d320-4842-82e0-104cf78ad929"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""arrowKeys"",
                    ""id"": ""852bda91-bf8e-4f41-a883-a7c8a41b1f9a"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""d94cfa8d-5c97-459d-8b61-f922cc78e666"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GenericControll"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""967fa7a6-57a0-4cbf-a62e-127cc1c24ec3"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GenericControll"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""3e7a1bd9-52c4-4639-b13e-cb81908f0748"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GenericControll"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""18de689c-1ee0-4713-91da-93e632ec65ce"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GenericControll"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""awsdKeys"",
                    ""id"": ""e4d12e49-8dd5-4c9d-8ad8-0e082792f993"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""c0c5bffa-fd3f-4d46-817c-01c571740158"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""b81801ec-c16b-41ff-aa60-6e0e4f132de0"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""5ac75fe5-c6fe-40cc-a8d0-d4f744b8f14e"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""ae22c742-da3d-42b7-b870-f5f7eb67758e"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""awsdKeys"",
                    ""id"": ""056761d2-ce87-4f8d-b8f5-3c5de6b1c4bf"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""cb397a5e-6258-4f22-bbdf-494c515ecac0"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GenericControll"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""3f09828a-e10e-4274-a86a-dfdaf5d3ce72"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GenericControll"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""eb27a3b8-c940-41f0-8574-b08b502b50c0"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GenericControll"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""d7c14e1a-6f3a-44b6-b492-67bc89795928"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GenericControll"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""DSArrowKeys"",
                    ""id"": ""99e5666a-c0f4-478a-b5f1-b8f05fca9f0b"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""ffb8e38a-fc80-460b-86fd-2bf4e3bfeb65"",
                    ""path"": ""<DualShockGamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""f9d05b3e-bc8b-41b4-8e81-8e87fa009f93"",
                    ""path"": ""<DualShockGamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""edba27f4-03dc-4646-82a8-44f60149cbb9"",
                    ""path"": ""<DualShockGamepad>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""8e405695-bdef-4ef7-a8af-1db723281372"",
                    ""path"": ""<DualShockGamepad>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""DSStickKeys"",
                    ""id"": ""6b03d35f-6c61-43b9-a858-63622fa1c9a3"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""796a40f9-8bd8-4857-b6ba-96dfd80888f1"",
                    ""path"": ""<DualShockGamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""1caf4d94-6af2-4039-9c7a-b6ba2b8080df"",
                    ""path"": ""<DualShockGamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""0cbe7be8-fc5f-4e97-af64-1eefab5923f2"",
                    ""path"": ""<DualShockGamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""935b7c9f-f42c-485f-a1ed-d6e23ff18459"",
                    ""path"": ""<DualShockGamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""XboxArrows"",
                    ""id"": ""d1f6a845-dd7a-4401-b4df-1be1001824f3"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""fee7cb36-24fc-4dfb-859e-fb4bb8bf3cdc"",
                    ""path"": ""<XInputController>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""c99dd8ca-1e7b-4d53-9234-35d93ed5e62b"",
                    ""path"": ""<XInputController>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""403b6bd7-c514-4932-9b6b-646edc47d5d6"",
                    ""path"": ""<XInputController>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""aeab8afd-4f7e-4bfc-92ea-db527a9030b9"",
                    ""path"": ""<XInputController>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""XboxArrows"",
                    ""id"": ""7c80dd90-ebc5-41fd-bc12-54de66408f73"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""819cc1b1-73d5-43b7-811e-4b18ba197507"",
                    ""path"": ""<XInputController>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""9d136c6f-b004-4708-b3aa-45f260afca15"",
                    ""path"": ""<XInputController>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""79263202-db79-4b1b-9aba-101893bb9063"",
                    ""path"": ""<XInputController>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""e4c6482a-0cee-41bd-9fd1-b1ce52f40045"",
                    ""path"": ""<XInputController>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""MoveInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""02ff3869-d52c-4f41-a8be-a569685897e6"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""ShootInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""56fe9509-1c56-4838-8ed9-fe84e2264945"",
                    ""path"": ""<DualShockGamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""ShootInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""db1ea517-b381-493e-985a-81616ce839fa"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GenericControll"",
                    ""action"": ""ShootInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d9edbd03-0b81-4ffb-8549-152a67d6a2ee"",
                    ""path"": ""<XInputController>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""ShootInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""49c6711b-b43d-4bba-9a46-aedd863cb8d1"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""AimInput"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""40d01f09-9696-4b06-8d03-3afb1eb40ab7"",
                    ""path"": ""<DualShockGamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""AimInput"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1d03f56a-e09c-46a0-9db0-d4b8a164e920"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GenericControll"",
                    ""action"": ""AimInput"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""860d4798-d6a3-429b-816b-a239bd8cf16f"",
                    ""path"": ""<XInputController>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""AimInput"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""21cf07ac-ab2a-4402-82b2-abc9fb95287c"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""CombatInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1d0f2a0c-f01c-4365-9400-af8604b1aad3"",
                    ""path"": ""<DualShockGamepad>/leftStickPress"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""RunInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""85e97dca-94cf-4521-bcd9-d6b90415545c"",
                    ""path"": ""<Gamepad>/leftStickPress"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GenericControll"",
                    ""action"": ""RunInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5c786088-410e-472a-96b9-633612253853"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""RunInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""70b5355a-5465-487d-bf8d-bd5218125212"",
                    ""path"": ""<XInputController>/leftStickPress"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""RunInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""00bd8008-1461-47e3-9fdd-4dfd699bff3d"",
                    ""path"": ""<DualShockGamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""CombatInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d2a7b6f2-b070-41e5-aefa-3df2049a9655"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GenericControll"",
                    ""action"": ""CombatInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cdf44f09-ba99-40cf-a39a-d20108b74dc8"",
                    ""path"": ""<DualShockGamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""CombatInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1cacc4cd-c9bd-404b-8908-bf1751f375ae"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GenericControll"",
                    ""action"": ""CombatInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f7176298-21b6-4bd4-8c23-e7394cf7af1d"",
                    ""path"": ""<XInputController>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""CombatInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9435bea9-075a-47df-a053-11dac63d03cd"",
                    ""path"": ""<XInputController>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""CombatInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""07da1cb7-1261-4db5-a2a1-16ea9ead83e3"",
                    ""path"": ""<Keyboard>/leftCtrl"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Stealth"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cc7d12de-e4b9-4467-9851-d5fcd14f354b"",
                    ""path"": ""<DualShockGamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""Stealth"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""71fad5d3-e839-4be4-91a1-8029fecb2a4b"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GenericControll"",
                    ""action"": ""Stealth"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""974f178a-303f-4096-ae08-c377494e703f"",
                    ""path"": ""<XInputController>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""Stealth"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""08a9231b-8ada-47cb-b1db-726b94da1c4c"",
                    ""path"": ""<DualShockGamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""DashInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""66ce0aec-7ef6-46f7-bd7d-b409f66d115f"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GenericControll"",
                    ""action"": ""DashInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a02b725d-c42f-4874-9f05-69bcb2af8d49"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""DashInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d22b7404-8b8d-4ff3-a115-ad6646f9b0b5"",
                    ""path"": ""<XInputController>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""DashInputs"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b9660879-c928-4ad8-b307-75229b481a1b"",
                    ""path"": ""<DualShockGamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""Fury"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d234ab13-8423-44e2-8a91-7c05e5995d8e"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GenericControll"",
                    ""action"": ""Fury"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2e4154a8-1544-4ce5-a67b-2b078317158c"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Fury"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c3ee519e-9884-4fa8-834a-f28f1a563218"",
                    ""path"": ""<XInputController>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""Fury"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4a267c90-f10c-482e-8b00-908dfef6b4f5"",
                    ""path"": ""<DualShockGamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""Interaction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ce0fb14d-b034-4dbb-8b8c-52e5e529b3be"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GenericControll"",
                    ""action"": ""Interaction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bbcd8eea-f744-4814-879e-338e2202ad3c"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Interaction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f98251e0-f049-42e6-83e4-bf28ccb388e1"",
                    ""path"": ""<XInputController>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""Interaction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""de9d0009-68a2-4b42-bdd7-48a27f5e8024"",
                    ""path"": ""<DualShockGamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""RightStick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""48978666-c0d4-4eb6-8eab-ece3b54c8c21"",
                    ""path"": ""<XInputController>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""RightStick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""MenuControlls"",
            ""id"": ""d7c2f1ab-05dd-477e-863a-f0ed7fac1e5a"",
            ""actions"": [
                {
                    ""name"": ""Ps4Pause"",
                    ""type"": ""Button"",
                    ""id"": ""e701be86-e9eb-4731-a37c-459fea3d5492"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""NavigationMenu"",
                    ""type"": ""Button"",
                    ""id"": ""b92cc0d4-5b7a-4bd4-9760-abc1340d8240"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Confirmation"",
                    ""type"": ""Button"",
                    ""id"": ""e5e2b159-df1b-4bb8-95cb-ea2a52edc38f"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Return"",
                    ""type"": ""Button"",
                    ""id"": ""552a6823-8c2e-4021-949f-6ddd188d3c3c"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""LeftPage"",
                    ""type"": ""Button"",
                    ""id"": ""8507a6a9-8bbb-43b7-869e-6a767bd41405"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""RightPage"",
                    ""type"": ""Button"",
                    ""id"": ""990d7e5e-e5f2-402d-a838-7cd14dda1f25"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""PC_Pause"",
                    ""type"": ""Button"",
                    ""id"": ""e6bf6479-3e97-4f74-b09c-636096ee922f"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""XBOX_Pause"",
                    ""type"": ""Button"",
                    ""id"": ""a892f152-36cb-4ad3-b34b-c747104ee5d1"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""MenuUp"",
                    ""type"": ""Button"",
                    ""id"": ""fc4ffd71-7783-43a0-8ec8-49feb82dfcd8"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""MenuDown"",
                    ""type"": ""Button"",
                    ""id"": ""7ee67934-e1aa-4f5b-89d9-6a1a5af9f789"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""35004c76-5f63-4372-9b5f-38a18864e617"",
                    ""path"": ""<DualShockGamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""Ps4Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e89a5761-06a7-4073-b520-48c4d1a27bf8"",
                    ""path"": ""<Gamepad>/select"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Ps4Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Up_Down_WS"",
                    ""id"": ""d057dc10-9c67-4193-bb82-70548d3c1e26"",
                    ""path"": ""1DAxis"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""a61d38e4-e122-462a-8877-cf3abe44c61e"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""155a4f30-aae1-4dbc-8282-8e0720b1cbb4"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Up_Down_Arrows"",
                    ""id"": ""7ac92c03-a3a0-40dc-81ad-b945ad937061"",
                    ""path"": ""1DAxis"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""f2c1845b-ecca-4da6-9a7f-023015ad248c"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""c1544e7b-5a66-43e0-84fb-f9e1e7df3667"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Left_Right_AD"",
                    ""id"": ""0c22f802-530f-46fc-8963-64840d7d24d8"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""74e50bac-0d6d-4466-af30-605f10180b55"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""1c749998-97cb-4b86-be90-4faa9b6db721"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Left_Right_Arrows"",
                    ""id"": ""470733e3-7a9b-43dd-9534-6ea10dd0e4c5"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""74c84d9a-de5a-4603-b455-cfdbc77feba0"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""7f9bcdd1-f1be-48c3-9623-7693842bb2ee"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Up_Down_Arrows"",
                    ""id"": ""23f08eb1-22e2-4c80-81a6-ca46bf17671c"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""933a5bc9-a522-4084-ac96-7845ef2aece2"",
                    ""path"": ""<DualShockGamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""97890651-9460-4d3c-b858-fcb2a0c401c0"",
                    ""path"": ""<DualShockGamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Up_Down_Arrows"",
                    ""id"": ""be9dbfd2-f327-4c09-84e9-4a544d9a610d"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""8f7bbccf-52af-4bab-8ec6-067391626b79"",
                    ""path"": ""<DualShockGamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""c0601959-cb73-476a-8a77-2c283bf91c20"",
                    ""path"": ""<DualShockGamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Right_Left_Arrows"",
                    ""id"": ""7204df6e-a9d8-4b06-b136-5a15ace0cfd9"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""9700a1bc-1dcb-4379-b5c3-0d19f71eed86"",
                    ""path"": ""<DualShockGamepad>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""aea10a7e-daed-4e9a-8131-2311f3e245d3"",
                    ""path"": ""<DualShockGamepad>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Up_Down_Stick"",
                    ""id"": ""9a71b094-b38e-4abf-9ed1-90b41d821ff3"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""7dc5f1d3-a7a4-45ba-8cf6-39e9744c7800"",
                    ""path"": ""<DualShockGamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""8fd3e774-ea1a-4241-baba-a8bcf5a779d4"",
                    ""path"": ""<DualShockGamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Right_Left_Stick"",
                    ""id"": ""052adefa-92b6-4da9-ad6f-a3f1de9ee60f"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""2e9df7c5-c594-40af-b746-f58c0af2336d"",
                    ""path"": ""<DualShockGamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""9ef51f8f-e0dc-4b9f-ab04-c8ecbfd50e7c"",
                    ""path"": ""<DualShockGamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Up_Down_Arrow"",
                    ""id"": ""d632f92b-aec2-4fa6-be18-983ccb317309"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""4408d4ab-8945-4695-9ca7-b38ff0ca651f"",
                    ""path"": ""<XInputController>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""1f7b2c74-04fc-4cdf-ac0b-654c71be17c0"",
                    ""path"": ""<XInputController>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Left_Right_Arrow"",
                    ""id"": ""677a58aa-16f1-4fe7-8d04-ce36b1323842"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""5b3e617c-e906-4ac6-9702-3032ea09c3ec"",
                    ""path"": ""<XInputController>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""1c147bce-4984-4716-b10f-a522ab21508c"",
                    ""path"": ""<XInputController>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Up_Down_Stick"",
                    ""id"": ""d5302409-46af-40ed-bf06-65e84e3a9ad4"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""b148dd61-55d0-474b-9455-8e4a21be5277"",
                    ""path"": ""<XInputController>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""ebebfc91-997e-48bb-9fb5-ea26bbd47cce"",
                    ""path"": ""<XInputController>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Up_Down_Arrow"",
                    ""id"": ""240cc96a-99ea-498c-b5e9-9cc299ae5c17"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""3594e110-e61a-4e92-883d-a94860c4055e"",
                    ""path"": ""<XInputController>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""d0cc080e-fc30-4866-9b4b-f43d0b706585"",
                    ""path"": ""<XInputController>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""NavigationMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""12c311e7-5e74-40cf-a5c7-97edcd9e2ed1"",
                    ""path"": ""<DualShockGamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""Confirmation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d20930ec-7e2b-4e7c-8b7a-2a786ef44ba4"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Confirmation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f24bfcb2-f032-4b26-818a-192fc654b1b9"",
                    ""path"": ""<XInputController>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""Confirmation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""58290a95-3cf3-447e-b2cc-15e30b849c2f"",
                    ""path"": ""<DualShockGamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""Return"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""23d95903-d44e-45cb-8811-3e78a3873b3a"",
                    ""path"": ""<DualShockGamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PS4Controller"",
                    ""action"": ""Return"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""502d1c4d-9dec-4011-b4e1-c3daace884ab"",
                    ""path"": ""<Keyboard>/backspace"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Return"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6e6e82cf-8949-4e96-b457-e7b6e3be8517"",
                    ""path"": ""<XInputController>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XboxController"",
                    ""action"": ""Return"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d6b85001-b1ac-4d17-a8fa-89fc30017773"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LeftPage"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c03951dc-5ca0-4554-bf4f-f666615eaf60"",
                    ""path"": ""<XInputController>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LeftPage"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""41238228-e635-4c32-82cb-1486eb97ecbf"",
                    ""path"": ""<DualShockGamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LeftPage"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""82b5b140-7388-4bf3-90d1-eded30b0b249"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RightPage"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f92c8f5f-e03e-45be-b0bf-92b76a9e984a"",
                    ""path"": ""<XInputController>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RightPage"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ddb52495-9dca-49f4-adcc-95495f3d9b2c"",
                    ""path"": ""<DualShockGamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RightPage"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5701ef30-55fa-474e-a4a3-9dcb49bed98c"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PC_Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0b743d0f-b415-4dfa-b1d6-330b2d064770"",
                    ""path"": ""<XInputController>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""XBOX_Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e92e456f-e69f-4587-b8f6-b37e3d41f2ff"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MenuUp"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""52b00207-dbc1-49f8-8e6a-790378edaa6c"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MenuDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""CheatControll"",
            ""id"": ""501f5d53-ab52-4c75-9929-5f59c16f5ead"",
            ""actions"": [
                {
                    ""name"": ""FullLifeInput"",
                    ""type"": ""Button"",
                    ""id"": ""becd1485-167a-4d1e-be62-d255129099bc"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""FullFuryInput"",
                    ""type"": ""Button"",
                    ""id"": ""efa53c0e-6fb3-4f31-ab31-ea781de64573"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""NoClipInput"",
                    ""type"": ""Button"",
                    ""id"": ""c7d69f69-22e0-4811-8ffb-892b3fb38278"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SceneOneInput"",
                    ""type"": ""Button"",
                    ""id"": ""33a2b26f-6c7d-4cf6-8bed-3569d544e35f"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SceneTwoInput"",
                    ""type"": ""Button"",
                    ""id"": ""be335811-523b-44b0-a746-5f593cc778b7"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SceneThreeInput"",
                    ""type"": ""Button"",
                    ""id"": ""997033d1-fdd9-426b-bf60-badfb401cf47"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""AllLores"",
                    ""type"": ""Button"",
                    ""id"": ""58a201c0-e458-4251-9a6d-1b9adb1bd3fc"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""0b7026ed-f190-48c3-aec3-405ed126c248"",
                    ""path"": ""<Keyboard>/numpad4"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""FullLifeInput"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1e67c761-da6c-4840-b26b-367c2875a4c3"",
                    ""path"": ""<Keyboard>/numpad5"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""FullFuryInput"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7aba0d4e-4222-4292-9e71-25ffec7eb3ae"",
                    ""path"": ""<Keyboard>/numpad6"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""NoClipInput"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e86bba5b-ecc0-448a-b300-3e04e0ea7ada"",
                    ""path"": ""<Keyboard>/numpad1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""SceneOneInput"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b06412b7-c310-4550-ba86-9d537f88dc5a"",
                    ""path"": ""<Keyboard>/numpad2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""SceneTwoInput"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""58f009a0-e981-476a-a23d-234a172fcb2a"",
                    ""path"": ""<Keyboard>/numpad3"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""SceneThreeInput"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6f7562e9-ceb4-47f3-a1b2-edd898e67bdf"",
                    ""path"": ""<Keyboard>/numpad8"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AllLores"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""PS4Controller"",
            ""bindingGroup"": ""PS4Controller"",
            ""devices"": [
                {
                    ""devicePath"": ""<DualShockGamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""PC"",
            ""bindingGroup"": ""PC"",
            ""devices"": [
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""XboxController"",
            ""bindingGroup"": ""XboxController"",
            ""devices"": [
                {
                    ""devicePath"": ""<XInputController>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""GenericControll"",
            ""bindingGroup"": ""GenericControll"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // PlayerControlls
        m_PlayerControlls = asset.FindActionMap("PlayerControlls", throwIfNotFound: true);
        m_PlayerControlls_MoveInputs = m_PlayerControlls.FindAction("MoveInputs", throwIfNotFound: true);
        m_PlayerControlls_AimInput = m_PlayerControlls.FindAction("AimInput", throwIfNotFound: true);
        m_PlayerControlls_ShootInputs = m_PlayerControlls.FindAction("ShootInputs", throwIfNotFound: true);
        m_PlayerControlls_CombatInputs = m_PlayerControlls.FindAction("CombatInputs", throwIfNotFound: true);
        m_PlayerControlls_RunInputs = m_PlayerControlls.FindAction("RunInputs", throwIfNotFound: true);
        m_PlayerControlls_DashInputs = m_PlayerControlls.FindAction("DashInputs", throwIfNotFound: true);
        m_PlayerControlls_Fury = m_PlayerControlls.FindAction("Fury", throwIfNotFound: true);
        m_PlayerControlls_Stealth = m_PlayerControlls.FindAction("Stealth", throwIfNotFound: true);
        m_PlayerControlls_Interaction = m_PlayerControlls.FindAction("Interaction", throwIfNotFound: true);
        m_PlayerControlls_RightStick = m_PlayerControlls.FindAction("RightStick", throwIfNotFound: true);
        // MenuControlls
        m_MenuControlls = asset.FindActionMap("MenuControlls", throwIfNotFound: true);
        m_MenuControlls_Ps4Pause = m_MenuControlls.FindAction("Ps4Pause", throwIfNotFound: true);
        m_MenuControlls_NavigationMenu = m_MenuControlls.FindAction("NavigationMenu", throwIfNotFound: true);
        m_MenuControlls_Confirmation = m_MenuControlls.FindAction("Confirmation", throwIfNotFound: true);
        m_MenuControlls_Return = m_MenuControlls.FindAction("Return", throwIfNotFound: true);
        m_MenuControlls_LeftPage = m_MenuControlls.FindAction("LeftPage", throwIfNotFound: true);
        m_MenuControlls_RightPage = m_MenuControlls.FindAction("RightPage", throwIfNotFound: true);
        m_MenuControlls_PC_Pause = m_MenuControlls.FindAction("PC_Pause", throwIfNotFound: true);
        m_MenuControlls_XBOX_Pause = m_MenuControlls.FindAction("XBOX_Pause", throwIfNotFound: true);
        m_MenuControlls_MenuUp = m_MenuControlls.FindAction("MenuUp", throwIfNotFound: true);
        m_MenuControlls_MenuDown = m_MenuControlls.FindAction("MenuDown", throwIfNotFound: true);
        // CheatControll
        m_CheatControll = asset.FindActionMap("CheatControll", throwIfNotFound: true);
        m_CheatControll_FullLifeInput = m_CheatControll.FindAction("FullLifeInput", throwIfNotFound: true);
        m_CheatControll_FullFuryInput = m_CheatControll.FindAction("FullFuryInput", throwIfNotFound: true);
        m_CheatControll_NoClipInput = m_CheatControll.FindAction("NoClipInput", throwIfNotFound: true);
        m_CheatControll_SceneOneInput = m_CheatControll.FindAction("SceneOneInput", throwIfNotFound: true);
        m_CheatControll_SceneTwoInput = m_CheatControll.FindAction("SceneTwoInput", throwIfNotFound: true);
        m_CheatControll_SceneThreeInput = m_CheatControll.FindAction("SceneThreeInput", throwIfNotFound: true);
        m_CheatControll_AllLores = m_CheatControll.FindAction("AllLores", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // PlayerControlls
    private readonly InputActionMap m_PlayerControlls;
    private IPlayerControllsActions m_PlayerControllsActionsCallbackInterface;
    private readonly InputAction m_PlayerControlls_MoveInputs;
    private readonly InputAction m_PlayerControlls_AimInput;
    private readonly InputAction m_PlayerControlls_ShootInputs;
    private readonly InputAction m_PlayerControlls_CombatInputs;
    private readonly InputAction m_PlayerControlls_RunInputs;
    private readonly InputAction m_PlayerControlls_DashInputs;
    private readonly InputAction m_PlayerControlls_Fury;
    private readonly InputAction m_PlayerControlls_Stealth;
    private readonly InputAction m_PlayerControlls_Interaction;
    private readonly InputAction m_PlayerControlls_RightStick;
    public struct PlayerControllsActions
    {
        private @InputMaster m_Wrapper;
        public PlayerControllsActions(@InputMaster wrapper) { m_Wrapper = wrapper; }
        public InputAction @MoveInputs => m_Wrapper.m_PlayerControlls_MoveInputs;
        public InputAction @AimInput => m_Wrapper.m_PlayerControlls_AimInput;
        public InputAction @ShootInputs => m_Wrapper.m_PlayerControlls_ShootInputs;
        public InputAction @CombatInputs => m_Wrapper.m_PlayerControlls_CombatInputs;
        public InputAction @RunInputs => m_Wrapper.m_PlayerControlls_RunInputs;
        public InputAction @DashInputs => m_Wrapper.m_PlayerControlls_DashInputs;
        public InputAction @Fury => m_Wrapper.m_PlayerControlls_Fury;
        public InputAction @Stealth => m_Wrapper.m_PlayerControlls_Stealth;
        public InputAction @Interaction => m_Wrapper.m_PlayerControlls_Interaction;
        public InputAction @RightStick => m_Wrapper.m_PlayerControlls_RightStick;
        public InputActionMap Get() { return m_Wrapper.m_PlayerControlls; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerControllsActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerControllsActions instance)
        {
            if (m_Wrapper.m_PlayerControllsActionsCallbackInterface != null)
            {
                @MoveInputs.started -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnMoveInputs;
                @MoveInputs.performed -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnMoveInputs;
                @MoveInputs.canceled -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnMoveInputs;
                @AimInput.started -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnAimInput;
                @AimInput.performed -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnAimInput;
                @AimInput.canceled -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnAimInput;
                @ShootInputs.started -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnShootInputs;
                @ShootInputs.performed -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnShootInputs;
                @ShootInputs.canceled -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnShootInputs;
                @CombatInputs.started -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnCombatInputs;
                @CombatInputs.performed -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnCombatInputs;
                @CombatInputs.canceled -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnCombatInputs;
                @RunInputs.started -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnRunInputs;
                @RunInputs.performed -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnRunInputs;
                @RunInputs.canceled -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnRunInputs;
                @DashInputs.started -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnDashInputs;
                @DashInputs.performed -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnDashInputs;
                @DashInputs.canceled -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnDashInputs;
                @Fury.started -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnFury;
                @Fury.performed -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnFury;
                @Fury.canceled -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnFury;
                @Stealth.started -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnStealth;
                @Stealth.performed -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnStealth;
                @Stealth.canceled -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnStealth;
                @Interaction.started -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnInteraction;
                @Interaction.performed -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnInteraction;
                @Interaction.canceled -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnInteraction;
                @RightStick.started -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnRightStick;
                @RightStick.performed -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnRightStick;
                @RightStick.canceled -= m_Wrapper.m_PlayerControllsActionsCallbackInterface.OnRightStick;
            }
            m_Wrapper.m_PlayerControllsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @MoveInputs.started += instance.OnMoveInputs;
                @MoveInputs.performed += instance.OnMoveInputs;
                @MoveInputs.canceled += instance.OnMoveInputs;
                @AimInput.started += instance.OnAimInput;
                @AimInput.performed += instance.OnAimInput;
                @AimInput.canceled += instance.OnAimInput;
                @ShootInputs.started += instance.OnShootInputs;
                @ShootInputs.performed += instance.OnShootInputs;
                @ShootInputs.canceled += instance.OnShootInputs;
                @CombatInputs.started += instance.OnCombatInputs;
                @CombatInputs.performed += instance.OnCombatInputs;
                @CombatInputs.canceled += instance.OnCombatInputs;
                @RunInputs.started += instance.OnRunInputs;
                @RunInputs.performed += instance.OnRunInputs;
                @RunInputs.canceled += instance.OnRunInputs;
                @DashInputs.started += instance.OnDashInputs;
                @DashInputs.performed += instance.OnDashInputs;
                @DashInputs.canceled += instance.OnDashInputs;
                @Fury.started += instance.OnFury;
                @Fury.performed += instance.OnFury;
                @Fury.canceled += instance.OnFury;
                @Stealth.started += instance.OnStealth;
                @Stealth.performed += instance.OnStealth;
                @Stealth.canceled += instance.OnStealth;
                @Interaction.started += instance.OnInteraction;
                @Interaction.performed += instance.OnInteraction;
                @Interaction.canceled += instance.OnInteraction;
                @RightStick.started += instance.OnRightStick;
                @RightStick.performed += instance.OnRightStick;
                @RightStick.canceled += instance.OnRightStick;
            }
        }
    }
    public PlayerControllsActions @PlayerControlls => new PlayerControllsActions(this);

    // MenuControlls
    private readonly InputActionMap m_MenuControlls;
    private IMenuControllsActions m_MenuControllsActionsCallbackInterface;
    private readonly InputAction m_MenuControlls_Ps4Pause;
    private readonly InputAction m_MenuControlls_NavigationMenu;
    private readonly InputAction m_MenuControlls_Confirmation;
    private readonly InputAction m_MenuControlls_Return;
    private readonly InputAction m_MenuControlls_LeftPage;
    private readonly InputAction m_MenuControlls_RightPage;
    private readonly InputAction m_MenuControlls_PC_Pause;
    private readonly InputAction m_MenuControlls_XBOX_Pause;
    private readonly InputAction m_MenuControlls_MenuUp;
    private readonly InputAction m_MenuControlls_MenuDown;
    public struct MenuControllsActions
    {
        private @InputMaster m_Wrapper;
        public MenuControllsActions(@InputMaster wrapper) { m_Wrapper = wrapper; }
        public InputAction @Ps4Pause => m_Wrapper.m_MenuControlls_Ps4Pause;
        public InputAction @NavigationMenu => m_Wrapper.m_MenuControlls_NavigationMenu;
        public InputAction @Confirmation => m_Wrapper.m_MenuControlls_Confirmation;
        public InputAction @Return => m_Wrapper.m_MenuControlls_Return;
        public InputAction @LeftPage => m_Wrapper.m_MenuControlls_LeftPage;
        public InputAction @RightPage => m_Wrapper.m_MenuControlls_RightPage;
        public InputAction @PC_Pause => m_Wrapper.m_MenuControlls_PC_Pause;
        public InputAction @XBOX_Pause => m_Wrapper.m_MenuControlls_XBOX_Pause;
        public InputAction @MenuUp => m_Wrapper.m_MenuControlls_MenuUp;
        public InputAction @MenuDown => m_Wrapper.m_MenuControlls_MenuDown;
        public InputActionMap Get() { return m_Wrapper.m_MenuControlls; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MenuControllsActions set) { return set.Get(); }
        public void SetCallbacks(IMenuControllsActions instance)
        {
            if (m_Wrapper.m_MenuControllsActionsCallbackInterface != null)
            {
                @Ps4Pause.started -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnPs4Pause;
                @Ps4Pause.performed -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnPs4Pause;
                @Ps4Pause.canceled -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnPs4Pause;
                @NavigationMenu.started -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnNavigationMenu;
                @NavigationMenu.performed -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnNavigationMenu;
                @NavigationMenu.canceled -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnNavigationMenu;
                @Confirmation.started -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnConfirmation;
                @Confirmation.performed -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnConfirmation;
                @Confirmation.canceled -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnConfirmation;
                @Return.started -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnReturn;
                @Return.performed -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnReturn;
                @Return.canceled -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnReturn;
                @LeftPage.started -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnLeftPage;
                @LeftPage.performed -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnLeftPage;
                @LeftPage.canceled -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnLeftPage;
                @RightPage.started -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnRightPage;
                @RightPage.performed -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnRightPage;
                @RightPage.canceled -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnRightPage;
                @PC_Pause.started -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnPC_Pause;
                @PC_Pause.performed -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnPC_Pause;
                @PC_Pause.canceled -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnPC_Pause;
                @XBOX_Pause.started -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnXBOX_Pause;
                @XBOX_Pause.performed -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnXBOX_Pause;
                @XBOX_Pause.canceled -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnXBOX_Pause;
                @MenuUp.started -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnMenuUp;
                @MenuUp.performed -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnMenuUp;
                @MenuUp.canceled -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnMenuUp;
                @MenuDown.started -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnMenuDown;
                @MenuDown.performed -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnMenuDown;
                @MenuDown.canceled -= m_Wrapper.m_MenuControllsActionsCallbackInterface.OnMenuDown;
            }
            m_Wrapper.m_MenuControllsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Ps4Pause.started += instance.OnPs4Pause;
                @Ps4Pause.performed += instance.OnPs4Pause;
                @Ps4Pause.canceled += instance.OnPs4Pause;
                @NavigationMenu.started += instance.OnNavigationMenu;
                @NavigationMenu.performed += instance.OnNavigationMenu;
                @NavigationMenu.canceled += instance.OnNavigationMenu;
                @Confirmation.started += instance.OnConfirmation;
                @Confirmation.performed += instance.OnConfirmation;
                @Confirmation.canceled += instance.OnConfirmation;
                @Return.started += instance.OnReturn;
                @Return.performed += instance.OnReturn;
                @Return.canceled += instance.OnReturn;
                @LeftPage.started += instance.OnLeftPage;
                @LeftPage.performed += instance.OnLeftPage;
                @LeftPage.canceled += instance.OnLeftPage;
                @RightPage.started += instance.OnRightPage;
                @RightPage.performed += instance.OnRightPage;
                @RightPage.canceled += instance.OnRightPage;
                @PC_Pause.started += instance.OnPC_Pause;
                @PC_Pause.performed += instance.OnPC_Pause;
                @PC_Pause.canceled += instance.OnPC_Pause;
                @XBOX_Pause.started += instance.OnXBOX_Pause;
                @XBOX_Pause.performed += instance.OnXBOX_Pause;
                @XBOX_Pause.canceled += instance.OnXBOX_Pause;
                @MenuUp.started += instance.OnMenuUp;
                @MenuUp.performed += instance.OnMenuUp;
                @MenuUp.canceled += instance.OnMenuUp;
                @MenuDown.started += instance.OnMenuDown;
                @MenuDown.performed += instance.OnMenuDown;
                @MenuDown.canceled += instance.OnMenuDown;
            }
        }
    }
    public MenuControllsActions @MenuControlls => new MenuControllsActions(this);

    // CheatControll
    private readonly InputActionMap m_CheatControll;
    private ICheatControllActions m_CheatControllActionsCallbackInterface;
    private readonly InputAction m_CheatControll_FullLifeInput;
    private readonly InputAction m_CheatControll_FullFuryInput;
    private readonly InputAction m_CheatControll_NoClipInput;
    private readonly InputAction m_CheatControll_SceneOneInput;
    private readonly InputAction m_CheatControll_SceneTwoInput;
    private readonly InputAction m_CheatControll_SceneThreeInput;
    private readonly InputAction m_CheatControll_AllLores;
    public struct CheatControllActions
    {
        private @InputMaster m_Wrapper;
        public CheatControllActions(@InputMaster wrapper) { m_Wrapper = wrapper; }
        public InputAction @FullLifeInput => m_Wrapper.m_CheatControll_FullLifeInput;
        public InputAction @FullFuryInput => m_Wrapper.m_CheatControll_FullFuryInput;
        public InputAction @NoClipInput => m_Wrapper.m_CheatControll_NoClipInput;
        public InputAction @SceneOneInput => m_Wrapper.m_CheatControll_SceneOneInput;
        public InputAction @SceneTwoInput => m_Wrapper.m_CheatControll_SceneTwoInput;
        public InputAction @SceneThreeInput => m_Wrapper.m_CheatControll_SceneThreeInput;
        public InputAction @AllLores => m_Wrapper.m_CheatControll_AllLores;
        public InputActionMap Get() { return m_Wrapper.m_CheatControll; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(CheatControllActions set) { return set.Get(); }
        public void SetCallbacks(ICheatControllActions instance)
        {
            if (m_Wrapper.m_CheatControllActionsCallbackInterface != null)
            {
                @FullLifeInput.started -= m_Wrapper.m_CheatControllActionsCallbackInterface.OnFullLifeInput;
                @FullLifeInput.performed -= m_Wrapper.m_CheatControllActionsCallbackInterface.OnFullLifeInput;
                @FullLifeInput.canceled -= m_Wrapper.m_CheatControllActionsCallbackInterface.OnFullLifeInput;
                @FullFuryInput.started -= m_Wrapper.m_CheatControllActionsCallbackInterface.OnFullFuryInput;
                @FullFuryInput.performed -= m_Wrapper.m_CheatControllActionsCallbackInterface.OnFullFuryInput;
                @FullFuryInput.canceled -= m_Wrapper.m_CheatControllActionsCallbackInterface.OnFullFuryInput;
                @NoClipInput.started -= m_Wrapper.m_CheatControllActionsCallbackInterface.OnNoClipInput;
                @NoClipInput.performed -= m_Wrapper.m_CheatControllActionsCallbackInterface.OnNoClipInput;
                @NoClipInput.canceled -= m_Wrapper.m_CheatControllActionsCallbackInterface.OnNoClipInput;
                @SceneOneInput.started -= m_Wrapper.m_CheatControllActionsCallbackInterface.OnSceneOneInput;
                @SceneOneInput.performed -= m_Wrapper.m_CheatControllActionsCallbackInterface.OnSceneOneInput;
                @SceneOneInput.canceled -= m_Wrapper.m_CheatControllActionsCallbackInterface.OnSceneOneInput;
                @SceneTwoInput.started -= m_Wrapper.m_CheatControllActionsCallbackInterface.OnSceneTwoInput;
                @SceneTwoInput.performed -= m_Wrapper.m_CheatControllActionsCallbackInterface.OnSceneTwoInput;
                @SceneTwoInput.canceled -= m_Wrapper.m_CheatControllActionsCallbackInterface.OnSceneTwoInput;
                @SceneThreeInput.started -= m_Wrapper.m_CheatControllActionsCallbackInterface.OnSceneThreeInput;
                @SceneThreeInput.performed -= m_Wrapper.m_CheatControllActionsCallbackInterface.OnSceneThreeInput;
                @SceneThreeInput.canceled -= m_Wrapper.m_CheatControllActionsCallbackInterface.OnSceneThreeInput;
                @AllLores.started -= m_Wrapper.m_CheatControllActionsCallbackInterface.OnAllLores;
                @AllLores.performed -= m_Wrapper.m_CheatControllActionsCallbackInterface.OnAllLores;
                @AllLores.canceled -= m_Wrapper.m_CheatControllActionsCallbackInterface.OnAllLores;
            }
            m_Wrapper.m_CheatControllActionsCallbackInterface = instance;
            if (instance != null)
            {
                @FullLifeInput.started += instance.OnFullLifeInput;
                @FullLifeInput.performed += instance.OnFullLifeInput;
                @FullLifeInput.canceled += instance.OnFullLifeInput;
                @FullFuryInput.started += instance.OnFullFuryInput;
                @FullFuryInput.performed += instance.OnFullFuryInput;
                @FullFuryInput.canceled += instance.OnFullFuryInput;
                @NoClipInput.started += instance.OnNoClipInput;
                @NoClipInput.performed += instance.OnNoClipInput;
                @NoClipInput.canceled += instance.OnNoClipInput;
                @SceneOneInput.started += instance.OnSceneOneInput;
                @SceneOneInput.performed += instance.OnSceneOneInput;
                @SceneOneInput.canceled += instance.OnSceneOneInput;
                @SceneTwoInput.started += instance.OnSceneTwoInput;
                @SceneTwoInput.performed += instance.OnSceneTwoInput;
                @SceneTwoInput.canceled += instance.OnSceneTwoInput;
                @SceneThreeInput.started += instance.OnSceneThreeInput;
                @SceneThreeInput.performed += instance.OnSceneThreeInput;
                @SceneThreeInput.canceled += instance.OnSceneThreeInput;
                @AllLores.started += instance.OnAllLores;
                @AllLores.performed += instance.OnAllLores;
                @AllLores.canceled += instance.OnAllLores;
            }
        }
    }
    public CheatControllActions @CheatControll => new CheatControllActions(this);
    private int m_PS4ControllerSchemeIndex = -1;
    public InputControlScheme PS4ControllerScheme
    {
        get
        {
            if (m_PS4ControllerSchemeIndex == -1) m_PS4ControllerSchemeIndex = asset.FindControlSchemeIndex("PS4Controller");
            return asset.controlSchemes[m_PS4ControllerSchemeIndex];
        }
    }
    private int m_PCSchemeIndex = -1;
    public InputControlScheme PCScheme
    {
        get
        {
            if (m_PCSchemeIndex == -1) m_PCSchemeIndex = asset.FindControlSchemeIndex("PC");
            return asset.controlSchemes[m_PCSchemeIndex];
        }
    }
    private int m_XboxControllerSchemeIndex = -1;
    public InputControlScheme XboxControllerScheme
    {
        get
        {
            if (m_XboxControllerSchemeIndex == -1) m_XboxControllerSchemeIndex = asset.FindControlSchemeIndex("XboxController");
            return asset.controlSchemes[m_XboxControllerSchemeIndex];
        }
    }
    private int m_GenericControllSchemeIndex = -1;
    public InputControlScheme GenericControllScheme
    {
        get
        {
            if (m_GenericControllSchemeIndex == -1) m_GenericControllSchemeIndex = asset.FindControlSchemeIndex("GenericControll");
            return asset.controlSchemes[m_GenericControllSchemeIndex];
        }
    }
    public interface IPlayerControllsActions
    {
        void OnMoveInputs(InputAction.CallbackContext context);
        void OnAimInput(InputAction.CallbackContext context);
        void OnShootInputs(InputAction.CallbackContext context);
        void OnCombatInputs(InputAction.CallbackContext context);
        void OnRunInputs(InputAction.CallbackContext context);
        void OnDashInputs(InputAction.CallbackContext context);
        void OnFury(InputAction.CallbackContext context);
        void OnStealth(InputAction.CallbackContext context);
        void OnInteraction(InputAction.CallbackContext context);
        void OnRightStick(InputAction.CallbackContext context);
    }
    public interface IMenuControllsActions
    {
        void OnPs4Pause(InputAction.CallbackContext context);
        void OnNavigationMenu(InputAction.CallbackContext context);
        void OnConfirmation(InputAction.CallbackContext context);
        void OnReturn(InputAction.CallbackContext context);
        void OnLeftPage(InputAction.CallbackContext context);
        void OnRightPage(InputAction.CallbackContext context);
        void OnPC_Pause(InputAction.CallbackContext context);
        void OnXBOX_Pause(InputAction.CallbackContext context);
        void OnMenuUp(InputAction.CallbackContext context);
        void OnMenuDown(InputAction.CallbackContext context);
    }
    public interface ICheatControllActions
    {
        void OnFullLifeInput(InputAction.CallbackContext context);
        void OnFullFuryInput(InputAction.CallbackContext context);
        void OnNoClipInput(InputAction.CallbackContext context);
        void OnSceneOneInput(InputAction.CallbackContext context);
        void OnSceneTwoInput(InputAction.CallbackContext context);
        void OnSceneThreeInput(InputAction.CallbackContext context);
        void OnAllLores(InputAction.CallbackContext context);
    }
}
