﻿using TMPro;
using UnityEngine;

namespace BrunoMikoski.TextJuicer.Modifiers
{
    [AddComponentMenu( "UI/Text Juicer Modifiers/Gradiend Modifier", 11 )]
    public sealed class TextJuicerGradientModifier : TextJuicerVertexModifier
    {
        [SerializeField]
        private Gradient gradient;

        private Color32[] newVertexColors;
        private Color targetColor;

        private TextMeshProUGUI originalColor;
        private GradientColorKey[] colorKey;
        private GradientAlphaKey[] alphaKey;
        private GradientColorKey[] originalColorKey;
        private GradientAlphaKey[] originalAlphaKey;

        public override bool ModifyGeometry
        {
            get
            {
                return false;
            }
        }
        public override bool ModifyVertex
        {
            get
            {
                return true;
            }
        }

        public override void ModifyCharacter(CharacterData characterData, TMP_Text textComponent,
            TMP_TextInfo textInfo,
            float progress,
            TMP_MeshInfo[] meshInfo)
        {

            if (gradient == null)
            {
                return;
                //colorKey = new GradientColorKey[2];
                //colorKey[0].color = Color.red;
                //colorKey[0].time = 0.0f;
                //colorKey[1].color = Color.blue;
                //colorKey[1].time = 1.0f;
                //alphaKey = new GradientAlphaKey[2];
                //alphaKey[0].alpha = 1.0f;
                //alphaKey[0].time = 0.0f;
                //alphaKey[1].alpha = 0.0f;
                //alphaKey[1].time = 1.0f;
            }

            int materialIndex = characterData.MaterialIndex;

            newVertexColors = textInfo.meshInfo[materialIndex].colors32;

            int vertexIndex = characterData.VertexIndex;

           
            

            targetColor = gradient.Evaluate( characterData.Progress );

            //if (characterData.Progress >= 1)
            //{
            //    gradient.SetKeys(originalColorKey, originalAlphaKey);
            //}

            newVertexColors[vertexIndex + 0] = targetColor;
            newVertexColors[vertexIndex + 1] = targetColor;
            newVertexColors[vertexIndex + 2] = targetColor;
            newVertexColors[vertexIndex + 3] = targetColor;
        }

        private void Update()
        {
            originalColor = GetComponent<TextMeshProUGUI>();
        }
        private void Start()
        {
            originalColor = GetComponent<TextMeshProUGUI>();
            originalColorKey = gradient.colorKeys;
            originalAlphaKey = gradient.alphaKeys;

            
            //////////////////

            colorKey = gradient.colorKeys;


            for (int i = 0; i < colorKey.Length; i++)
            {
                
                colorKey[i].color = originalColor.color * colorKey[i].color;
            }

            alphaKey = gradient.alphaKeys;

            for (int i = 0; i < alphaKey.Length; i++)
            {
                alphaKey[i].alpha = originalColor.color.a * alphaKey[i].alpha;
            }

            gradient.SetKeys(colorKey, alphaKey);

            ///////////////////////
        }
    }
}
