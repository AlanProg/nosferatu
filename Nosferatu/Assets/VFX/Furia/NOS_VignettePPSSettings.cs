// Amplify Shader Editor - Visual Shader Editing Tool
// Copyright (c) Amplify Creations, Lda <info@amplify.pt>
#if UNITY_POST_PROCESSING_STACK_V2
using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[Serializable]
[PostProcess( typeof( NOS_VignettePPSRenderer ), PostProcessEvent.AfterStack, "NOS_Vignette", true )]
public sealed class NOS_VignettePPSSettings : PostProcessEffectSettings
{
}

public sealed class NOS_VignettePPSRenderer : PostProcessEffectRenderer<NOS_VignettePPSSettings>
{
	public override void Render( PostProcessRenderContext context )
	{
		var sheet = context.propertySheets.Get( Shader.Find( "NOS_Vignette" ) );
		context.command.BlitFullscreenTriangle( context.source, context.destination, sheet, 0 );
	}
}
#endif
