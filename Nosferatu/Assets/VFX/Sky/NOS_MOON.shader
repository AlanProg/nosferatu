// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "NOS_MOON"
{
	Properties
	{
		_TEX_Moon_AlphaBlended2("TEX_Moon_AlphaBlended2", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Unlit alpha:fade keepalpha noshadow noambient novertexlights nolightmap  nodynlightmap nodirlightmap nofog 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float _GlobalSaturation;
		uniform sampler2D _TEX_Moon_AlphaBlended2;
		uniform float4 _TEX_Moon_AlphaBlended2_ST;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float4 color4 = IsGammaSpace() ? float4(1.304119,1.304119,1.304119,0) : float4(1.793485,1.793485,1.793485,0);
			float4 color9 = IsGammaSpace() ? float4(0.8384046,0.2016917,0.2016917,0) : float4(0.6709803,0.03363431,0.03363431,0);
			float4 lerpResult7 = lerp( color4 , color9 , _GlobalSaturation);
			float2 uv_TEX_Moon_AlphaBlended2 = i.uv_texcoord * _TEX_Moon_AlphaBlended2_ST.xy + _TEX_Moon_AlphaBlended2_ST.zw;
			float4 tex2DNode1 = tex2D( _TEX_Moon_AlphaBlended2, uv_TEX_Moon_AlphaBlended2 );
			o.Emission = ( lerpResult7 * tex2DNode1 ).rgb;
			o.Alpha = tex2DNode1.a;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16900
-1920;0;1920;1019;1620.754;351.6456;1;True;False
Node;AmplifyShaderEditor.ColorNode;4;-493.4358,-301.2632;Float;False;Constant;_Color0;Color 0;1;1;[HDR];Create;True;0;0;False;0;1.304119,1.304119,1.304119,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;5;-796.446,181.3126;Float;False;Global;_GlobalSaturation;_GlobalSaturation;1;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;9;-622.5815,342.2388;Float;False;Constant;_Color1;Color 1;1;1;[HDR];Create;True;0;0;False;0;0.8384046,0.2016917,0.2016917,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;1;-810.4358,-146.2632;Float;True;Property;_TEX_Moon_AlphaBlended2;TEX_Moon_AlphaBlended2;0;0;Create;True;0;0;False;0;debed2bacbfd7554ea6696b186e70496;debed2bacbfd7554ea6696b186e70496;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;7;-367.5815,130.2388;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;2;-196.4358,-94.26315;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;0;Unlit;NOS_MOON;False;False;False;False;True;True;True;True;True;True;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;False;0;False;Transparent;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Spherical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;7;0;4;0
WireConnection;7;1;9;0
WireConnection;7;2;5;0
WireConnection;2;0;7;0
WireConnection;2;1;1;0
WireConnection;0;2;2;0
WireConnection;0;9;1;4
ASEEND*/
//CHKSM=59DC3649E33AB8C9B2C15614ED15896ABCC27965