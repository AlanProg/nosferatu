// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "NOS_Clouds"
{
	Properties
	{
		_1("1", 2D) = "white" {}
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_MoonPosition("_MoonPosition", Vector) = (0,0,0,0)
		_CloudParter("_CloudParter", Float) = 0
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Unlit alpha:fade keepalpha noshadow noambient novertexlights nolightmap  nodynlightmap nodirlightmap nofog vertex:vertexDataFunc 
		struct Input
		{
			float3 worldPos;
			float eyeDepth;
		};

		uniform sampler2D _1;
		uniform sampler2D _TextureSample0;
		uniform float3 _MoonPosition;
		uniform float _CloudParter;
		uniform float _GlobalSaturation;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			o.eyeDepth = -UnityObjectToViewPos( v.vertex.xyz ).z;
		}

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float4 color60 = IsGammaSpace() ? float4(0,0,0,0) : float4(0,0,0,0);
			o.Emission = color60.rgb;
			float3 ase_worldPos = i.worldPos;
			float2 appendResult23 = (float2(ase_worldPos.x , ase_worldPos.z));
			float mulTime31 = _Time.y * 3.0;
			float2 appendResult19 = (float2(ase_worldPos.x , ase_worldPos.z));
			float mulTime33 = _Time.y * 2.0;
			float4 clampResult6 = clamp( tex2D( _1, ( ( tex2D( _TextureSample0, ( ( appendResult23 + mulTime31 ) * 0.006 ) ) * 0.1 ) + float4( ( ( appendResult19 + mulTime33 ) * 0.003 ), 0.0 , 0.0 ) ).rg ) , float4( 0,0,0,0 ) , float4( 1,1,1,0 ) );
			float cameraDepthFade14 = (( i.eyeDepth -_ProjectionParams.y - 0.0 ) / 500.0);
			float clampResult36 = clamp( ( 1.0 - cameraDepthFade14 ) , 0.0 , 1.0 );
			float clampResult53 = clamp( ( abs( distance( ase_worldPos , _MoonPosition ) ) * 0.004 ) , 0.0 , 1.0 );
			float lerpResult56 = lerp( 1.0 , pow( clampResult53 , 10.0 ) , max( _CloudParter , _GlobalSaturation ));
			o.Alpha = ( ( pow( ( clampResult6 * clampResult36 ) , 1.4 ) * lerpResult56 ) * 20.0 ).r;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16900
-1920;0;1920;1019;3461.489;-372.2679;1.6;True;False
Node;AmplifyShaderEditor.WorldPosInputsNode;22;-2106.141,-547.2781;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.DynamicAppendNode;23;-1867.877,-694.7386;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleTimeNode;31;-1946.844,-857.687;Float;False;1;0;FLOAT;3;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;1;-1513.224,-99.82562;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleAddOpNode;30;-1683.572,-721.0364;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;24;-1724.504,-435.4508;Float;False;Constant;_Float2;Float 2;1;0;Create;True;0;0;False;0;0.006;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;33;-1692.381,-188.0922;Float;False;1;0;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25;-1513.504,-574.751;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;19;-1305.819,-255.5762;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.WorldPosInputsNode;46;-1930.584,1071.122;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SamplerNode;26;-1383.636,-664.9602;Float;True;Property;_TextureSample0;Texture Sample 0;1;0;Create;True;0;0;False;0;3d9de2cbc07ff8545a66a4c4b2eae0ae;0051d41af7898da4d9f905390bc50f06;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;32;-1116.454,-242.9424;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;29;-1196.176,-470.3818;Float;False;Constant;_Float3;Float 3;2;0;Create;True;0;0;False;0;0.1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;21;-1149.929,-33.31299;Float;False;Constant;_Float1;Float 1;1;0;Create;True;0;0;False;0;0.003;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;37;-1904.891,832.8335;Float;False;Property;_MoonPosition;_MoonPosition;2;0;Create;True;0;0;False;0;0,0,0;421.2908,171.3,406.9;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;28;-998.5764,-599.0817;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.DistanceOpNode;50;-1490.168,946.0905;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;-959.7288,-163.513;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.CameraDepthFade;14;-703.6307,618.1049;Float;False;3;2;FLOAT3;0,0,0;False;0;FLOAT;500;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;27;-850.3765,-344.2817;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT2;0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.AbsOpNode;51;-1217.168,944.4908;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;18;-641.3609,-286.2222;Float;True;Property;_1;1;0;0;Create;True;0;0;False;0;3d9de2cbc07ff8545a66a4c4b2eae0ae;3d9de2cbc07ff8545a66a4c4b2eae0ae;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;52;-920.4249,959.8423;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0.004;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;16;-365.631,580.405;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;53;-728.9097,985.0545;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;36;-178.4485,518.5941;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;6;-448.7495,44.86114;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;1,1,1,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;58;-527.1833,1259.834;Float;False;Global;_GlobalSaturation;_GlobalSaturation;3;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;67;-517.1731,1377.671;Float;False;Property;_CloudParter;_CloudParter;3;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;17;-101.731,298.305;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;68;-220.1731,1339.671;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;54;-558.0925,981.0194;Float;False;2;0;FLOAT;0;False;1;FLOAT;10;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;56;-141.3995,946.1135;Float;False;3;0;FLOAT;1;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;34;83.05359,284.4642;Float;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;1.4;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;57;234.313,399.7374;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;63;249.9378,553.1393;Float;False;Constant;_Float4;Float 4;3;0;Create;True;0;0;False;0;20;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;9;-809.9019,208.9179;Float;False;Simplex3D;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;7;-1455.875,206.6394;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;8;-1027.505,196.3858;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0.01,0.01,0.01;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;61;60.03601,-283.203;Float;False;Constant;_Float0;Float 0;3;0;Create;True;0;0;False;0;-7;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;60;-10.96375,-203.203;Float;False;Constant;_Color0;Color 0;3;1;[HDR];Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;62;412.4377,405.5393;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;10;-490.9027,269.3;Float;False;3;0;FLOAT;0;False;1;FLOAT;0.7;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;786.0135,-37.90711;Float;False;True;2;Float;ASEMaterialInspector;0;0;Unlit;NOS_Clouds;False;False;False;False;True;True;True;True;True;True;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;5;True;False;0;False;Transparent;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;23;0;22;1
WireConnection;23;1;22;3
WireConnection;30;0;23;0
WireConnection;30;1;31;0
WireConnection;25;0;30;0
WireConnection;25;1;24;0
WireConnection;19;0;1;1
WireConnection;19;1;1;3
WireConnection;26;1;25;0
WireConnection;32;0;19;0
WireConnection;32;1;33;0
WireConnection;28;0;26;0
WireConnection;28;1;29;0
WireConnection;50;0;46;0
WireConnection;50;1;37;0
WireConnection;20;0;32;0
WireConnection;20;1;21;0
WireConnection;27;0;28;0
WireConnection;27;1;20;0
WireConnection;51;0;50;0
WireConnection;18;1;27;0
WireConnection;52;0;51;0
WireConnection;16;0;14;0
WireConnection;53;0;52;0
WireConnection;36;0;16;0
WireConnection;6;0;18;0
WireConnection;17;0;6;0
WireConnection;17;1;36;0
WireConnection;68;0;67;0
WireConnection;68;1;58;0
WireConnection;54;0;53;0
WireConnection;56;1;54;0
WireConnection;56;2;68;0
WireConnection;34;0;17;0
WireConnection;57;0;34;0
WireConnection;57;1;56;0
WireConnection;9;0;8;0
WireConnection;8;0;7;0
WireConnection;62;0;57;0
WireConnection;62;1;63;0
WireConnection;10;0;9;0
WireConnection;0;2;60;0
WireConnection;0;9;62;0
ASEEND*/
//CHKSM=5D27C74AF5B95B6F400DC90A2AED3BBC12445779