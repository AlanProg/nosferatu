// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "DecalShaderNormal"
{
	Properties
	{
		_LightTex("LightTex", 2D) = "white" {}
		_FalloffTex("FalloffTex", 2D) = "white" {}
		_TextureSample0("Texture Sample 0", 2D) = "bump" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard alpha:fade keepalpha noshadow exclude_path:deferred noambient novertexlights nolightmap  nodynlightmap nodirlightmap nofog vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_texcoord;
			float4 vertexToFrag5;
			float4 vertexToFrag10;
		};

		uniform sampler2D _TextureSample0;
		uniform float4 _TextureSample0_ST;
		uniform sampler2D _LightTex;
		float4x4 unity_Projector;
		uniform sampler2D _FalloffTex;
		float4x4 unity_ProjectorClip;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float4 ase_vertex4Pos = v.vertex;
			o.vertexToFrag5 = mul( unity_Projector, ase_vertex4Pos );
			o.vertexToFrag10 = mul( unity_ProjectorClip, ase_vertex4Pos );
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			float2 temp_output_12_0 = ( (i.vertexToFrag5).xy / (i.vertexToFrag5).w );
			float4 tex2DNode16 = tex2D( _LightTex, temp_output_12_0 );
			float temp_output_30_0 = ( tex2DNode16.a * tex2D( _FalloffTex, ( (i.vertexToFrag10).xy / (i.vertexToFrag10).w ) ).a );
			o.Normal = ( ( UnpackNormal( tex2D( _TextureSample0, uv_TextureSample0 ) ) * temp_output_30_0 ) * float3( 20,20,20 ) );
			float4 appendResult21 = (float4(tex2DNode16.rgb , ( 1.0 - tex2DNode16.a )));
			o.Albedo = appendResult21.xyz;
			o.Metallic = 0.1;
			o.Smoothness = 0.3;
			float clampResult32 = clamp( ( temp_output_30_0 * 20.0 ) , 0.0 , 1.0 );
			o.Alpha = clampResult32;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16900
275;1044;1906;1008;623.8503;798.2939;1.465761;True;False
Node;AmplifyShaderEditor.UnityProjectorClipMatrixNode;4;-1336.696,-132.087;Float;False;0;1;FLOAT4x4;0
Node;AmplifyShaderEditor.PosVertexDataNode;6;-1336.696,-52.08704;Float;False;1;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PosVertexDataNode;1;-1336.696,-436.087;Float;False;1;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.UnityProjectorMatrixNode;2;-1336.696,-516.087;Float;False;0;1;FLOAT4x4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;9;-1128.696,-132.087;Float;False;2;2;0;FLOAT4x4;0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;3;-1128.696,-516.087;Float;False;2;2;0;FLOAT4x4;0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.VertexToFragmentNode;10;-984.6957,-132.087;Float;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.VertexToFragmentNode;5;-984.6957,-516.087;Float;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ComponentMaskNode;8;-744.6957,-516.087;Float;False;True;True;False;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ComponentMaskNode;7;-744.6957,-436.087;Float;False;False;False;False;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;14;-744.6957,-52.08704;Float;False;False;False;False;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;15;-744.6957,-132.087;Float;False;True;True;False;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;12;-504.6957,-516.087;Float;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;18;-504.6957,-132.087;Float;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;16;-360.6957,-516.087;Float;True;Property;_LightTex;LightTex;0;0;Create;True;0;0;False;0;None;dd02b49d9c81fd441bb22c39e530e13c;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;20;-335.9595,-125.4907;Float;True;Property;_FalloffTex;FalloffTex;1;0;Create;True;0;0;False;0;62010525e97e86644b7706ffa8f3a162;62010525e97e86644b7706ffa8f3a162;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;30;175.5356,-213.2997;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;39;38.63904,429.7069;Float;True;Property;_TextureSample0;Texture Sample 0;2;0;Create;True;0;0;False;0;b0a40f304ba3d5c42a00c73644c4220d;None;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;31;352.0526,-128.0805;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;20;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;17;20.63214,-410.2982;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;41;438.1841,412.0402;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;34;844.4155,-190.063;Float;False;Constant;_Float0;Float 0;2;0;Create;True;0;0;False;0;0.3;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;21;312.2443,-493.928;Float;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;35;878.4155,-104.063;Float;False;Constant;_Float1;Float 1;2;0;Create;True;0;0;False;0;0.1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;32;597.2747,-44.93622;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;47;653.6448,409.4201;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;20,20,20;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;22;1118.1,-219.1999;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;DecalShaderNormal;False;False;False;False;True;True;True;True;True;True;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0;True;False;0;False;Transparent;;Transparent;ForwardOnly;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;9;0;4;0
WireConnection;9;1;6;0
WireConnection;3;0;2;0
WireConnection;3;1;1;0
WireConnection;10;0;9;0
WireConnection;5;0;3;0
WireConnection;8;0;5;0
WireConnection;7;0;5;0
WireConnection;14;0;10;0
WireConnection;15;0;10;0
WireConnection;12;0;8;0
WireConnection;12;1;7;0
WireConnection;18;0;15;0
WireConnection;18;1;14;0
WireConnection;16;1;12;0
WireConnection;20;1;18;0
WireConnection;30;0;16;4
WireConnection;30;1;20;4
WireConnection;39;1;12;0
WireConnection;31;0;30;0
WireConnection;17;0;16;4
WireConnection;41;0;39;0
WireConnection;41;1;30;0
WireConnection;21;0;16;0
WireConnection;21;3;17;0
WireConnection;32;0;31;0
WireConnection;47;0;41;0
WireConnection;22;0;21;0
WireConnection;22;1;47;0
WireConnection;22;3;35;0
WireConnection;22;4;34;0
WireConnection;22;9;32;0
ASEEND*/
//CHKSM=DE18ED7420AC6F5368B019EF042715DF0B3C0134