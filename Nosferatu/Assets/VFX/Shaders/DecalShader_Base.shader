// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "DecalShader"
{
	Properties
	{
		_LightTex("LightTex", 2D) = "white" {}
		_FalloffTex("FalloffTex", 2D) = "white" {}
		[HDR]_DecalColor("Decal Color", Color) = (0,0,0,0)
		_Smoothness("Smoothness", Float) = 0.2
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		Blend DstColor Zero
		
		AlphaToMask On
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha exclude_path:deferred nolightmap  nodynlightmap nofog vertex:vertexDataFunc 
		struct Input
		{
			float4 vertexToFrag5;
			float4 vertexToFrag10;
		};

		uniform float4 _DecalColor;
		uniform sampler2D _LightTex;
		float4x4 unity_Projector;
		uniform sampler2D _FalloffTex;
		float4x4 unity_ProjectorClip;
		uniform float _GlobalSaturation;
		uniform float _Smoothness;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float4 ase_vertex4Pos = v.vertex;
			o.vertexToFrag5 = mul( unity_Projector, ase_vertex4Pos );
			o.vertexToFrag10 = mul( unity_ProjectorClip, ase_vertex4Pos );
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 temp_output_12_0 = ( (i.vertexToFrag5).xy / (i.vertexToFrag5).w );
			float4 tex2DNode16 = tex2D( _LightTex, temp_output_12_0 );
			float4 appendResult21 = (float4(tex2DNode16.rgb , ( 1.0 - tex2DNode16.a )));
			float4 temp_output_64_0 = ( _DecalColor * appendResult21 );
			o.Albedo = temp_output_64_0.rgb;
			float4 tex2DNode20 = tex2D( _FalloffTex, ( (i.vertexToFrag10).xy / (i.vertexToFrag10).w ) );
			float ifLocalVar60 = 0;
			if( tex2DNode20.a <= 0.1 )
				ifLocalVar60 = 0.0;
			else
				ifLocalVar60 = tex2DNode20.a;
			float clampResult32 = clamp( ( ( tex2DNode16.a * ifLocalVar60 ) * 3.0 ) , 0.0 , 1.0 );
			float temp_output_37_0 = ( clampResult32 * 0.95 );
			o.Emission = ( temp_output_64_0 * ( ( tex2DNode16 * temp_output_37_0 ) + ( _GlobalSaturation * 3.0 ) ) ).rgb;
			o.Smoothness = _Smoothness;
			o.Alpha = ( _DecalColor.a * temp_output_37_0 );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16900
-1920;0;1920;1019;383.9681;922.4659;1;True;False
Node;AmplifyShaderEditor.UnityProjectorClipMatrixNode;4;-1336.696,-132.087;Float;False;0;1;FLOAT4x4;0
Node;AmplifyShaderEditor.PosVertexDataNode;6;-1336.696,-52.08704;Float;False;1;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.UnityProjectorMatrixNode;2;-1330.296,-519.2869;Float;False;0;1;FLOAT4x4;0
Node;AmplifyShaderEditor.PosVertexDataNode;1;-1336.696,-436.087;Float;False;1;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;9;-1128.696,-132.087;Float;False;2;2;0;FLOAT4x4;0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.VertexToFragmentNode;10;-984.6957,-132.087;Float;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;3;-1128.696,-516.087;Float;False;2;2;0;FLOAT4x4;0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.VertexToFragmentNode;5;-984.6957,-516.087;Float;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ComponentMaskNode;15;-744.6957,-132.087;Float;False;True;True;False;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ComponentMaskNode;14;-744.6957,-52.08704;Float;False;False;False;False;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;8;-744.6957,-516.087;Float;False;True;True;False;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ComponentMaskNode;7;-744.6957,-436.087;Float;False;False;False;False;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;18;-504.6957,-132.087;Float;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;61;-206.3122,87.50582;Float;False;Constant;_Float0;Float 0;4;0;Create;True;0;0;False;0;0.1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;20;-335.9595,-125.4907;Float;True;Property;_FalloffTex;FalloffTex;2;0;Create;True;0;0;False;0;None;62010525e97e86644b7706ffa8f3a162;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;62;-140.3735,166.5174;Float;False;Constant;_Float2;Float 2;4;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;12;-504.6957,-516.087;Float;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;16;-360.6957,-516.087;Float;True;Property;_LightTex;LightTex;0;0;Create;True;0;0;False;0;None;db3fab5093c2a1b47a6ebc657c7bdcd1;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ConditionalIfNode;60;43.45636,32.15937;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;30;175.5356,-213.2997;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;31;248.7472,-102.4105;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;3;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;32;471.7227,-6.481163;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;38;662.9985,116.2334;Float;False;Constant;_Float1;Float 1;3;0;Create;True;0;0;False;0;0.95;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;56;169.003,-742.8411;Float;False;Global;_GlobalSaturation;_GlobalSaturation;4;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;37;834.6979,7.833347;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;17;20.63214,-410.2982;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;57;399.3192,-730.5718;Float;False;2;2;0;FLOAT;5;False;1;FLOAT;3;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;58;969.1132,-261.5623;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;66;756.5941,-616.8717;Float;False;Property;_DecalColor;Decal Color;5;1;[HDR];Create;True;0;0;False;0;0,0,0,0;1,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;21;312.2443,-493.928;Float;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;64;1124.133,-450.6338;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;70;1152.517,-259.5933;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;67;1479.698,-19.36474;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;53;-335.4528,-857.4617;Float;True;Property;_NormalMap;NormalMap;3;0;Create;True;0;0;False;0;None;7f88e6d52aebbde478965cda3d40c9f6;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;63;1175.893,-87.2639;Float;False;Property;_Smoothness;Smoothness;6;0;Create;True;0;0;False;0;0.2;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;71;1360.717,-272.8933;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;54;-318.6264,-1060.704;Float;True;Property;_SmoothnessMap;SmoothnessMap;4;0;Create;True;0;0;False;0;None;None;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;22;1691.879,-224.4708;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;DecalShader;False;False;False;False;False;False;True;True;False;True;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0;True;False;0;True;Transparent;;Transparent;ForwardOnly;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;6;2;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;1;-1;-1;-1;0;True;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;9;0;4;0
WireConnection;9;1;6;0
WireConnection;10;0;9;0
WireConnection;3;0;2;0
WireConnection;3;1;1;0
WireConnection;5;0;3;0
WireConnection;15;0;10;0
WireConnection;14;0;10;0
WireConnection;8;0;5;0
WireConnection;7;0;5;0
WireConnection;18;0;15;0
WireConnection;18;1;14;0
WireConnection;20;1;18;0
WireConnection;12;0;8;0
WireConnection;12;1;7;0
WireConnection;16;1;12;0
WireConnection;60;0;20;4
WireConnection;60;1;61;0
WireConnection;60;2;20;4
WireConnection;60;3;62;0
WireConnection;60;4;62;0
WireConnection;30;0;16;4
WireConnection;30;1;60;0
WireConnection;31;0;30;0
WireConnection;32;0;31;0
WireConnection;37;0;32;0
WireConnection;37;1;38;0
WireConnection;17;0;16;4
WireConnection;57;0;56;0
WireConnection;58;0;16;0
WireConnection;58;1;37;0
WireConnection;21;0;16;0
WireConnection;21;3;17;0
WireConnection;64;0;66;0
WireConnection;64;1;21;0
WireConnection;70;0;58;0
WireConnection;70;1;57;0
WireConnection;67;0;66;4
WireConnection;67;1;37;0
WireConnection;53;1;12;0
WireConnection;71;0;64;0
WireConnection;71;1;70;0
WireConnection;54;1;12;0
WireConnection;22;0;64;0
WireConnection;22;2;71;0
WireConnection;22;4;63;0
WireConnection;22;9;67;0
ASEEND*/
//CHKSM=2931544280156E107E862C20A064105B0393D9AA