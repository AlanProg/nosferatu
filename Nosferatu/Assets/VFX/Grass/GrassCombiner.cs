﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

[ExecuteInEditMode]
public class GrassCombiner : MonoBehaviour
{
    [SerializeField] private GameObject LODGrass;
    private GameObject parentLOD;
    private CombineInstance[] combine;
    private MeshFilter[] meshfilters;

    private Transform pivot;


    public bool combineNow = false;

    private GameObject parentObj, actualMommaObj;
    // Start is called before the first frame update

    // Update is called once per frame
    private void Reset()
    {
        LODGrass = AssetDatabase.LoadAssetAtPath<GameObject>( "Assets/VFX/Grass/LODGrass.prefab" );
        if(LODGrass == null)
        {
            print("Não foi encontrado o LODGrass");
        }
    }

    void Update()
    {
      if(combineNow == true)
        {
            combineNow = false;
            StartCoroutine("Merge1000");
        }  
    }

    IEnumerator Merge1000()
    {
       // print(Selection.gameObjects.Length);
        for (int a = 0; a < Mathf.Ceil((float)Selection.gameObjects.Length / 1000f); a++)
        {
            GameObject[] selections;
            if (a == (Mathf.Ceil((float)Selection.gameObjects.Length / 1000f)-1))
            {
                selections = new GameObject[Selection.gameObjects.Length - ((a * 1000))];

            }
            else
            {
                selections = new GameObject[1000];

                
            }
            for (int c = 0; c < selections.Length; c++)
            {
               // print(c + "  " + a);
                selections[c] = Selection.gameObjects[c + (a * 1000)];
            }

            CreateLodGroup(selections.Length,selections);
            yield return new WaitForSeconds(1);
        }
    }

    void CreateLodGroup(int length, GameObject[] mergers)
    {
     
        //if (Selection.gameObjects.Length > 1000)
        //{
        //    EditorUtility.DisplayDialog("Erro",Selection.gameObjects.Length + " Objetos detectados," +  "Limite-se  a 1000 gramas por merge, mais que isso causara bugs", "Ok");
        //    return;

       // }

        pivot = Selection.gameObjects[0].transform;

        actualMommaObj = new GameObject();
        actualMommaObj.transform.position = pivot.position;
        actualMommaObj.transform.rotation = pivot.rotation;

        foreach(GameObject o in mergers)
        {
            o.transform.parent = actualMommaObj.transform;
        }

        parentObj = new GameObject();
        parentObj.transform.position = pivot.position;
        parentObj.transform.rotation = pivot.rotation;
        actualMommaObj.transform.parent = parentObj.transform;


        combine = new CombineInstance[length];
        meshfilters = actualMommaObj.GetComponentsInChildren<MeshFilter>();


        parentLOD = new GameObject();
        parentLOD.transform.position = pivot.position;
        parentLOD.transform.rotation = pivot.rotation;


        //parentLOD.transform.localScale = transform.localScale;

        int i = 0;
        GameObject currentGrass;
        //print(meshfilters.Length);

        
        while (i < meshfilters.Length)
        {
            currentGrass = Instantiate(LODGrass,actualMommaObj.transform.GetChild(i));
            currentGrass.transform.parent = parentLOD.transform;
            i++;
        }

        MergeMesh(parentLOD);
        MergeMesh(actualMommaObj);
        


        LODGroup lod = parentObj.AddComponent<LODGroup>();
        LOD[] lods = new LOD[3];
        Renderer[] rend = new Renderer[1];
        rend[0] = GetComponent<Renderer>();
        lods[0] = new LOD(0.5f,rend);

        Renderer[] rend2 = new Renderer[1];
        rend2[0] = parentLOD.GetComponent<Renderer>();
        lods[1] = new LOD(0.3f, rend2);

        lods[2] = new LOD(0.1f, rend2);

        lod.SetLODs(lods);
        lod.RecalculateBounds();

        //EditorUtility.DisplayDialog("Boa sr.Romulo", meshfilters.Length + " Gramas foram mergeadas, salvas em VFX/Grass/CombinedMeshes", "Ok");

    }

    void MergeMesh(GameObject objtomerge)
    {
        meshfilters = objtomerge.GetComponentsInChildren<MeshFilter>();
        objtomerge.AddComponent<MeshFilter>();
        objtomerge.AddComponent<MeshRenderer>();


        //print(meshfilters.Length);
        int i = 0;

        Vector3 oldPos = pivot.position;
        Quaternion oldRot = pivot.rotation;
        objtomerge.transform.position = Vector3.zero;
        objtomerge.transform.rotation = Quaternion.identity;

        while (i < meshfilters.Length)
        {
            combine[i].subMeshIndex = 0;
            combine[i].mesh = meshfilters[i].sharedMesh;
            combine[i].transform = meshfilters[i].transform.localToWorldMatrix;
            meshfilters[i].gameObject.SetActive(false);

            i++;
        }


        Mesh newMesh = objtomerge.GetComponent<MeshFilter>().sharedMesh = new Mesh();
        newMesh.name = "LOD_MESH" + GetInstanceID();
        objtomerge.GetComponent<MeshFilter>().sharedMesh.CombineMeshes(combine);
        objtomerge.GetComponent<MeshRenderer>().sharedMaterial = objtomerge.transform.GetChild(1).GetComponent<MeshRenderer>().sharedMaterial;

        objtomerge.transform.position = oldPos;
        objtomerge.transform.rotation = oldRot;

        objtomerge.transform.parent = parentObj.transform;

        objtomerge.isStatic = true;

        string combinedMeshesPath = "Assets/VFX/Grass/CombinedMeshes/";
        Directory.CreateDirectory(combinedMeshesPath);

        AssetDatabase.CreateAsset(objtomerge.GetComponent<MeshFilter>().sharedMesh, combinedMeshesPath + "CombinedMesh" + objtomerge.GetInstanceID() + Random.Range(1, 100) + ".asset");
        AssetDatabase.SaveAssets();


    }
}
