// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "BoW/LitSature_Statue"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_SaturationMinimum("Saturation Minimum", Range( 0 , 1)) = 0
		[Toggle]_DessaturarGlobal("DessaturarGlobal", Float) = 0
		_Albedo("Albedo", 2D) = "white" {}
		_AlbedoColor("Albedo Color", Color) = (1,1,1,0)
		[Normal]_NormalMap("NormalMap", 2D) = "bump" {}
		_NormalMult("NormalMult", Range( 0 , 1)) = 1
		_Rougness("Rougness", 2D) = "white" {}
		_Mettalic("Mettalic", 2D) = "white" {}
		_Occlusion("Occlusion", 2D) = "white" {}
		_RoughnessMult("RoughnessMult", Float) = 0
		_MettalicMult("MettalicMult", Float) = 1
		[Toggle]_Triplanar("Triplanar", Float) = 0
		_TriplanarTiling("Triplanar Tiling", Vector) = (1,1,0,0)
		[Toggle]_RMAOE("RMAOE?", Float) = 1
		_RMAOE_Texture("RMAOE_Texture", 2D) = "white" {}
		[HDR]_EmmisionColor("EmmisionColor", Color) = (0,0,0,0)
		_Metallic_MULT("Metallic_MULT", Range( 0 , 1)) = 1
		_Roughness_MULT("Roughness_MULT", Range( 0 , 1)) = 0.1934139
		_GeneralEmission("General Emission", Float) = 0
		_Dissolve("_Dissolve", Range( 0 , 1)) = 0
		_DarknessTint("_DarknessTint", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "AlphaTest+0" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#define ASE_TEXTURE_PARAMS(textureName) textureName

		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform float _GeneralEmission;
		uniform float4 _EmmisionColor;
		uniform float _Triplanar;
		uniform sampler2D _Rougness;
		uniform float4 _Rougness_ST;
		uniform float2 _TriplanarTiling;
		uniform float _RoughnessMult;
		uniform sampler2D _Mettalic;
		uniform float4 _Mettalic_ST;
		uniform float _MettalicMult;
		uniform sampler2D _Occlusion;
		uniform float4 _Occlusion_ST;
		uniform sampler2D _RMAOE_Texture;
		uniform float4 _RMAOE_Texture_ST;
		uniform float _RMAOE;
		uniform float _DarknessTint;
		uniform float _Dissolve;
		uniform sampler2D _NormalMap;
		uniform float4 _NormalMap_ST;
		uniform float _NormalMult;
		uniform float _Metallic_MULT;
		uniform float _Roughness_MULT;
		uniform float4 _AlbedoColor;
		uniform float _DessaturarGlobal;
		uniform float _GlobalSaturation;
		uniform float _SaturationMinimum;
		uniform float _Cutoff = 0.5;


		inline float4 TriplanarSamplingSF( sampler2D topTexMap, float3 worldPos, float3 worldNormal, float falloff, float2 tiling, float3 normalScale, float3 index )
		{
			float3 projNormal = ( pow( abs( worldNormal ), falloff ) );
			projNormal /= ( projNormal.x + projNormal.y + projNormal.z ) + 0.00001;
			float3 nsign = sign( worldNormal );
			half4 xNorm; half4 yNorm; half4 zNorm;
			xNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.zy * float2( nsign.x, 1.0 ) ) );
			yNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.xz * float2( nsign.y, 1.0 ) ) );
			zNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.xy * float2( -nsign.z, 1.0 ) ) );
			return xNorm * projNormal.x + yNorm * projNormal.y + zNorm * projNormal.z;
		}


		float3 mod3D289( float3 x ) { return x - floor( x / 289.0 ) * 289.0; }

		float4 mod3D289( float4 x ) { return x - floor( x / 289.0 ) * 289.0; }

		float4 permute( float4 x ) { return mod3D289( ( x * 34.0 + 1.0 ) * x ); }

		float4 taylorInvSqrt( float4 r ) { return 1.79284291400159 - r * 0.85373472095314; }

		float snoise( float3 v )
		{
			const float2 C = float2( 1.0 / 6.0, 1.0 / 3.0 );
			float3 i = floor( v + dot( v, C.yyy ) );
			float3 x0 = v - i + dot( i, C.xxx );
			float3 g = step( x0.yzx, x0.xyz );
			float3 l = 1.0 - g;
			float3 i1 = min( g.xyz, l.zxy );
			float3 i2 = max( g.xyz, l.zxy );
			float3 x1 = x0 - i1 + C.xxx;
			float3 x2 = x0 - i2 + C.yyy;
			float3 x3 = x0 - 0.5;
			i = mod3D289( i);
			float4 p = permute( permute( permute( i.z + float4( 0.0, i1.z, i2.z, 1.0 ) ) + i.y + float4( 0.0, i1.y, i2.y, 1.0 ) ) + i.x + float4( 0.0, i1.x, i2.x, 1.0 ) );
			float4 j = p - 49.0 * floor( p / 49.0 );  // mod(p,7*7)
			float4 x_ = floor( j / 7.0 );
			float4 y_ = floor( j - 7.0 * x_ );  // mod(j,N)
			float4 x = ( x_ * 2.0 + 0.5 ) / 7.0 - 1.0;
			float4 y = ( y_ * 2.0 + 0.5 ) / 7.0 - 1.0;
			float4 h = 1.0 - abs( x ) - abs( y );
			float4 b0 = float4( x.xy, y.xy );
			float4 b1 = float4( x.zw, y.zw );
			float4 s0 = floor( b0 ) * 2.0 + 1.0;
			float4 s1 = floor( b1 ) * 2.0 + 1.0;
			float4 sh = -step( h, 0.0 );
			float4 a0 = b0.xzyw + s0.xzyw * sh.xxyy;
			float4 a1 = b1.xzyw + s1.xzyw * sh.zzww;
			float3 g0 = float3( a0.xy, h.x );
			float3 g1 = float3( a0.zw, h.y );
			float3 g2 = float3( a1.xy, h.z );
			float3 g3 = float3( a1.zw, h.w );
			float4 norm = taylorInvSqrt( float4( dot( g0, g0 ), dot( g1, g1 ), dot( g2, g2 ), dot( g3, g3 ) ) );
			g0 *= norm.x;
			g1 *= norm.y;
			g2 *= norm.z;
			g3 *= norm.w;
			float4 m = max( 0.6 - float4( dot( x0, x0 ), dot( x1, x1 ), dot( x2, x2 ), dot( x3, x3 ) ), 0.0 );
			m = m* m;
			m = m* m;
			float4 px = float4( dot( x0, g0 ), dot( x1, g1 ), dot( x2, g2 ), dot( x3, g3 ) );
			return 42.0 * dot( m, px);
		}


		inline float3 TriplanarSamplingSNF( sampler2D topTexMap, float3 worldPos, float3 worldNormal, float falloff, float2 tiling, float3 normalScale, float3 index )
		{
			float3 projNormal = ( pow( abs( worldNormal ), falloff ) );
			projNormal /= ( projNormal.x + projNormal.y + projNormal.z ) + 0.00001;
			float3 nsign = sign( worldNormal );
			half4 xNorm; half4 yNorm; half4 zNorm;
			xNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.zy * float2( nsign.x, 1.0 ) ) );
			yNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.xz * float2( nsign.y, 1.0 ) ) );
			zNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.xy * float2( -nsign.z, 1.0 ) ) );
			xNorm.xyz = half3( UnpackNormal( xNorm ).xy * float2( nsign.x, 1.0 ) + worldNormal.zy, worldNormal.x ).zyx;
			yNorm.xyz = half3( UnpackNormal( yNorm ).xy * float2( nsign.y, 1.0 ) + worldNormal.xz, worldNormal.y ).xzy;
			zNorm.xyz = half3( UnpackNormal( zNorm ).xy * float2( -nsign.z, 1.0 ) + worldNormal.xy, worldNormal.z ).xyz;
			return normalize( xNorm.xyz * projNormal.x + yNorm.xyz * projNormal.y + zNorm.xyz * projNormal.z );
		}


		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#ifdef UNITY_PASS_FORWARDBASE
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			#if defined(HANDLE_SHADOWS_BLENDING_IN_GI)
			half bakedAtten = UnitySampleBakedOcclusion(data.lightmapUV.xy, data.worldPos);
			float zDist = dot(_WorldSpaceCameraPos - data.worldPos, UNITY_MATRIX_V[2].xyz);
			float fadeDist = UnityComputeShadowFadeDistance(data.worldPos, zDist);
			ase_lightAtten = UnityMixRealtimeAndBakedShadows(data.atten, bakedAtten, UnityComputeShadowFade(fadeDist));
			#endif
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float simplePerlin3D218 = snoise( ( ase_vertex3Pos * float3( 6,6,6 ) ) );
			float simplePerlin3D208 = snoise( ( ( simplePerlin3D218 * 0.2 ) + ( ase_vertex3Pos * float3( 4,4,4 ) ) ) );
			float clampResult212 = clamp( simplePerlin3D208 , 0.0 , 1.0 );
			SurfaceOutputStandard s26_g59 = (SurfaceOutputStandard ) 0;
			s26_g59.Albedo = float3( 0,0,0 );
			float2 uv_NormalMap = i.uv_texcoord * _NormalMap_ST.xy + _NormalMap_ST.zw;
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float3 ase_worldTangent = WorldNormalVector( i, float3( 1, 0, 0 ) );
			float3 ase_worldBitangent = WorldNormalVector( i, float3( 0, 1, 0 ) );
			float3x3 ase_worldToTangent = float3x3( ase_worldTangent, ase_worldBitangent, ase_worldNormal );
			float3 triplanar175 = TriplanarSamplingSNF( _NormalMap, ase_worldPos, ase_worldNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float3 tanTriplanarNormal175 = mul( ase_worldToTangent, triplanar175 );
			float3 temp_output_20_0_g59 = ( (( lerp(0.0,1.0,_Triplanar) == 0.0 ) ? UnpackNormal( tex2D( _NormalMap, uv_NormalMap ) ) :  tanTriplanarNormal175 ) * _NormalMult );
			s26_g59.Normal = WorldNormalVector( i , temp_output_20_0_g59 );
			s26_g59.Emission = float3( 0,0,0 );
			float2 uv_Rougness = i.uv_texcoord * _Rougness_ST.xy + _Rougness_ST.zw;
			float4 triplanar182 = TriplanarSamplingSF( _Rougness, ase_worldPos, ase_worldNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float2 uv_Mettalic = i.uv_texcoord * _Mettalic_ST.xy + _Mettalic_ST.zw;
			float4 triplanar171 = TriplanarSamplingSF( _Mettalic, ase_worldPos, ase_worldNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float2 uv_Occlusion = i.uv_texcoord * _Occlusion_ST.xy + _Occlusion_ST.zw;
			float4 triplanar169 = TriplanarSamplingSF( _Occlusion, ase_worldPos, ase_worldNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float4 appendResult188 = (float4(( ( 1.0 - (( lerp(0.0,1.0,_Triplanar) == 0.0 ) ? tex2D( _Rougness, uv_Rougness ).r :  triplanar182.x ) ) * _RoughnessMult ) , ( (( lerp(0.0,1.0,_Triplanar) == 0.0 ) ? tex2D( _Mettalic, uv_Mettalic ).r :  triplanar171.x ) * _MettalicMult ) , (( lerp(0.0,1.0,_Triplanar) == 0.0 ) ? tex2D( _Occlusion, uv_Occlusion ).r :  triplanar169.x ) , 0.0));
			float2 uv_RMAOE_Texture = i.uv_texcoord * _RMAOE_Texture_ST.xy + _RMAOE_Texture_ST.zw;
			float4 tex2DNode187 = tex2D( _RMAOE_Texture, uv_RMAOE_Texture );
			float4 appendResult190 = (float4(( 1.0 - tex2DNode187.r ) , tex2DNode187.g , tex2DNode187.b , tex2DNode187.a));
			float4 lerpResult191 = lerp( appendResult188 , appendResult190 , lerp(0.0,1.0,_RMAOE));
			float4 break193 = lerpResult191;
			float temp_output_32_0_g59 = ( break193.y * _Metallic_MULT );
			s26_g59.Metallic = temp_output_32_0_g59;
			float temp_output_28_0_g59 = ( break193.x * _Roughness_MULT );
			s26_g59.Smoothness = temp_output_28_0_g59;
			float temp_output_33_0_g59 = break193.z;
			s26_g59.Occlusion = temp_output_33_0_g59;

			data.light = gi.light;

			UnityGI gi26_g59 = gi;
			#ifdef UNITY_PASS_FORWARDBASE
			Unity_GlossyEnvironmentData g26_g59 = UnityGlossyEnvironmentSetup( s26_g59.Smoothness, data.worldViewDir, s26_g59.Normal, float3(0,0,0));
			gi26_g59 = UnityGlobalIllumination( data, s26_g59.Occlusion, s26_g59.Normal, g26_g59 );
			#endif

			float3 surfResult26_g59 = LightingStandard ( s26_g59, viewDir, gi26_g59 ).rgb;
			surfResult26_g59 += s26_g59.Emission;

			#ifdef UNITY_PASS_FORWARDADD//26_g59
			surfResult26_g59 -= s26_g59.Emission;
			#endif//26_g59
			float grayscale34_g59 = Luminance(surfResult26_g59);
			float clampResult12_g59 = clamp( step( 0.0001 , grayscale34_g59 ) , 0.0 , 1.0 );
			SurfaceOutputStandard s27_g59 = (SurfaceOutputStandard ) 0;
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float4 tex2DNode4 = tex2D( _Albedo, uv_Albedo );
			float4 triplanar184 = TriplanarSamplingSF( _Albedo, ase_worldPos, ase_worldNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			s27_g59.Albedo = ( (( lerp(0.0,1.0,_Triplanar) == 0.0 ) ? tex2DNode4 :  triplanar184 ) * _AlbedoColor ).rgb;
			s27_g59.Normal = WorldNormalVector( i , temp_output_20_0_g59 );
			s27_g59.Emission = float3( 0,0,0 );
			s27_g59.Metallic = temp_output_32_0_g59;
			s27_g59.Smoothness = temp_output_28_0_g59;
			s27_g59.Occlusion = temp_output_33_0_g59;

			data.light = gi.light;

			UnityGI gi27_g59 = gi;
			#ifdef UNITY_PASS_FORWARDBASE
			Unity_GlossyEnvironmentData g27_g59 = UnityGlossyEnvironmentSetup( s27_g59.Smoothness, data.worldViewDir, s27_g59.Normal, float3(0,0,0));
			gi27_g59 = UnityGlobalIllumination( data, s27_g59.Occlusion, s27_g59.Normal, g27_g59 );
			#endif

			float3 surfResult27_g59 = LightingStandard ( s27_g59, viewDir, gi27_g59 ).rgb;
			surfResult27_g59 += s27_g59.Emission;

			#ifdef UNITY_PASS_FORWARDADD//27_g59
			surfResult27_g59 -= s27_g59.Emission;
			#endif//27_g59
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = normalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float dotResult3_g59 = dot( ase_worldlightDir , (WorldNormalVector( i , temp_output_20_0_g59 )) );
			float temp_output_7_0_g59 = ( ase_lightAtten * dotResult3_g59 );
			float ifLocalVar8_g59 = 0;
			UNITY_BRANCH 
			if( _WorldSpaceLightPos0.w <= 0.5 )
				ifLocalVar8_g59 = 1.0;
			else
				ifLocalVar8_g59 = temp_output_7_0_g59;
			float clampResult37_g59 = clamp( ( lerp(_GlobalSaturation,-_GlobalSaturation,_DessaturarGlobal) + ifLocalVar8_g59 ) , 0.0 , ( 1.0 - _SaturationMinimum ) );
			float3 desaturateInitialColor15_g59 = surfResult27_g59;
			float desaturateDot15_g59 = dot( desaturateInitialColor15_g59, float3( 0.299, 0.587, 0.114 ));
			float3 desaturateVar15_g59 = lerp( desaturateInitialColor15_g59, desaturateDot15_g59.xxx, clampResult37_g59 );
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			float3 lerpResult13_g59 = lerp( float3( 0,0,0 ) , desaturateVar15_g59 , ( ase_lightColor * temp_output_7_0_g59 ).rgb);
			float temp_output_235_0 = ( 1.0 - _DarknessTint );
			float3 lerpResult234 = lerp( float3( 0,0,0 ) , ( clampResult12_g59 * lerpResult13_g59 ) , temp_output_235_0);
			c.rgb = lerpResult234;
			c.a = 1;
			clip( ( (-0.4 + (( 1.0 - _Dissolve ) - 0.0) * (1.0 - -0.4) / (1.0 - 0.0)) + clampResult212 ) - _Cutoff );
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float4 tex2DNode4 = tex2D( _Albedo, uv_Albedo );
			float2 uv_Rougness = i.uv_texcoord * _Rougness_ST.xy + _Rougness_ST.zw;
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float4 triplanar182 = TriplanarSamplingSF( _Rougness, ase_worldPos, ase_worldNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float2 uv_Mettalic = i.uv_texcoord * _Mettalic_ST.xy + _Mettalic_ST.zw;
			float4 triplanar171 = TriplanarSamplingSF( _Mettalic, ase_worldPos, ase_worldNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float2 uv_Occlusion = i.uv_texcoord * _Occlusion_ST.xy + _Occlusion_ST.zw;
			float4 triplanar169 = TriplanarSamplingSF( _Occlusion, ase_worldPos, ase_worldNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float4 appendResult188 = (float4(( ( 1.0 - (( lerp(0.0,1.0,_Triplanar) == 0.0 ) ? tex2D( _Rougness, uv_Rougness ).r :  triplanar182.x ) ) * _RoughnessMult ) , ( (( lerp(0.0,1.0,_Triplanar) == 0.0 ) ? tex2D( _Mettalic, uv_Mettalic ).r :  triplanar171.x ) * _MettalicMult ) , (( lerp(0.0,1.0,_Triplanar) == 0.0 ) ? tex2D( _Occlusion, uv_Occlusion ).r :  triplanar169.x ) , 0.0));
			float2 uv_RMAOE_Texture = i.uv_texcoord * _RMAOE_Texture_ST.xy + _RMAOE_Texture_ST.zw;
			float4 tex2DNode187 = tex2D( _RMAOE_Texture, uv_RMAOE_Texture );
			float4 appendResult190 = (float4(( 1.0 - tex2DNode187.r ) , tex2DNode187.g , tex2DNode187.b , tex2DNode187.a));
			float4 lerpResult191 = lerp( appendResult188 , appendResult190 , lerp(0.0,1.0,_RMAOE));
			float4 break193 = lerpResult191;
			float temp_output_235_0 = ( 1.0 - _DarknessTint );
			float4 lerpResult233 = lerp( float4( 0,0,0,0 ) , ( ( tex2DNode4 * _GeneralEmission ) + ( _EmmisionColor * break193.w ) ) , temp_output_235_0);
			o.Emission = lerpResult233.rgb;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				UnityGI gi;
				UNITY_INITIALIZE_OUTPUT( UnityGI, gi );
				o.Alpha = LightingStandardCustomLighting( o, worldViewDir, gi ).a;
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16900
-1920;0;1920;1019;-1633.996;-1947.741;1;True;False
Node;AmplifyShaderEditor.TexturePropertyNode;101;-1486.98,2117.027;Float;True;Property;_Rougness;Rougness;8;0;Create;True;0;0;False;0;7a170cdb7cc88024cb628cfcdbb6705c;7a170cdb7cc88024cb628cfcdbb6705c;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.Vector2Node;186;-2093.896,3635.083;Float;False;Property;_TriplanarTiling;Triplanar Tiling;14;0;Create;True;0;0;False;0;1,1;1,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.ToggleSwitchNode;178;-1168.269,3827.675;Float;False;Property;_Triplanar;Triplanar;13;0;Create;True;0;0;False;0;0;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TriplanarNode;182;-1192.31,2326.723;Float;True;Spherical;World;False;Top Texture 3;_TopTexture3;white;-1;None;Mid Texture 3;_MidTexture3;white;-1;None;Bot Texture 3;_BotTexture3;white;-1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;100;-1112.58,2128.727;Float;True;Property;_TextureSample2;Texture Sample 2;3;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexturePropertyNode;129;-1531.531,2513.01;Float;True;Property;_Mettalic;Mettalic;9;0;Create;True;0;0;False;0;7a170cdb7cc88024cb628cfcdbb6705c;7a170cdb7cc88024cb628cfcdbb6705c;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.SamplerNode;130;-1125.384,2539.264;Float;True;Property;_TextureSample3;Texture Sample 3;5;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TriplanarNode;171;-1211.882,2762.622;Float;True;Spherical;World;False;Top Texture 1;_TopTexture1;white;-1;None;Mid Texture 1;_MidTexture1;white;-1;None;Bot Texture 1;_BotTexture1;white;-1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexturePropertyNode;132;-1484.726,3274.895;Float;True;Property;_Occlusion;Occlusion;10;0;Create;True;0;0;False;0;7a170cdb7cc88024cb628cfcdbb6705c;7a170cdb7cc88024cb628cfcdbb6705c;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.TFHCCompareEqual;181;-616.549,2152.373;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TriplanarNode;169;-1277.894,3538.761;Float;True;Spherical;World;False;Top Texture 0;_TopTexture0;white;-1;None;Mid Texture 0;_MidTexture0;white;-1;None;Bot Texture 0;_BotTexture0;white;-1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PosVertexDataNode;209;2014.979,2083.149;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCCompareEqual;180;-625.8513,2738.483;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;144;554.6935,2381.182;Float;False;Property;_RoughnessMult;RoughnessMult;11;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;133;-1188.534,3328.448;Float;True;Property;_TextureSample4;Texture Sample 4;6;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;187;258.5562,3367.603;Float;True;Property;_RMAOE_Texture;RMAOE_Texture;16;0;Create;True;0;0;False;0;None;14bd90041089c004395f11809b77fdce;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;147;513.7634,2237.95;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;150;668.8138,2841.689;Float;False;Property;_MettalicMult;MettalicMult;12;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCCompareEqual;179;-515.0402,3501.292;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;149;873.239,2470.593;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;201;512.6358,3199.419;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;153;807.7731,2169.948;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;221;2136.609,2323.166;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;6,6,6;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ToggleSwitchNode;192;1361.603,2926.29;Float;False;Property;_RMAOE;RMAOE?;15;0;Create;True;0;0;False;0;1;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;188;1136.651,2184.396;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DynamicAppendNode;190;848.3486,3041.875;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;218;2289.609,2345.166;Float;False;Simplex3D;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;5;-1641.212,1179.564;Float;True;Property;_NormalMap;NormalMap;6;1;[Normal];Create;True;0;0;False;0;017469a3ea9ca3e4ea89e14f79a6e221;41027373a7d46af4ba12e90891554778;True;bump;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.TexturePropertyNode;3;-2017.465,1693.44;Float;True;Property;_Albedo;Albedo;4;0;Create;True;0;0;False;0;69c23a9535a4c2143bdfda7b563b993a;8cd40db7e67ecad45a8ac0e7263321ab;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;220;2610.17,2350.211;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0.2;False;1;FLOAT;0
Node;AmplifyShaderEditor.TriplanarNode;175;-1104.373,1388.394;Float;True;Spherical;World;True;Top Texture 2;_TopTexture2;white;-1;None;Mid Texture 2;_MidTexture2;white;-1;None;Bot Texture 2;_BotTexture2;white;-1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TriplanarNode;184;-1694.099,1905.723;Float;True;Spherical;World;False;Top Texture 4;_TopTexture4;white;-1;None;Mid Texture 4;_MidTexture4;white;-1;None;Bot Texture 4;_BotTexture4;white;-1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;191;1411.586,2489.055;Float;False;3;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SamplerNode;6;-1018.667,1178.891;Float;True;Property;_TextureSample1;Texture Sample 1;2;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;4;-1587.948,1701.773;Float;True;Property;_TextureSample0;Texture Sample 0;1;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;215;2439.505,2193.378;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;4,4,4;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;219;2642.609,2174.166;Float;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;195;1704.2,1374.407;Float;False;Property;_EmmisionColor;EmmisionColor;17;1;[HDR];Create;True;0;0;False;0;0,0,0,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCCompareEqual;183;-604.4841,1761.895;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;FLOAT4;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;30;219.6395,1400.081;Float;False;Property;_NormalMult;NormalMult;7;0;Create;True;0;0;False;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;198;1471.666,2200.207;Float;False;Property;_Metallic_MULT;Metallic_MULT;19;0;Create;True;0;0;False;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;200;1618.698,2357.708;Float;False;Property;_Roughness_MULT;Roughness_MULT;20;0;Create;True;0;0;False;0;0.1934139;0.1934139;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCCompareEqual;185;-537.2999,1239.979;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;156;305.3006,1994.355;Float;False;Property;_AlbedoColor;Albedo Color;5;0;Create;True;0;0;False;0;1,1,1,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;210;1935.505,2519.378;Float;False;Property;_Dissolve;_Dissolve;22;0;Create;True;0;0;False;0;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;193;1470.931,1953.307;Float;False;FLOAT4;1;0;FLOAT4;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.RangedFloatNode;206;2170.347,1433.102;Float;False;Property;_GeneralEmission;General Emission;21;0;Create;True;0;0;False;0;0;3;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;162;528.584,1016.101;Float;False;Property;_SaturationMinimum;Saturation Minimum;1;0;Create;True;0;0;False;0;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;232;1946.148,2692.32;Float;False;Property;_DarknessTint;_DarknessTint;23;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.LightAttenuation;98;-1007.292,1601.693;Float;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;205;2385.649,1356.814;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;208;2782.102,2210.74;Float;False;Simplex3D;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;29;499.0316,1260.856;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;194;2009.7,1503.107;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;199;1870.198,2339.708;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;197;1848.198,2194.708;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;159;577.2839,1852.146;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;214;2232.505,2521.378;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;213;2495.505,2502.378;Float;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-0.4;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;235;2241.684,2739.188;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;204;2411.078,1595.851;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;212;3044.505,2220.378;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;207;1997.856,1774.07;Float;False;NosferatuLighting;2;;59;b239d9f94a6547b429fbfe3bc6fe1151;0;7;44;FLOAT;0;False;33;FLOAT;0;False;32;FLOAT;0;False;28;FLOAT;0;False;23;FLOAT;0;False;20;FLOAT3;0,0,0;False;21;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;211;3214.505,2401.378;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;202;2124.573,1263.573;Float;False;Property;_GeneralEmissionColor;General Emission Color;18;1;[HDR];Create;True;0;0;False;0;0,0,0,0;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;233;2886.336,1714.338;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;234;3098.118,2016.88;Float;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;3464.868,1597.74;Float;False;True;2;Float;ASEMaterialInspector;0;0;CustomLighting;BoW/LitSature_Statue;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Masked;0.5;True;True;0;False;TransparentCutout;;AlphaTest;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;1;0,0,0,0;VertexScale;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;182;0;101;0
WireConnection;182;3;186;0
WireConnection;100;0;101;0
WireConnection;130;0;129;0
WireConnection;171;0;129;0
WireConnection;171;3;186;0
WireConnection;181;0;178;0
WireConnection;181;2;100;1
WireConnection;181;3;182;1
WireConnection;169;0;132;0
WireConnection;169;3;186;0
WireConnection;180;0;178;0
WireConnection;180;2;130;1
WireConnection;180;3;171;1
WireConnection;133;0;132;0
WireConnection;147;0;181;0
WireConnection;179;0;178;0
WireConnection;179;2;133;1
WireConnection;179;3;169;1
WireConnection;149;0;180;0
WireConnection;149;1;150;0
WireConnection;201;0;187;1
WireConnection;153;0;147;0
WireConnection;153;1;144;0
WireConnection;221;0;209;0
WireConnection;188;0;153;0
WireConnection;188;1;149;0
WireConnection;188;2;179;0
WireConnection;190;0;201;0
WireConnection;190;1;187;2
WireConnection;190;2;187;3
WireConnection;190;3;187;4
WireConnection;218;0;221;0
WireConnection;220;0;218;0
WireConnection;175;0;5;0
WireConnection;175;3;186;0
WireConnection;184;0;3;0
WireConnection;184;3;186;0
WireConnection;191;0;188;0
WireConnection;191;1;190;0
WireConnection;191;2;192;0
WireConnection;6;0;5;0
WireConnection;4;0;3;0
WireConnection;215;0;209;0
WireConnection;219;0;220;0
WireConnection;219;1;215;0
WireConnection;183;0;178;0
WireConnection;183;2;4;0
WireConnection;183;3;184;0
WireConnection;185;0;178;0
WireConnection;185;2;6;0
WireConnection;185;3;175;0
WireConnection;193;0;191;0
WireConnection;205;0;4;0
WireConnection;205;1;206;0
WireConnection;208;0;219;0
WireConnection;29;0;185;0
WireConnection;29;1;30;0
WireConnection;194;0;195;0
WireConnection;194;1;193;3
WireConnection;199;0;193;0
WireConnection;199;1;200;0
WireConnection;197;0;193;1
WireConnection;197;1;198;0
WireConnection;159;0;183;0
WireConnection;159;1;156;0
WireConnection;214;0;210;0
WireConnection;213;0;214;0
WireConnection;235;0;232;0
WireConnection;204;0;205;0
WireConnection;204;1;194;0
WireConnection;212;0;208;0
WireConnection;207;44;162;0
WireConnection;207;33;193;2
WireConnection;207;32;197;0
WireConnection;207;28;199;0
WireConnection;207;23;98;0
WireConnection;207;20;29;0
WireConnection;207;21;159;0
WireConnection;211;0;213;0
WireConnection;211;1;212;0
WireConnection;233;1;204;0
WireConnection;233;2;235;0
WireConnection;234;1;207;0
WireConnection;234;2;235;0
WireConnection;0;2;233;0
WireConnection;0;10;211;0
WireConnection;0;13;234;0
ASEEND*/
//CHKSM=3B60639FD268987FA1E169F90809A2C1D434EC0A