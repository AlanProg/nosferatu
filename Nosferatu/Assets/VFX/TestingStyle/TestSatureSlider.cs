﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[ExecuteInEditMode]
public class TestSatureSlider : MonoBehaviour
{   
    
    [Range(0,1)][SerializeField] private float saturationMod;
    [SerializeField] private PostProcessVolume pp;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        pp.weight = saturationMod;
        Shader.SetGlobalFloat("_GlobalSaturation",saturationMod);
    }
}
