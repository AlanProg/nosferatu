// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "BoW/LitSature"
{
	Properties
	{
		_SaturationMinimum("Saturation Minimum", Range( 0 , 1)) = 0
		[Toggle]_DessaturarGlobal("DessaturarGlobal", Float) = 0
		_Albedo("Albedo", 2D) = "white" {}
		_AlbedoColor("Albedo Color", Color) = (1,1,1,0)
		[Normal]_NormalMap("NormalMap", 2D) = "bump" {}
		_NormalMult("NormalMult", Range( 0 , 1)) = 1
		_Rougness("Rougness", 2D) = "white" {}
		_Mettalic("Mettalic", 2D) = "white" {}
		_Occlusion("Occlusion", 2D) = "white" {}
		_RoughnessMult("RoughnessMult", Float) = 0
		_MettalicMult("MettalicMult", Float) = 1
		[Toggle]_Triplanar("Triplanar", Float) = 1
		_TriplanarTiling("Triplanar Tiling", Vector) = (1,1,0,0)
		[Toggle]_IsTriplanarInObjectSpace("IsTriplanarInObjectSpace", Float) = 1
		[Toggle]_RMAOE("RMAOE?", Float) = 1
		_RMAOE_Texture("RMAOE_Texture", 2D) = "white" {}
		[HDR]_EmmisionColor("EmmisionColor", Color) = (0,0,0,0)
		_Metallic_MULT("Metallic_MULT", Range( 0 , 1)) = 1
		_Roughness_MULT("Roughness_MULT", Range( 0 , 1)) = 0.1934139
		_GeneralEmission("General Emission", Float) = 0
		_Triplanar_RMAOE("Triplanar_RMAOE", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		Blend SrcAlpha OneMinusSrcAlpha
		
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#define ASE_TEXTURE_PARAMS(textureName) textureName

		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform float _Triplanar;
		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform float _IsTriplanarInObjectSpace;
		uniform float2 _TriplanarTiling;
		uniform float _GeneralEmission;
		uniform float4 _EmmisionColor;
		uniform sampler2D _Rougness;
		uniform float4 _Rougness_ST;
		uniform float _RoughnessMult;
		uniform sampler2D _Mettalic;
		uniform float4 _Mettalic_ST;
		uniform float _MettalicMult;
		uniform sampler2D _Occlusion;
		uniform float4 _Occlusion_ST;
		uniform sampler2D _RMAOE_Texture;
		uniform float4 _RMAOE_Texture_ST;
		uniform sampler2D _Triplanar_RMAOE;
		uniform float _RMAOE;
		uniform sampler2D _NormalMap;
		uniform float4 _NormalMap_ST;
		uniform float _NormalMult;
		uniform float _Metallic_MULT;
		uniform float _Roughness_MULT;
		uniform float4 _AlbedoColor;
		uniform float _DessaturarGlobal;
		uniform float _GlobalSaturation;
		uniform float _SaturationMinimum;


		inline float4 TriplanarSamplingSF( sampler2D topTexMap, float3 worldPos, float3 worldNormal, float falloff, float2 tiling, float3 normalScale, float3 index )
		{
			float3 projNormal = ( pow( abs( worldNormal ), falloff ) );
			projNormal /= ( projNormal.x + projNormal.y + projNormal.z ) + 0.00001;
			float3 nsign = sign( worldNormal );
			half4 xNorm; half4 yNorm; half4 zNorm;
			xNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.zy * float2( nsign.x, 1.0 ) ) );
			yNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.xz * float2( nsign.y, 1.0 ) ) );
			zNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.xy * float2( -nsign.z, 1.0 ) ) );
			return xNorm * projNormal.x + yNorm * projNormal.y + zNorm * projNormal.z;
		}


		inline float3 TriplanarSamplingSNF( sampler2D topTexMap, float3 worldPos, float3 worldNormal, float falloff, float2 tiling, float3 normalScale, float3 index )
		{
			float3 projNormal = ( pow( abs( worldNormal ), falloff ) );
			projNormal /= ( projNormal.x + projNormal.y + projNormal.z ) + 0.00001;
			float3 nsign = sign( worldNormal );
			half4 xNorm; half4 yNorm; half4 zNorm;
			xNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.zy * float2( nsign.x, 1.0 ) ) );
			yNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.xz * float2( nsign.y, 1.0 ) ) );
			zNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.xy * float2( -nsign.z, 1.0 ) ) );
			xNorm.xyz = half3( UnpackNormal( xNorm ).xy * float2( nsign.x, 1.0 ) + worldNormal.zy, worldNormal.x ).zyx;
			yNorm.xyz = half3( UnpackNormal( yNorm ).xy * float2( nsign.y, 1.0 ) + worldNormal.xz, worldNormal.y ).xzy;
			zNorm.xyz = half3( UnpackNormal( zNorm ).xy * float2( -nsign.z, 1.0 ) + worldNormal.xy, worldNormal.z ).xyz;
			return normalize( xNorm.xyz * projNormal.x + yNorm.xyz * projNormal.y + zNorm.xyz * projNormal.z );
		}


		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#ifdef UNITY_PASS_FORWARDBASE
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			#if defined(HANDLE_SHADOWS_BLENDING_IN_GI)
			half bakedAtten = UnitySampleBakedOcclusion(data.lightmapUV.xy, data.worldPos);
			float zDist = dot(_WorldSpaceCameraPos - data.worldPos, UNITY_MATRIX_V[2].xyz);
			float fadeDist = UnityComputeShadowFadeDistance(data.worldPos, zDist);
			ase_lightAtten = UnityMixRealtimeAndBakedShadows(data.atten, bakedAtten, UnityComputeShadowFade(fadeDist));
			#endif
			SurfaceOutputStandard s26_g59 = (SurfaceOutputStandard ) 0;
			s26_g59.Albedo = float3( 0,0,0 );
			float2 uv_NormalMap = i.uv_texcoord * _NormalMap_ST.xy + _NormalMap_ST.zw;
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float3 ase_worldTangent = WorldNormalVector( i, float3( 1, 0, 0 ) );
			float3 ase_worldBitangent = WorldNormalVector( i, float3( 0, 1, 0 ) );
			float3x3 ase_worldToTangent = float3x3( ase_worldTangent, ase_worldBitangent, ase_worldNormal );
			float3 triplanar175 = TriplanarSamplingSNF( _NormalMap, ase_worldPos, ase_worldNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float3 tanTriplanarNormal175 = mul( ase_worldToTangent, triplanar175 );
			float4 ase_vertexTangent = mul( unity_WorldToObject, float4( ase_worldTangent, 0 ) );
			float3 ase_vertexBitangent = mul( unity_WorldToObject, float4( ase_worldBitangent, 0 ) );
			float3 ase_vertexNormal = mul( unity_WorldToObject, float4( ase_worldNormal, 0 ) );
			float3x3 objectToTangent = float3x3(ase_vertexTangent.xyz, ase_vertexBitangent, ase_vertexNormal);
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float3 triplanar225 = TriplanarSamplingSNF( _NormalMap, ase_vertex3Pos, ase_vertexNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float3 tanTriplanarNormal225 = mul( objectToTangent, triplanar225 );
			float3 temp_output_20_0_g59 = ( (( lerp(0.0,1.0,_Triplanar) == 0.0 ) ? UnpackNormal( tex2D( _NormalMap, uv_NormalMap ) ) :  (( lerp(0.0,1.0,_IsTriplanarInObjectSpace) == 0.0 ) ? tanTriplanarNormal175 :  tanTriplanarNormal225 ) ) * _NormalMult );
			s26_g59.Normal = WorldNormalVector( i , temp_output_20_0_g59 );
			s26_g59.Emission = float3( 0,0,0 );
			float2 uv_Rougness = i.uv_texcoord * _Rougness_ST.xy + _Rougness_ST.zw;
			float4 triplanar182 = TriplanarSamplingSF( _Rougness, ase_worldPos, ase_worldNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float4 triplanar221 = TriplanarSamplingSF( _Rougness, ase_vertex3Pos, ase_vertexNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float2 uv_Mettalic = i.uv_texcoord * _Mettalic_ST.xy + _Mettalic_ST.zw;
			float4 triplanar171 = TriplanarSamplingSF( _Mettalic, ase_worldPos, ase_worldNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float4 triplanar223 = TriplanarSamplingSF( _Mettalic, ase_vertex3Pos, ase_vertexNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float2 uv_Occlusion = i.uv_texcoord * _Occlusion_ST.xy + _Occlusion_ST.zw;
			float4 triplanar169 = TriplanarSamplingSF( _Occlusion, ase_worldPos, ase_worldNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float4 triplanar227 = TriplanarSamplingSF( _Occlusion, ase_vertex3Pos, ase_vertexNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float4 appendResult188 = (float4(( ( 1.0 - (( lerp(0.0,1.0,_Triplanar) == 0.0 ) ? tex2D( _Rougness, uv_Rougness ).r :  (( lerp(0.0,1.0,_IsTriplanarInObjectSpace) == 0.0 ) ? triplanar182.x :  triplanar221.x ) ) ) * _RoughnessMult ) , ( (( lerp(0.0,1.0,_Triplanar) == 0.0 ) ? tex2D( _Mettalic, uv_Mettalic ).r :  (( lerp(0.0,1.0,_IsTriplanarInObjectSpace) == 0.0 ) ? triplanar171.x :  triplanar223.x ) ) * _MettalicMult ) , (( lerp(0.0,1.0,_Triplanar) == 0.0 ) ? tex2D( _Occlusion, uv_Occlusion ).r :  (( lerp(0.0,1.0,_IsTriplanarInObjectSpace) == 0.0 ) ? triplanar169.x :  triplanar227.x ) ) , 0.0));
			float2 uv_RMAOE_Texture = i.uv_texcoord * _RMAOE_Texture_ST.xy + _RMAOE_Texture_ST.zw;
			float4 triplanar230 = TriplanarSamplingSF( _Triplanar_RMAOE, ase_worldPos, ase_worldNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float4 triplanar235 = TriplanarSamplingSF( _Triplanar_RMAOE, ase_vertex3Pos, ase_vertexNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float4 break238 = (( lerp(0.0,1.0,_Triplanar) == 0.0 ) ? tex2D( _RMAOE_Texture, uv_RMAOE_Texture ) :  (( lerp(0.0,1.0,_IsTriplanarInObjectSpace) == 0.0 ) ? triplanar230 :  triplanar235 ) );
			float4 appendResult240 = (float4(( 1.0 - break238.x ) , break238.y , break238.z , break238.w));
			float4 lerpResult191 = lerp( appendResult188 , appendResult240 , lerp(0.0,1.0,_RMAOE));
			float4 break193 = lerpResult191;
			float temp_output_32_0_g59 = ( break193.y * _Metallic_MULT );
			s26_g59.Metallic = temp_output_32_0_g59;
			float temp_output_28_0_g59 = ( break193.x * _Roughness_MULT );
			s26_g59.Smoothness = temp_output_28_0_g59;
			float temp_output_33_0_g59 = break193.z;
			s26_g59.Occlusion = temp_output_33_0_g59;

			data.light = gi.light;

			UnityGI gi26_g59 = gi;
			#ifdef UNITY_PASS_FORWARDBASE
			Unity_GlossyEnvironmentData g26_g59 = UnityGlossyEnvironmentSetup( s26_g59.Smoothness, data.worldViewDir, s26_g59.Normal, float3(0,0,0));
			gi26_g59 = UnityGlobalIllumination( data, s26_g59.Occlusion, s26_g59.Normal, g26_g59 );
			#endif

			float3 surfResult26_g59 = LightingStandard ( s26_g59, viewDir, gi26_g59 ).rgb;
			surfResult26_g59 += s26_g59.Emission;

			#ifdef UNITY_PASS_FORWARDADD//26_g59
			surfResult26_g59 -= s26_g59.Emission;
			#endif//26_g59
			float grayscale34_g59 = Luminance(surfResult26_g59);
			float clampResult12_g59 = clamp( step( 0.0001 , grayscale34_g59 ) , 0.0 , 1.0 );
			SurfaceOutputStandard s27_g59 = (SurfaceOutputStandard ) 0;
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float4 triplanar184 = TriplanarSamplingSF( _Albedo, ase_worldPos, ase_worldNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float4 triplanar217 = TriplanarSamplingSF( _Albedo, ase_vertex3Pos, ase_vertexNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float4 temp_output_183_0 = (( lerp(0.0,1.0,_Triplanar) == 0.0 ) ? tex2D( _Albedo, uv_Albedo ) :  (( lerp(0.0,1.0,_IsTriplanarInObjectSpace) == 0.0 ) ? triplanar184 :  triplanar217 ) );
			s27_g59.Albedo = ( temp_output_183_0 * _AlbedoColor ).rgb;
			s27_g59.Normal = WorldNormalVector( i , temp_output_20_0_g59 );
			s27_g59.Emission = float3( 0,0,0 );
			s27_g59.Metallic = temp_output_32_0_g59;
			s27_g59.Smoothness = temp_output_28_0_g59;
			s27_g59.Occlusion = temp_output_33_0_g59;

			data.light = gi.light;

			UnityGI gi27_g59 = gi;
			#ifdef UNITY_PASS_FORWARDBASE
			Unity_GlossyEnvironmentData g27_g59 = UnityGlossyEnvironmentSetup( s27_g59.Smoothness, data.worldViewDir, s27_g59.Normal, float3(0,0,0));
			gi27_g59 = UnityGlobalIllumination( data, s27_g59.Occlusion, s27_g59.Normal, g27_g59 );
			#endif

			float3 surfResult27_g59 = LightingStandard ( s27_g59, viewDir, gi27_g59 ).rgb;
			surfResult27_g59 += s27_g59.Emission;

			#ifdef UNITY_PASS_FORWARDADD//27_g59
			surfResult27_g59 -= s27_g59.Emission;
			#endif//27_g59
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = normalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float dotResult3_g59 = dot( ase_worldlightDir , (WorldNormalVector( i , temp_output_20_0_g59 )) );
			float temp_output_7_0_g59 = ( ase_lightAtten * dotResult3_g59 );
			float ifLocalVar8_g59 = 0;
			UNITY_BRANCH 
			if( _WorldSpaceLightPos0.w <= 0.5 )
				ifLocalVar8_g59 = 1.0;
			else
				ifLocalVar8_g59 = temp_output_7_0_g59;
			float clampResult37_g59 = clamp( ( lerp(_GlobalSaturation,-_GlobalSaturation,_DessaturarGlobal) + ifLocalVar8_g59 ) , 0.0 , ( 1.0 - _SaturationMinimum ) );
			float3 desaturateInitialColor15_g59 = surfResult27_g59;
			float desaturateDot15_g59 = dot( desaturateInitialColor15_g59, float3( 0.299, 0.587, 0.114 ));
			float3 desaturateVar15_g59 = lerp( desaturateInitialColor15_g59, desaturateDot15_g59.xxx, clampResult37_g59 );
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			float3 lerpResult13_g59 = lerp( float3( 0,0,0 ) , desaturateVar15_g59 , ( ase_lightColor * temp_output_7_0_g59 ).rgb);
			c.rgb = ( clampResult12_g59 * lerpResult13_g59 );
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float4 triplanar184 = TriplanarSamplingSF( _Albedo, ase_worldPos, ase_worldNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float3 ase_vertexNormal = mul( unity_WorldToObject, float4( ase_worldNormal, 0 ) );
			float4 triplanar217 = TriplanarSamplingSF( _Albedo, ase_vertex3Pos, ase_vertexNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float4 temp_output_183_0 = (( lerp(0.0,1.0,_Triplanar) == 0.0 ) ? tex2D( _Albedo, uv_Albedo ) :  (( lerp(0.0,1.0,_IsTriplanarInObjectSpace) == 0.0 ) ? triplanar184 :  triplanar217 ) );
			float2 uv_Rougness = i.uv_texcoord * _Rougness_ST.xy + _Rougness_ST.zw;
			float4 triplanar182 = TriplanarSamplingSF( _Rougness, ase_worldPos, ase_worldNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float4 triplanar221 = TriplanarSamplingSF( _Rougness, ase_vertex3Pos, ase_vertexNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float2 uv_Mettalic = i.uv_texcoord * _Mettalic_ST.xy + _Mettalic_ST.zw;
			float4 triplanar171 = TriplanarSamplingSF( _Mettalic, ase_worldPos, ase_worldNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float4 triplanar223 = TriplanarSamplingSF( _Mettalic, ase_vertex3Pos, ase_vertexNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float2 uv_Occlusion = i.uv_texcoord * _Occlusion_ST.xy + _Occlusion_ST.zw;
			float4 triplanar169 = TriplanarSamplingSF( _Occlusion, ase_worldPos, ase_worldNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float4 triplanar227 = TriplanarSamplingSF( _Occlusion, ase_vertex3Pos, ase_vertexNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float4 appendResult188 = (float4(( ( 1.0 - (( lerp(0.0,1.0,_Triplanar) == 0.0 ) ? tex2D( _Rougness, uv_Rougness ).r :  (( lerp(0.0,1.0,_IsTriplanarInObjectSpace) == 0.0 ) ? triplanar182.x :  triplanar221.x ) ) ) * _RoughnessMult ) , ( (( lerp(0.0,1.0,_Triplanar) == 0.0 ) ? tex2D( _Mettalic, uv_Mettalic ).r :  (( lerp(0.0,1.0,_IsTriplanarInObjectSpace) == 0.0 ) ? triplanar171.x :  triplanar223.x ) ) * _MettalicMult ) , (( lerp(0.0,1.0,_Triplanar) == 0.0 ) ? tex2D( _Occlusion, uv_Occlusion ).r :  (( lerp(0.0,1.0,_IsTriplanarInObjectSpace) == 0.0 ) ? triplanar169.x :  triplanar227.x ) ) , 0.0));
			float2 uv_RMAOE_Texture = i.uv_texcoord * _RMAOE_Texture_ST.xy + _RMAOE_Texture_ST.zw;
			float4 triplanar230 = TriplanarSamplingSF( _Triplanar_RMAOE, ase_worldPos, ase_worldNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float4 triplanar235 = TriplanarSamplingSF( _Triplanar_RMAOE, ase_vertex3Pos, ase_vertexNormal, 1.0, _TriplanarTiling, 1.0, 0 );
			float4 break238 = (( lerp(0.0,1.0,_Triplanar) == 0.0 ) ? tex2D( _RMAOE_Texture, uv_RMAOE_Texture ) :  (( lerp(0.0,1.0,_IsTriplanarInObjectSpace) == 0.0 ) ? triplanar230 :  triplanar235 ) );
			float4 appendResult240 = (float4(( 1.0 - break238.x ) , break238.y , break238.z , break238.w));
			float4 lerpResult191 = lerp( appendResult188 , appendResult240 , lerp(0.0,1.0,_RMAOE));
			float4 break193 = lerpResult191;
			o.Emission = ( ( temp_output_183_0 * _GeneralEmission ) + ( _EmmisionColor * break193.w ) ).rgb;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16900
-1920;0;1920;1019;1225.084;-240.126;2.315122;True;False
Node;AmplifyShaderEditor.Vector2Node;186;-2349.182,2817.175;Float;False;Property;_TriplanarTiling;Triplanar Tiling;13;0;Create;True;0;0;False;0;1,1;1,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TexturePropertyNode;101;-1486.98,2117.027;Float;True;Property;_Rougness;Rougness;7;0;Create;True;0;0;False;0;7a170cdb7cc88024cb628cfcdbb6705c;7a170cdb7cc88024cb628cfcdbb6705c;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.TexturePropertyNode;234;-10.6676,3517.331;Float;True;Property;_Triplanar_RMAOE;Triplanar_RMAOE;22;0;Create;True;0;0;False;0;None;fd8ec33f42e01c14cb5b0582d0ff6b0b;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.TriplanarNode;221;-1698.311,2317.303;Float;True;Spherical;Object;False;Top Texture 6;_TopTexture6;white;-1;None;Mid Texture 6;_MidTexture6;white;-1;None;Bot Texture 6;_BotTexture6;white;-1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ToggleSwitchNode;219;-2047.928,889.1372;Float;False;Property;_IsTriplanarInObjectSpace;IsTriplanarInObjectSpace;14;0;Create;True;0;0;False;0;1;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TriplanarNode;182;-1192.31,2326.723;Float;True;Spherical;World;False;Top Texture 3;_TopTexture3;white;-1;None;Mid Texture 3;_MidTexture3;white;-1;None;Bot Texture 3;_BotTexture3;white;-1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexturePropertyNode;129;-1531.531,2513.01;Float;True;Property;_Mettalic;Mettalic;8;0;Create;True;0;0;False;0;7a170cdb7cc88024cb628cfcdbb6705c;None;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.TriplanarNode;235;328.99,3788.2;Float;True;Spherical;Object;False;Top Texture 11;_TopTexture11;white;-1;None;Mid Texture 11;_MidTexture11;white;-1;None;Bot Texture 11;_BotTexture11;white;-1;None;Triplanar_RMAO;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TriplanarNode;230;324.4696,3580.435;Float;True;Spherical;World;False;Top Texture 10;_TopTexture10;white;-1;None;Mid Texture 10;_MidTexture10;white;-1;None;Bot Texture 10;_BotTexture10;white;-1;None;Triplanar_RMAO;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCCompareEqual;236;895.9996,3630.986;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT4;0,0,0,0;False;3;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TriplanarNode;171;-1211.882,2762.622;Float;True;Spherical;World;False;Top Texture 1;_TopTexture1;white;-1;None;Mid Texture 1;_MidTexture1;white;-1;None;Bot Texture 1;_BotTexture1;white;-1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCCompareEqual;222;-709.3105,2241.303;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;178;-1168.269,3827.675;Float;False;Property;_Triplanar;Triplanar;12;0;Create;True;0;0;False;0;1;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TriplanarNode;223;-1677.557,2764.713;Float;True;Spherical;Object;False;Top Texture 7;_TopTexture7;white;-1;None;Mid Texture 7;_MidTexture7;white;-1;None;Bot Texture 7;_BotTexture7;white;-1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;100;-1112.58,2128.727;Float;True;Property;_TextureSample2;Texture Sample 2;3;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;187;436.033,3274.415;Float;True;Property;_RMAOE_Texture;RMAOE_Texture;16;0;Create;True;0;0;False;0;None;fd8ec33f42e01c14cb5b0582d0ff6b0b;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexturePropertyNode;132;-1484.726,3274.895;Float;True;Property;_Occlusion;Occlusion;9;0;Create;True;0;0;False;0;7a170cdb7cc88024cb628cfcdbb6705c;None;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.TFHCCompareEqual;181;-399.549,2113.373;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCCompareEqual;224;-721.5569,2738.713;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TriplanarNode;227;-1762.294,3525.824;Float;True;Spherical;Object;False;Top Texture 9;_TopTexture9;white;-1;None;Mid Texture 9;_MidTexture9;white;-1;None;Bot Texture 9;_BotTexture9;white;-1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;130;-1125.384,2539.264;Float;True;Property;_TextureSample3;Texture Sample 3;5;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCCompareEqual;237;1151.194,3508.324;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TriplanarNode;169;-1277.894,3538.761;Float;True;Spherical;World;False;Top Texture 0;_TopTexture0;white;-1;None;Mid Texture 0;_MidTexture0;white;-1;None;Bot Texture 0;_BotTexture0;white;-1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;144;554.6935,2381.182;Float;False;Property;_RoughnessMult;RoughnessMult;10;0;Create;True;0;0;False;0;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;238;1392.623,3498.756;Float;False;FLOAT4;1;0;FLOAT4;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.OneMinusNode;147;513.7634,2237.95;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCCompareEqual;180;-424.8513,2662.483;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;150;668.8138,2841.689;Float;False;Property;_MettalicMult;MettalicMult;11;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCCompareEqual;228;-747.9217,3566.716;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;133;-1188.534,3328.448;Float;True;Property;_TextureSample4;Texture Sample 4;6;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;153;807.7731,2169.948;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCCompareEqual;179;-470.14,3433.491;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;149;873.239,2470.593;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;5;-1547.148,1064.474;Float;True;Property;_NormalMap;NormalMap;5;1;[Normal];Create;True;0;0;False;0;017469a3ea9ca3e4ea89e14f79a6e221;6287d5bdf15090e41b86f7bf96ee32b3;True;bump;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.OneMinusNode;239;1668.767,3476.719;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;3;-2120.166,1471.14;Float;True;Property;_Albedo;Albedo;3;0;Create;True;0;0;False;0;69c23a9535a4c2143bdfda7b563b993a;a57603ba46f96f9488f3ce0018a10e7e;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.TriplanarNode;217;-2297.703,1979.676;Float;True;Spherical;Object;False;Top Texture 5;_TopTexture5;white;-1;None;Mid Texture 5;_MidTexture5;white;-1;None;Bot Texture 5;_BotTexture5;white;-1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TriplanarNode;225;-1581.497,1387.23;Float;True;Spherical;Object;True;Top Texture 8;_TopTexture8;white;-1;None;Mid Texture 8;_MidTexture8;white;-1;None;Bot Texture 8;_BotTexture8;white;-1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TriplanarNode;175;-1122.373,1367.394;Float;True;Spherical;World;True;Top Texture 2;_TopTexture2;white;-1;None;Mid Texture 2;_MidTexture2;white;-1;None;Bot Texture 2;_BotTexture2;white;-1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;240;1877.26,3494.168;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TriplanarNode;184;-1776.099,1910.723;Float;True;Spherical;World;False;Top Texture 4;_TopTexture4;white;-1;None;Mid Texture 4;_MidTexture4;white;-1;None;Bot Texture 4;_BotTexture4;white;-1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ToggleSwitchNode;192;1152.303,2516.79;Float;False;Property;_RMAOE;RMAOE?;15;0;Create;True;0;0;False;0;1;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;188;1258.436,1726.778;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.LerpOp;191;1306.286,2347.355;Float;False;3;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SamplerNode;4;-1587.948,1701.773;Float;True;Property;_TextureSample0;Texture Sample 0;1;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCCompareEqual;220;-1145.54,1870.563;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT4;0,0,0,0;False;3;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TFHCCompareEqual;226;-643.5232,1369.651;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;6;-1018.667,1178.891;Float;True;Property;_TextureSample1;Texture Sample 1;2;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;156;305.3006,1994.355;Float;False;Property;_AlbedoColor;Albedo Color;4;0;Create;True;0;0;False;0;1,1,1,0;0.3018867,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;200;1539.698,2341.708;Float;False;Property;_Roughness_MULT;Roughness_MULT;20;0;Create;True;0;0;False;0;0.1934139;0.663;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;30;219.6395,1400.081;Float;False;Property;_NormalMult;NormalMult;6;0;Create;True;0;0;False;0;1;0.964;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;198;1495.066,2192.407;Float;False;Property;_Metallic_MULT;Metallic_MULT;19;0;Create;True;0;0;False;0;1;0.621;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCCompareEqual;183;-603.4841,1792.895;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;FLOAT4;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;195;1704.2,1374.407;Float;False;Property;_EmmisionColor;EmmisionColor;17;1;[HDR];Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCCompareEqual;185;-296.9589,1244.406;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;206;2170.347,1433.102;Float;False;Property;_GeneralEmission;General Emission;21;0;Create;True;0;0;False;0;0;0.39;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;193;1438.131,1936.607;Float;False;FLOAT4;1;0;FLOAT4;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;159;577.2839,1852.146;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;162;528.584,1016.101;Float;False;Property;_SaturationMinimum;Saturation Minimum;0;0;Create;True;0;0;False;0;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.LightAttenuation;98;-1007.292,1601.693;Float;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;199;1870.198,2339.708;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;197;1848.198,2194.708;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;29;499.0316,1260.856;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;194;2009.7,1503.107;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;205;2420.375,1345.239;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.FunctionNode;207;1997.856,1774.07;Float;False;NosferatuLighting;1;;59;b239d9f94a6547b429fbfe3bc6fe1151;0;7;44;FLOAT;0;False;33;FLOAT;0;False;32;FLOAT;0;False;28;FLOAT;0;False;23;FLOAT;0;False;20;FLOAT3;0,0,0;False;21;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;204;2411.078,1595.851;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;202;2124.573,1263.573;Float;False;Property;_GeneralEmissionColor;General Emission Color;18;1;[HDR];Create;True;0;0;False;0;0,0,0,0;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;2580.995,1560.655;Float;False;True;2;Float;ASEMaterialInspector;0;0;CustomLighting;BoW/LitSature;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;1;0,0,0,0;VertexScale;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;221;0;101;0
WireConnection;221;3;186;0
WireConnection;182;0;101;0
WireConnection;182;3;186;0
WireConnection;235;0;234;0
WireConnection;235;3;186;0
WireConnection;230;0;234;0
WireConnection;230;3;186;0
WireConnection;236;0;219;0
WireConnection;236;2;230;0
WireConnection;236;3;235;0
WireConnection;171;0;129;0
WireConnection;171;3;186;0
WireConnection;222;0;219;0
WireConnection;222;2;182;1
WireConnection;222;3;221;1
WireConnection;223;0;129;0
WireConnection;223;3;186;0
WireConnection;100;0;101;0
WireConnection;181;0;178;0
WireConnection;181;2;100;1
WireConnection;181;3;222;0
WireConnection;224;0;219;0
WireConnection;224;2;171;1
WireConnection;224;3;223;1
WireConnection;227;0;132;0
WireConnection;227;3;186;0
WireConnection;130;0;129;0
WireConnection;237;0;178;0
WireConnection;237;2;187;0
WireConnection;237;3;236;0
WireConnection;169;0;132;0
WireConnection;169;3;186;0
WireConnection;238;0;237;0
WireConnection;147;0;181;0
WireConnection;180;0;178;0
WireConnection;180;2;130;1
WireConnection;180;3;224;0
WireConnection;228;0;219;0
WireConnection;228;2;169;1
WireConnection;228;3;227;1
WireConnection;133;0;132;0
WireConnection;153;0;147;0
WireConnection;153;1;144;0
WireConnection;179;0;178;0
WireConnection;179;2;133;1
WireConnection;179;3;228;0
WireConnection;149;0;180;0
WireConnection;149;1;150;0
WireConnection;239;0;238;0
WireConnection;217;0;3;0
WireConnection;217;3;186;0
WireConnection;225;0;5;0
WireConnection;225;3;186;0
WireConnection;175;0;5;0
WireConnection;175;3;186;0
WireConnection;240;0;239;0
WireConnection;240;1;238;1
WireConnection;240;2;238;2
WireConnection;240;3;238;3
WireConnection;184;0;3;0
WireConnection;184;3;186;0
WireConnection;188;0;153;0
WireConnection;188;1;149;0
WireConnection;188;2;179;0
WireConnection;191;0;188;0
WireConnection;191;1;240;0
WireConnection;191;2;192;0
WireConnection;4;0;3;0
WireConnection;220;0;219;0
WireConnection;220;2;184;0
WireConnection;220;3;217;0
WireConnection;226;0;219;0
WireConnection;226;2;175;0
WireConnection;226;3;225;0
WireConnection;6;0;5;0
WireConnection;183;0;178;0
WireConnection;183;2;4;0
WireConnection;183;3;220;0
WireConnection;185;0;178;0
WireConnection;185;2;6;0
WireConnection;185;3;226;0
WireConnection;193;0;191;0
WireConnection;159;0;183;0
WireConnection;159;1;156;0
WireConnection;199;0;193;0
WireConnection;199;1;200;0
WireConnection;197;0;193;1
WireConnection;197;1;198;0
WireConnection;29;0;185;0
WireConnection;29;1;30;0
WireConnection;194;0;195;0
WireConnection;194;1;193;3
WireConnection;205;0;183;0
WireConnection;205;1;206;0
WireConnection;207;44;162;0
WireConnection;207;33;193;2
WireConnection;207;32;197;0
WireConnection;207;28;199;0
WireConnection;207;23;98;0
WireConnection;207;20;29;0
WireConnection;207;21;159;0
WireConnection;204;0;205;0
WireConnection;204;1;194;0
WireConnection;0;2;204;0
WireConnection;0;13;207;0
ASEEND*/
//CHKSM=6D221843EDCEE629802A8EC55363C6A36AE09279