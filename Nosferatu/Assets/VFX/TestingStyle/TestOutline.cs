// Amplify Shader Editor - Visual Shader Editing Tool
// Copyright (c) Amplify Creations, Lda <info@amplify.pt>
#if UNITY_POST_PROCESSING_STACK_V2
using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[Serializable]
[PostProcess( typeof( TemplatesPostProcessStackPPSRenderer ), PostProcessEvent.BeforeStack, "TestOutline", true )]
public sealed class TestOutline : PostProcessEffectSettings
{
}

public sealed class TemplatesPostProcessStackPPSRenderer : PostProcessEffectRenderer<TestOutline>
{
	public override void Render( PostProcessRenderContext context )
	{
		var sheet = context.propertySheets.Get( Shader.Find( "Hidden/Templates/PostProcessStack" ) );
		context.command.BlitFullscreenTriangle( context.source, context.destination, sheet, 0 );
	}
}
#endif
