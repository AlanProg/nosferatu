// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Hidden/Templates/PostProcessStack"
{
	Properties
	{
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
	}

	SubShader
	{
		Cull Off
		ZWrite Off
		ZTest Always
		
		Pass
		{
			CGPROGRAM

			

			#pragma vertex Vert
			#pragma fragment Frag
			#pragma target 3.0

			#include "UnityCG.cginc"
			
		
			struct ASEAttributesDefault
			{
				float3 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				
			};

			struct ASEVaryingsDefault
			{
				float4 vertex : SV_POSITION;
				float2 texcoord : TEXCOORD0;
				float2 texcoordStereo : TEXCOORD1;
			#if STEREO_INSTANCING_ENABLED
				uint stereoTargetEyeIndex : SV_RenderTargetArrayIndex;
			#endif
				
			};

			uniform sampler2D _MainTex;
			uniform half4 _MainTex_TexelSize;
			uniform half4 _MainTex_ST;
			
			uniform float _GlobalSaturation;
			UNITY_DECLARE_DEPTH_TEXTURE( _CameraDepthTexture );
			uniform float4 _CameraDepthTexture_TexelSize;
			uniform sampler2D _CameraDepthNormalsTexture;

			
			float2 TransformTriangleVertexToUV (float2 vertex)
			{
				float2 uv = (vertex + 1.0) * 0.5;
				return uv;
			}

			ASEVaryingsDefault Vert( ASEAttributesDefault v  )
			{
				ASEVaryingsDefault o;
				o.vertex = float4(v.vertex.xy, 0.0, 1.0);
				o.texcoord = TransformTriangleVertexToUV (v.vertex.xy);
#if UNITY_UV_STARTS_AT_TOP
				o.texcoord = o.texcoord * float2(1.0, -1.0) + float2(0.0, 1.0);
#endif
				o.texcoordStereo = TransformStereoScreenSpaceTex (o.texcoord, 1.0);

				v.texcoord = o.texcoordStereo;
				float4 ase_ppsScreenPosVertexNorm = float4(o.texcoordStereo,0,1);

				

				return o;
			}

			float4 Frag (ASEVaryingsDefault i  ) : SV_Target
			{
				float4 ase_ppsScreenPosFragNorm = float4(i.texcoordStereo,0,1);

				float2 uv_MainTex = i.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;
				float4 tex2DNode35 = tex2D( _MainTex, uv_MainTex );
				float grayscale77 = Luminance(tex2DNode35.rgb);
				float4 color30 = IsGammaSpace() ? float4(0,0,0,0) : float4(0,0,0,0);
				float eyeDepth80 = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture,UNITY_PROJ_COORD( ase_ppsScreenPosFragNorm )));
				float clampResult90 = clamp( pow( ( eyeDepth80 * 0.04 ) , 10.0 ) , 0.0 , 1.0 );
				float4 appendResult19 = (float4(0.0007 , 0.0007 , 0.0 , 0.0));
				float eyeDepth11 = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture,UNITY_PROJ_COORD( ( ase_ppsScreenPosFragNorm + appendResult19 ) )));
				float4 appendResult21 = (float4(-0.0007 , -0.0007 , 0.0 , 0.0));
				float eyeDepth10 = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture,UNITY_PROJ_COORD( ( appendResult21 + ase_ppsScreenPosFragNorm ) )));
				float4 appendResult22 = (float4(-0.0007 , 0.0007 , 0.0 , 0.0));
				float eyeDepth9 = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture,UNITY_PROJ_COORD( ( appendResult22 + ase_ppsScreenPosFragNorm ) )));
				float4 appendResult23 = (float4(0.0007 , -0.0007 , 0.0 , 0.0));
				float eyeDepth3 = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture,UNITY_PROJ_COORD( ( appendResult23 + ase_ppsScreenPosFragNorm ) )));
				float4 appendResult70 = (float4(0.002 , 0.002 , 0.0 , 0.0));
				float depthDecodedVal48 = 0;
				float3 normalDecodedVal48 = float3(0,0,0);
				DecodeDepthNormal( tex2D( _CameraDepthNormalsTexture, ( ase_ppsScreenPosFragNorm + appendResult70 ).xy ), depthDecodedVal48, normalDecodedVal48 );
				float4 appendResult72 = (float4(-0.002 , -0.002 , 0.0 , 0.0));
				float depthDecodedVal50 = 0;
				float3 normalDecodedVal50 = float3(0,0,0);
				DecodeDepthNormal( tex2D( _CameraDepthNormalsTexture, ( appendResult72 + ase_ppsScreenPosFragNorm ).xy ), depthDecodedVal50, normalDecodedVal50 );
				float4 appendResult69 = (float4(-0.002 , 0.002 , 0.0 , 0.0));
				float depthDecodedVal52 = 0;
				float3 normalDecodedVal52 = float3(0,0,0);
				DecodeDepthNormal( tex2D( _CameraDepthNormalsTexture, ( appendResult69 + ase_ppsScreenPosFragNorm ).xy ), depthDecodedVal52, normalDecodedVal52 );
				float4 appendResult71 = (float4(0.002 , -0.002 , 0.0 , 0.0));
				float depthDecodedVal54 = 0;
				float3 normalDecodedVal54 = float3(0,0,0);
				DecodeDepthNormal( tex2D( _CameraDepthNormalsTexture, ( appendResult71 + ase_ppsScreenPosFragNorm ).xy ), depthDecodedVal54, normalDecodedVal54 );
				float dotResult61 = dot( ( normalDecodedVal48 - normalDecodedVal50 ) , ( normalDecodedVal52 - normalDecodedVal54 ) );
				float clampResult97 = clamp( pow( ( eyeDepth80 * 0.1 ) , 10.0 ) , 0.0 , 1.0 );
				float clampResult105 = clamp( ( ( 1.0 - _GlobalSaturation ) * max( ( ( 1.0 - clampResult90 ) * pow( ( max( ( eyeDepth11 - eyeDepth10 ) , ( eyeDepth9 - eyeDepth3 ) ) * 0.5 ) , 4.0 ) ) , ( ( 1.0 - step( dotResult61 , 0.8 ) ) * ( 1.0 - clampResult97 ) ) ) ) , 0.0 , 50.0 );
				float4 lerpResult32 = lerp( tex2DNode35 , color30 , clampResult105);
				float4 ifLocalVar76 = 0;
				if( grayscale77 <= 0.5 )
				ifLocalVar76 = lerpResult32;
				else
				ifLocalVar76 = tex2DNode35;
				

				float4 color = ifLocalVar76;
				
				return color;
			}
			ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=16900
-1920;0;1920;1019;-303.5161;527.3158;1.0316;True;False
Node;AmplifyShaderEditor.RangedFloatNode;73;-2126.315,615.6105;Float;False;Constant;_Float1;Float 1;0;0;Create;True;0;0;False;0;0.002;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;18;-1830.335,-479.899;Float;False;Constant;_Float0;Float 0;0;0;Create;True;0;0;False;0;0.0007;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;68;-1935.214,691.0104;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;71;-1735.015,1136.911;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DynamicAppendNode;72;-1722.465,769.0172;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DynamicAppendNode;69;-1718.116,930.2103;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DynamicAppendNode;70;-1719.415,610.4105;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;63;-2297.576,994.2487;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NegateNode;20;-1639.234,-404.499;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;66;-1334.677,931.8488;Float;False;2;2;0;FLOAT4;-0.01,0.01,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DynamicAppendNode;22;-1422.136,-165.299;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DynamicAppendNode;23;-1439.035,41.40089;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DynamicAppendNode;19;-1423.435,-485.099;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleAddOpNode;64;-1345.877,1101.449;Float;False;2;2;0;FLOAT4;0.01,-0.01,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DynamicAppendNode;21;-1426.485,-326.4922;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TexturePropertyNode;46;-2202.862,1391.212;Float;True;Global;_CameraDepthNormalsTexture;_CameraDepthNormalsTexture;0;0;Create;False;0;0;False;0;None;None;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.SimpleAddOpNode;65;-1405.077,659.8489;Float;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0.01,0.01,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;7;-2001.596,-101.2606;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;67;-1360.277,807.0487;Float;False;2;2;0;FLOAT4;-0.01,-0.01,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SamplerNode;55;-1044.98,2079.597;Float;True;Property;_TextureSample4;Texture Sample 4;1;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;53;-1037.886,1831.329;Float;True;Property;_TextureSample3;Texture Sample 3;1;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;15;-1049.897,5.939355;Float;False;2;2;0;FLOAT4;0.01,-0.01,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleAddOpNode;12;-1109.097,-435.6606;Float;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0.01,0.01,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;81;-661.3438,-1035.547;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;14;-1038.697,-163.6606;Float;False;2;2;0;FLOAT4;-0.01,0.01,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SamplerNode;49;-1060.898,1344.422;Float;True;Property;_TextureSample1;Texture Sample 1;1;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;13;-1064.297,-288.4606;Float;False;2;2;0;FLOAT4;-0.01,-0.01,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SamplerNode;51;-1052.073,1609.07;Float;True;Property;_TextureSample2;Texture Sample 2;1;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenDepthNode;80;-271.1795,-1013.178;Float;False;0;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenDepthNode;11;-814.6971,-450.0607;Float;False;0;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DecodeDepthNormalNode;48;-647.9097,1395.194;Float;False;1;0;FLOAT4;0,0,0,0;False;2;FLOAT;0;FLOAT3;1
Node;AmplifyShaderEditor.ScreenDepthNode;9;-797.0972,-237.2607;Float;False;0;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DecodeDepthNormalNode;50;-643.2867,1626.23;Float;False;1;0;FLOAT4;0,0,0,0;False;2;FLOAT;0;FLOAT3;1
Node;AmplifyShaderEditor.ScreenDepthNode;3;-784.297,-74.06121;Float;False;0;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DecodeDepthNormalNode;52;-629.0999,1848.49;Float;False;1;0;FLOAT4;0,0,0,0;False;2;FLOAT;0;FLOAT3;1
Node;AmplifyShaderEditor.ScreenDepthNode;10;-797.0973,-352.4606;Float;False;0;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DecodeDepthNormalNode;54;-636.1934,2096.758;Float;False;1;0;FLOAT4;0,0,0,0;False;2;FLOAT;0;FLOAT3;1
Node;AmplifyShaderEditor.SimpleSubtractOpNode;60;-333.1147,1991.937;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;59;-326.9214,1517.111;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;17;-526.4349,-139.2991;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;82;-10.27201,-959.7399;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0.04;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;95;-83.86127,-783.3749;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0.1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;16;-542.035,-422.6991;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;61;-144.8451,1708.73;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;83;139.2281,-842.74;Float;False;2;0;FLOAT;0;False;1;FLOAT;10;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;96;79.56873,-661.8777;Float;False;2;0;FLOAT;0;False;1;FLOAT;10;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;25;-322.3369,-349.8994;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;74;171.0859,1672.738;Float;False;2;0;FLOAT;0;False;1;FLOAT;0.8;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;97;269.8786,-560.8092;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;90;341.5606,-723.1998;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;6;-158.6964,-278.8611;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;94;516.0597,-645.1744;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;98;415.0302,-507.0493;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;75;356.6386,1565.037;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;27;3.732479,-191.9796;Float;False;2;0;FLOAT;0;False;1;FLOAT;4;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;99;290.8191,571.2935;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;100;514.9225,472.4387;Float;False;Global;_GlobalSaturation;_GlobalSaturation;1;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;93;283.9264,291.4635;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;101;716.4225,377.4387;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;62;502.972,292.1833;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TemplateShaderPropertyNode;33;-290.6215,262.4415;Float;False;0;0;_MainTex;Pass;0;5;SAMPLER2D;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;103;838.483,264.345;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;30;-208.3912,-24.05387;Float;False;Constant;_Color0;Color 0;0;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;35;-78.09174,309.7415;Float;True;Property;_TextureSample0;Texture Sample 0;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ClampOpNode;105;991.4556,276.8831;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;50;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCGrayscale;77;553.0995,-371.6935;Float;False;0;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;32;960.773,-75.65959;Float;False;3;0;COLOR;1,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;104;1058.595,-303.3257;Float;False;Constant;_LightThreshold;LightThreshold;1;0;Create;True;0;0;False;0;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;76;1272.5,-145.8495;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0.25;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;0;2151.572,-188.5531;Float;False;True;2;Float;ASEMaterialInspector;0;2;Hidden/Templates/PostProcessStack;32139be9c1eb75640a847f011acf3bcf;True;SubShader 0 Pass 0;0;0;SubShader 0 Pass 0;1;False;False;False;True;2;False;-1;False;False;True;2;False;-1;True;7;False;-1;False;False;False;0;False;False;False;False;False;False;False;False;False;False;True;2;0;;0;0;Standard;0;0;1;True;False;1;0;FLOAT4;0,0,0,0;False;0
WireConnection;68;0;73;0
WireConnection;71;0;73;0
WireConnection;71;1;68;0
WireConnection;72;0;68;0
WireConnection;72;1;68;0
WireConnection;69;0;68;0
WireConnection;69;1;73;0
WireConnection;70;0;73;0
WireConnection;70;1;73;0
WireConnection;20;0;18;0
WireConnection;66;0;69;0
WireConnection;66;1;63;0
WireConnection;22;0;20;0
WireConnection;22;1;18;0
WireConnection;23;0;18;0
WireConnection;23;1;20;0
WireConnection;19;0;18;0
WireConnection;19;1;18;0
WireConnection;64;0;71;0
WireConnection;64;1;63;0
WireConnection;21;0;20;0
WireConnection;21;1;20;0
WireConnection;65;0;63;0
WireConnection;65;1;70;0
WireConnection;67;0;72;0
WireConnection;67;1;63;0
WireConnection;55;0;46;0
WireConnection;55;1;64;0
WireConnection;53;0;46;0
WireConnection;53;1;66;0
WireConnection;15;0;23;0
WireConnection;15;1;7;0
WireConnection;12;0;7;0
WireConnection;12;1;19;0
WireConnection;14;0;22;0
WireConnection;14;1;7;0
WireConnection;49;0;46;0
WireConnection;49;1;65;0
WireConnection;13;0;21;0
WireConnection;13;1;7;0
WireConnection;51;0;46;0
WireConnection;51;1;67;0
WireConnection;80;0;81;0
WireConnection;11;0;12;0
WireConnection;48;0;49;0
WireConnection;9;0;14;0
WireConnection;50;0;51;0
WireConnection;3;0;15;0
WireConnection;52;0;53;0
WireConnection;10;0;13;0
WireConnection;54;0;55;0
WireConnection;60;0;52;1
WireConnection;60;1;54;1
WireConnection;59;0;48;1
WireConnection;59;1;50;1
WireConnection;17;0;9;0
WireConnection;17;1;3;0
WireConnection;82;0;80;0
WireConnection;95;0;80;0
WireConnection;16;0;11;0
WireConnection;16;1;10;0
WireConnection;61;0;59;0
WireConnection;61;1;60;0
WireConnection;83;0;82;0
WireConnection;96;0;95;0
WireConnection;25;0;16;0
WireConnection;25;1;17;0
WireConnection;74;0;61;0
WireConnection;97;0;96;0
WireConnection;90;0;83;0
WireConnection;6;0;25;0
WireConnection;94;0;90;0
WireConnection;98;0;97;0
WireConnection;75;0;74;0
WireConnection;27;0;6;0
WireConnection;99;0;75;0
WireConnection;99;1;98;0
WireConnection;93;0;94;0
WireConnection;93;1;27;0
WireConnection;101;0;100;0
WireConnection;62;0;93;0
WireConnection;62;1;99;0
WireConnection;103;0;101;0
WireConnection;103;1;62;0
WireConnection;35;0;33;0
WireConnection;105;0;103;0
WireConnection;77;0;35;0
WireConnection;32;0;35;0
WireConnection;32;1;30;0
WireConnection;32;2;105;0
WireConnection;76;0;77;0
WireConnection;76;1;104;0
WireConnection;76;2;35;0
WireConnection;76;3;32;0
WireConnection;76;4;32;0
WireConnection;0;0;76;0
ASEEND*/
//CHKSM=E8915AE71A7C7F538DCFC5D938AA4825FFD2BE9D