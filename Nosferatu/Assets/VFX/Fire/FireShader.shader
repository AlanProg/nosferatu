// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "FireShader"
{
	Properties
	{
		_DistortTiling("DistortTiling", Vector) = (3,3,0,0)
		[HDR]_FireColor("FireColor", Color) = (2.118547,1.00936,0.2329293,0)
		_DistortStrength("DistortStrength", Float) = 0.2
		_PannerSpeed("PannerSpeed", Vector) = (0,0,0,0)
		_FireTiling("FireTiling", Vector) = (6,1,0,0)
		_FireBaseStrenght("FireBaseStrenght", Float) = 0.5
		_GradientAStrenght("GradientAStrenght", Float) = 2
		_GradientBStrength("GradientBStrength", Float) = 5
		_WindPannerSpeed("WindPannerSpeed", Vector) = (0,0,0,0)
		_WindTiling("WindTiling", Vector) = (0,0,0,0)
		_TheWind("The Wind", Vector) = (0,0,1,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Unlit alpha:fade keepalpha noshadow vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_texcoord;
			float4 vertexColor : COLOR;
		};

		uniform float3 _TheWind;
		uniform float2 _WindPannerSpeed;
		uniform float2 _WindTiling;
		uniform float2 _FireTiling;
		uniform float2 _DistortTiling;
		uniform float _DistortStrength;
		uniform float2 _PannerSpeed;
		uniform float _FireBaseStrenght;
		uniform float _GradientAStrenght;
		uniform float _GradientBStrength;
		uniform float4 _FireColor;


		struct Gradient
		{
			int type;
			int colorsLength;
			int alphasLength;
			float4 colors[8];
			float2 alphas[8];
		};


		Gradient NewGradient(int type, int colorsLength, int alphasLength, 
		float4 colors0, float4 colors1, float4 colors2, float4 colors3, float4 colors4, float4 colors5, float4 colors6, float4 colors7,
		float2 alphas0, float2 alphas1, float2 alphas2, float2 alphas3, float2 alphas4, float2 alphas5, float2 alphas6, float2 alphas7)
		{
			Gradient g;
			g.type = type;
			g.colorsLength = colorsLength;
			g.alphasLength = alphasLength;
			g.colors[ 0 ] = colors0;
			g.colors[ 1 ] = colors1;
			g.colors[ 2 ] = colors2;
			g.colors[ 3 ] = colors3;
			g.colors[ 4 ] = colors4;
			g.colors[ 5 ] = colors5;
			g.colors[ 6 ] = colors6;
			g.colors[ 7 ] = colors7;
			g.alphas[ 0 ] = alphas0;
			g.alphas[ 1 ] = alphas1;
			g.alphas[ 2 ] = alphas2;
			g.alphas[ 3 ] = alphas3;
			g.alphas[ 4 ] = alphas4;
			g.alphas[ 5 ] = alphas5;
			g.alphas[ 6 ] = alphas6;
			g.alphas[ 7 ] = alphas7;
			return g;
		}


		float4 SampleGradient( Gradient gradient, float time )
		{
			float3 color = gradient.colors[0].rgb;
			UNITY_UNROLL
			for (int c = 1; c < 8; c++)
			{
			float colorPos = saturate((time - gradient.colors[c-1].w) / (gradient.colors[c].w - gradient.colors[c-1].w)) * step(c, (float)gradient.colorsLength-1);
			color = lerp(color, gradient.colors[c].rgb, lerp(colorPos, step(0.01, colorPos), gradient.type));
			}
			#ifndef UNITY_COLORSPACE_GAMMA
			color = half3(GammaToLinearSpaceExact(color.r), GammaToLinearSpaceExact(color.g), GammaToLinearSpaceExact(color.b));
			#endif
			float alpha = gradient.alphas[0].x;
			UNITY_UNROLL
			for (int a = 1; a < 8; a++)
			{
			float alphaPos = saturate((time - gradient.alphas[a-1].y) / (gradient.alphas[a].y - gradient.alphas[a-1].y)) * step(a, (float)gradient.alphasLength-1);
			alpha = lerp(alpha, gradient.alphas[a].x, lerp(alphaPos, step(0.01, alphaPos), gradient.type));
			}
			return float4(color, alpha);
		}


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			Gradient gradient44 = NewGradient( 0, 2, 2, float4( 0, 0, 0, 0 ), float4( 1, 1, 1, 1 ), 0, 0, 0, 0, 0, 0, float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
			float temp_output_91_0 = ( 3.0 * v.color.a );
			float2 appendResult87 = (float2(( temp_output_91_0 * 2.0 ) , temp_output_91_0));
			float2 temp_output_96_0 = ( appendResult87 + _FireTiling );
			float2 uv_TexCoord77 = v.texcoord.xy * _WindTiling + temp_output_96_0;
			float2 panner79 = ( 1.0 * _Time.y * _WindPannerSpeed + uv_TexCoord77);
			float simplePerlin2D76 = snoise( panner79 );
			float4 transform73 = mul(unity_WorldToObject,float4( ( ( 1.0 - SampleGradient( gradient44, v.texcoord.xy.y ) ) * ( _TheWind * (0.4 + (simplePerlin2D76 - 0.0) * (1.0 - 0.4) / (1.0 - 0.0)) ) ) , 0.0 ));
			v.vertex.xyz += transform73.xyz;
		}

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float2 uv_TexCoord2 = i.uv_texcoord * _DistortTiling;
			float simplePerlin2D1 = snoise( uv_TexCoord2 );
			float temp_output_91_0 = ( 3.0 * i.vertexColor.a );
			float2 appendResult87 = (float2(( temp_output_91_0 * 2.0 ) , temp_output_91_0));
			float2 temp_output_96_0 = ( appendResult87 + _FireTiling );
			float2 uv_TexCoord7 = i.uv_texcoord * temp_output_96_0 + temp_output_96_0;
			float2 panner8 = ( 1.0 * _Time.y * _PannerSpeed + uv_TexCoord7);
			float simplePerlin2D4 = snoise( ( ( simplePerlin2D1 * _DistortStrength ) + panner8 ) );
			Gradient gradient19 = NewGradient( 0, 2, 2, float4( 0, 0, 0, 0 ), float4( 1, 1, 1, 1 ), 0, 0, 0, 0, 0, 0, float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
			float clampResult35 = clamp( ( ( simplePerlin2D4 * _FireBaseStrenght ) + ( SampleGradient( gradient19, i.uv_texcoord.y ) * _GradientAStrenght ) ) , 0.0 , 1.0 );
			Gradient gradient44 = NewGradient( 0, 2, 2, float4( 0, 0, 0, 0 ), float4( 1, 1, 1, 1 ), 0, 0, 0, 0, 0, 0, float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
			float clampResult50 = clamp( ( SampleGradient( gradient44, i.uv_texcoord.y ) * _GradientBStrength ) , 0.0 , 1.0 );
			float temp_output_98_0 = pow( ( clampResult35 * clampResult50 ) , 4.0 );
			o.Emission = ( temp_output_98_0 * _FireColor ).rgb;
			o.Alpha = temp_output_98_0;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16900
-1920;0;1920;1019;-711.6948;377.5142;1;True;False
Node;AmplifyShaderEditor.VertexColorNode;90;-2372.957,7.161587;Float;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;91;-2020.315,4.454308;Float;False;2;2;0;FLOAT;3;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;97;-1888.89,-121.1507;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;87;-1756.129,4.770071;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.Vector2Node;103;-1739.022,316.3575;Float;False;Property;_FireTiling;FireTiling;4;0;Create;True;0;0;False;0;6,1;14,3;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.Vector2Node;100;-1672.723,-277.7424;Float;False;Property;_DistortTiling;DistortTiling;0;0;Create;True;0;0;False;0;3,3;2.23,3;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TextureCoordinatesNode;2;-1464.565,-93.74762;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;3,3;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;96;-1488.489,220.7494;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;6,1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.Vector2Node;102;-1144.923,334.5574;Float;False;Property;_PannerSpeed;PannerSpeed;3;0;Create;True;0;0;False;0;0,0;1,5;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;101;-1131.923,57.65735;Float;False;Property;_DistortStrength;DistortStrength;2;0;Create;True;0;0;False;0;0.2;0.2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;7;-1189.095,181.2487;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;7,2;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NoiseGeneratorNode;1;-1165.863,-97.64769;Float;False;Simplex2D;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;5;-887.1282,-48.32905;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0.2;False;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;8;-908.325,175.232;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;1,5;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;32;-1434.531,631.9777;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;110;-1296.22,1639.954;Float;False;Property;_WindTiling;WindTiling;9;0;Create;True;0;0;False;0;0,0;1,0.2;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.GradientNode;19;-1133.962,485.8996;Float;False;0;2;2;0,0,0,0;1,1,1,1;1,0;1,1;0;1;OBJECT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;6;-617.5264,-30.33183;Float;False;2;2;0;FLOAT;0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;77;-1069.473,1609.56;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;3,0.2;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;109;-1089.11,1852.624;Float;False;Property;_WindPannerSpeed;WindPannerSpeed;8;0;Create;True;0;0;False;0;0,0;0.6,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.GradientNode;44;80.88583,801.6203;Float;False;0;2;2;0,0,0,0;1,1,1,1;1,0;1,1;0;1;OBJECT;0
Node;AmplifyShaderEditor.RangedFloatNode;105;-375.3224,-217.9424;Float;False;Property;_FireBaseStrenght;FireBaseStrenght;5;0;Create;True;0;0;False;0;0.5;0.3;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GradientSampleNode;20;-845.3618,450.7998;Float;True;2;0;OBJECT;0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;107;-507.9224,629.6575;Float;False;Property;_GradientAStrenght;GradientAStrenght;6;0;Create;True;0;0;False;0;2;0.99;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;45;-219.6831,947.6984;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NoiseGeneratorNode;4;-405.9575,-45.13095;Float;False;Simplex2D;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GradientSampleNode;46;369.4861,766.5206;Float;True;2;0;OBJECT;0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;108;513.119,648.3412;Float;False;Property;_GradientBStrength;GradientBStrength;7;0;Create;True;0;0;False;0;5;2.65;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;79;-815.0836,1654.56;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;6,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;95;-144.9652,-95.98361;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;106;-290.8223,432.0576;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;12;64.47327,34.67725;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;76;-507.0836,1730.56;Float;False;Simplex2D;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;49;770.9162,824.44;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;92;-262.4358,1782.885;Float;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0.4;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;71;42.70367,1310.906;Float;False;Property;_TheWind;The Wind;10;0;Create;True;0;0;False;0;0,0,1;1,0,0.5;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ClampOpNode;50;1062.484,796.8351;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;35;280.0411,74.99011;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;57;534.1154,993.4826;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;40;1346.439,266.0091;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;74;321.6116,1405.426;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.PowerNode;98;1502.779,136.4601;Float;False;2;0;FLOAT;0;False;1;FLOAT;4;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;69;811.4777,1159.778;Float;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;51;1601.673,-151.6486;Float;False;Property;_FireColor;FireColor;1;1;[HDR];Create;True;0;0;False;0;2.118547,1.00936,0.2329293,0;5.59088,1.287952,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;55;1623.911,869.0632;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;52;1884.668,118.4874;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WorldToObjectTransfNode;73;1110.381,1069.724;Float;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;2135.078,148.5589;Float;False;True;2;Float;ASEMaterialInspector;0;0;Unlit;FireShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;2;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;False;0;False;Transparent;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;True;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;91;1;90;4
WireConnection;97;0;91;0
WireConnection;87;0;97;0
WireConnection;87;1;91;0
WireConnection;2;0;100;0
WireConnection;96;0;87;0
WireConnection;96;1;103;0
WireConnection;7;0;96;0
WireConnection;7;1;96;0
WireConnection;1;0;2;0
WireConnection;5;0;1;0
WireConnection;5;1;101;0
WireConnection;8;0;7;0
WireConnection;8;2;102;0
WireConnection;6;0;5;0
WireConnection;6;1;8;0
WireConnection;77;0;110;0
WireConnection;77;1;96;0
WireConnection;20;0;19;0
WireConnection;20;1;32;2
WireConnection;4;0;6;0
WireConnection;46;0;44;0
WireConnection;46;1;45;2
WireConnection;79;0;77;0
WireConnection;79;2;109;0
WireConnection;95;0;4;0
WireConnection;95;1;105;0
WireConnection;106;0;20;1
WireConnection;106;1;107;0
WireConnection;12;0;95;0
WireConnection;12;1;106;0
WireConnection;76;0;79;0
WireConnection;49;0;46;1
WireConnection;49;1;108;0
WireConnection;92;0;76;0
WireConnection;50;0;49;0
WireConnection;35;0;12;0
WireConnection;57;0;46;1
WireConnection;40;0;35;0
WireConnection;40;1;50;0
WireConnection;74;0;71;0
WireConnection;74;1;92;0
WireConnection;98;0;40;0
WireConnection;69;0;57;0
WireConnection;69;1;74;0
WireConnection;52;0;98;0
WireConnection;52;1;51;0
WireConnection;73;0;69;0
WireConnection;0;2;52;0
WireConnection;0;9;98;0
WireConnection;0;11;73;0
ASEEND*/
//CHKSM=F931FFA79F9292A96010B112DB1B300AD8A0877A