﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AI;

public class RangerEnemy : MonoBehaviour
{

    public Animator enemyAnim;
    public float life = 100;
    public float dmgPerBullet;

    public Transform destinys;

    [Header("Evento chamado quando receber dano")]
    public UnityEvent OnHit;
    [Header("Evento chamado quando morrer")]
    public UnityEvent WhenDie;

    float distancePlayer;

    bool attack;
    float count;

    NavMeshAgent agent;



    private void Awake()
    {

        agent = GetComponent<NavMeshAgent>();
        OnHit.AddListener(() => { enemyAnim.SetTrigger("Hit"); });
        WhenDie.AddListener(() => { OnHit.Invoke(); });
        WhenDie.AddListener(() => { enemyAnim.SetBool("Death", true); });

    }

    // Update is called once per frame
    void Update()
    {

        if (attack && count >= 4) { attack = false; }

        distancePlayer = Vector3.Distance(transform.position, GameObject.Find("Player").transform.position);

        if (life <= 0)
        {
            GetComponent<Rigidbody>().isKinematic = true;
            GetComponent<CapsuleCollider>().enabled = false;
            enemyAnim.SetBool("Death", true);
            return;
        }

        if (distancePlayer < 8 && life > 0 && !attack && !Controller.Instance.crouch && !FuryControll.Instance.furyState)
        {

            agent.destination = transform.GetComponent<CatchDestiny>().destiny.position;
            enemyAnim.SetBool("Run", true);
            transform.LookAt(GameObject.Find("Player").transform);
            count = 0;
            attack = true;
            return;

        }
        else
        {
            enemyAnim.SetBool("Run", false);
            count++;
            agent.destination = transform.position;
        }

    }

    public void hitByArrow(float dmg)
    {
        life -= dmg;
        if (life < 0)
        {
            WhenDie.Invoke();
        }
        else
        {
            OnHit.Invoke();
        }
    }

    public void hitByShotgun(float dmg)
    {
        life -= dmg;
        if (life < 0)
        {
            WhenDie.Invoke();
        }
        else
        {
            OnHit.Invoke();
        }
    }

    public void HittedSW(float dmg)
    {
        life -= dmg;
        if (life < 0)
        {
            WhenDie.Invoke();
        }
        else
        {
            OnHit.Invoke();
        }
    }
}
