﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem.Interactions;
using TMPro;

public class PauseScript : MonoBehaviour
{

    public GameObject canvasPause;
    InputMaster inputMaster;

    bool paused = false;

    public Animator pauseAnimator;

    [SerializeField] private TextMeshProUGUI page1, page2;
    private int currentLoreIndex = 0;

    private int currentPage = 1;

    private void Awake()
    {
        inputMaster = new InputMaster();

        inputMaster.MenuControlls.Ps4Pause.performed += act => PauseIn_Out(0);
        inputMaster.MenuControlls.PC_Pause.performed += act => PauseIn_Out(1);
        inputMaster.MenuControlls.XBOX_Pause.performed += act => PauseIn_Out(2);
       

        inputMaster.MenuControlls.LeftPage.performed += act => MenuLeft();
        inputMaster.MenuControlls.RightPage.performed += act => MenuRight();

        inputMaster.MenuControlls.MenuUp.performed += act => LoreScroll(1);
        inputMaster.MenuControlls.MenuDown.performed += act => LoreScroll(-1);

        
    }

    


    private void OnEnable()
    {
        inputMaster.Enable();
        
    }

    private void OnDisable()
    {
        inputMaster.Disable();
    }

    void PauseIn_Out(int a)
    {

       

        if (paused)
        {
            canvasPause.SetActive(false);
            paused = false;
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            pauseAnimator.updateMode = AnimatorUpdateMode.UnscaledTime ;
            canvasPause.SetActive(true);
            paused = true;
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            currentPage = LoreControll.Instance.unlockedLores.Count - 1;
            LoreScroll(0);
        }

       /*GameObject[] objs;
        if (a == 0)
        {
            objs = GameObject.FindGameObjectsWithTag("PS4");
        }
        if (a == 1)
        {
            objs = GameObject.FindGameObjectsWithTag("PC");
        }
        if (a == 2)
        {
            objs = GameObject.FindGameObjectsWithTag("XBOX");
        }
        else
        {
            objs = GameObject.FindGameObjectsWithTag("GAMEPAD");
        }
        foreach (GameObject obj in objs)
        {
            obj.SetActive(true);
        }*/

    }

    void MenuRight()
    {
        if(currentPage == 0)
        {
            pauseAnimator.SetTrigger("T_Opt");
            currentPage = 1;
        }if(currentPage == -1)
        {
            pauseAnimator.SetTrigger("T_ExitCtrl");
            currentPage = 0;
        }
    }

    void MenuLeft()
    {
        if (currentPage == 0)
        {
            pauseAnimator.SetTrigger("T_Ctrl");
            currentPage = -1;
        }
        if (currentPage == 1)
        {
            pauseAnimator.SetTrigger("T_ExitOpt");
            currentPage = 0;
        }
        
    }

    void LoreScroll(int move)
    {
        if (paused)
        {
            currentPage += move;
            currentPage = Mathf.Clamp(currentPage, 0, LoreControll.Instance.unlockedLores.Count-1);
            print(currentPage);
            if (LoreControll.Instance.unlockedLores.Capacity != 0)
            {
                page1.text = LoreControll.Instance.unlockedLores[currentPage].ContentPageOne;
                page2.text = LoreControll.Instance.unlockedLores[currentPage].ContentPageTwo;
            }
           
        }

    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
