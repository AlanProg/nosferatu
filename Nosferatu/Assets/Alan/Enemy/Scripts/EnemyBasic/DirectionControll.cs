﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionControll : MonoBehaviour
{
    public bool up;
    public float value;
    public float veloView = 15;

    private void Awake()
    {
#if UNITY_EDITOR
        GetComponentInChildren<MeshRenderer>().enabled = true;
#endif
    }

    // Update is called once per frame
    void Update()
    {
        if(value < 90 && up)
        {
            transform.Rotate(0, veloView * Time.deltaTime, 0);
            value += veloView * Time.deltaTime;
        }
        else if(value > 90 && up)
        {
            up = false;
        }

        if (value > -90 && !up)
        {
            transform.Rotate(0, -veloView * Time.deltaTime, 0);
            value += -veloView * Time.deltaTime;
        }
        else if (value < -90 && !up)
        {
            up = true;
        }
    }
}
