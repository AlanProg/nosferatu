﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AI;

public class Nosferatu_Basic : MonoBehaviour
{

    public Animator enemyAnim;
    public LayerMask isPlayer;
    public float life = 100;
    public float dmgOnHit;
    [Header("Distancias que o jogador será detectado")]
    public float normalDistance = 8;
    [Tooltip("Quando o jogador estiver correndo ou atirando, ele ativará o" +
     " modo screaming, que fará o inimigo ouvir o jogador e o encontrar automaticamente ou com mais velocidade")]
    public float distanceOnScreaming = 12;
    public float distanceOnDistraction = 4;
    public float distanceToAttack = 1;

    [Header("Velocidade com que a atenção diminui")]
    public float velocityReduceAttemption = 1;
    
    [Header("Se ele ficará parado em um determinado posto")]
    public bool isGuardian;

    [Header("PreFab de chamas")]
    public GameObject onFirePrefab;

    public Transform rayOriginView;
    public Transform direction;

    Vector3 positionFocus;
    Transform destiny;

    [Header("Evento chamado quando receber dano")]
    public UnityEvent OnHit;
    [Header("Evento chamado quando morrer")]
    public UnityEvent WhenDie;

    public int indice = 0;

    public GameObject alertSinal;

    public float timeToViewPlayer = 25;

    float distancePlayer;

    bool attack;
    float count;

    NavMeshAgent agent;

    public bool iamMoreClose = false;

    GameObject destiny_Ref;

    bool onFreeWalk = false;

    public bool playerOnView;

    public float countToView;

    bool onFire;
    float dmgOnFire;
    float timePerHit;
    float fireDuration;
    float countFire;

    float countOnView = 0;

    bool inDistraction = false;
    Vector3 distractionPoint;

    GameObject fireControll;

    private void Awake()
    {

        positionFocus = transform.position;

        destiny_Ref = new GameObject();

        switch (indice)
        {
            case 0:
                destiny = GameObject.Find("PointA").gameObject.transform;
                break;
            case 1:
                destiny = GameObject.Find("PointA1").gameObject.transform;
                break;
            case 2:
                destiny = GameObject.Find("PointA2").gameObject.transform;
                break;
        }

        agent = GetComponent<NavMeshAgent>();
        OnHit.AddListener(() => { enemyAnim.SetTrigger("Hit"); });
        WhenDie.AddListener(() => { OnHit.Invoke(); });
        WhenDie.AddListener(() => { enemyAnim.SetBool("Death", true); });

        int randomPoint = (int)Random.Range(0, 3);

        switch (randomPoint)
        {
            case 0:
                destiny = GameObject.Find("PointA").gameObject.transform;
                return;
            case 1:
                destiny = GameObject.Find("PointA1").gameObject.transform;
                return;
        }
        destiny = GameObject.Find("PointA2").gameObject.transform;

    }

    private void Start()
    {
        EnemyGeneral.Instance.enemysReference.Add(gameObject);    
    }

    // Update is called once per frame
    void Update()
    {

        // Enemy Ray View Debug 
        Debug.DrawRay(rayOriginView.position, (rayOriginView.position - direction.position) * -1, Color.red);

        distancePlayer = Vector3.Distance(transform.position, PlayerInfo.Instance.transform.position);

        if (attack && count >= 2) { attack = false; }

        // aplicações para quando o inimigo está em chamas
        if (onFire)
        {
            if(countFire >= timePerHit)
            {
                fireDuration -= countFire;
                life -= dmgOnFire;
                if(life <= 0)
                {
                    WhenDie.Invoke();
                }
                countFire = 0;
                if (fireDuration <= 0)
                {
                    onFire = false;
                    Destroy(fireControll.gameObject);
                }
            }
        }

        // aplicações para quando o inimigo está distraido
        if (inDistraction)
        {
            if (EnemyGeneral.Instance.playerOnView)
            {
                inDistraction = false;
                return;
            }

            if (!EnemyGeneral.Instance.playerOnView)
            {
                if(!playerOnView)
                    playerOnView = CheckView();

                if (playerOnView || distancePlayer <= distanceOnScreaming)
                {
                    SearchForPlayer();

                    if (countToView >= timeToViewPlayer)
                    {
                        EnemyGeneral.Instance.playerOnView = true;
                    }
                }
            }

            agent.destination = distractionPoint;
            return;
        }

        // aplicações para quando o inimigo está Patrulhando

        #region Verificações de patrulha

        if (!isGuardian)
        {
            if (!attack && !EnemyGeneral.Instance.playerOnView && count >= 3 && !onFreeWalk)
            {
                enemyAnim.SetBool("Run", true);
                count = 0;
                onFreeWalk = true;
                Vector3 newDestiny = positionFocus;
                newDestiny.x = Random.Range(positionFocus.x - 10, positionFocus.x + 10);
                newDestiny.z = Random.Range(positionFocus.z - 10, positionFocus.z + 10);
                agent.destination = newDestiny;
            }

            if (onFreeWalk && count >= 9)
            {
                Vector3 newDestiny = positionFocus;
                newDestiny.x = Random.Range(newDestiny.x - 10, newDestiny.x + 10);
                newDestiny.z = Random.Range(newDestiny.z - 10, newDestiny.z + 10);
                agent.destination = newDestiny;
                count = 0;
            }

            if (onFreeWalk)
            {
                if (Vector3.Distance(transform.position, agent.destination) < 1)
                    enemyAnim.SetBool("Run", false);
                else
                    enemyAnim.SetBool("Run", true);
            }
        }

        #endregion

        if (life <= 0)
        {
            GetComponent<Rigidbody>().isKinematic = true;
            GetComponent<CapsuleCollider>().enabled = false;
            enemyAnim.SetBool("Death", true);
            return;
        }

        #region Player normal Detection 

        if (distancePlayer < normalDistance && !PlayerInfo.Instance.imScreaming || distancePlayer < distanceOnScreaming && PlayerInfo.Instance.imScreaming)
        {

            if (life > 0 && !attack || EnemyGeneral.Instance.playerOnView && !attack)
            {

                if (!EnemyGeneral.Instance.playerOnView)
                {

                    if (PlayerInfo.Instance.imHidden && EnemyGeneral.Instance.foundInHide)
                    {
                        EnemyGeneral.Instance.playerOnView = true;
                    }

                    if(!playerOnView)
                        playerOnView = CheckView();

                    if (playerOnView)
                    {
                        if (!alertSinal.activeSelf)
                            alertSinal.SetActive(true);
                    }

                    SearchForPlayer();

                    if (timeToViewPlayer >= 10)
                    {
                        if (!alertSinal.activeSelf)
                            alertSinal.SetActive(true);
                        EnemyGeneral.Instance.playerOnView = true;
                    }
                }

                if (EnemyGeneral.Instance.playerOnView)
                {

                    transform.LookAt(PlayerInfo.Instance.transform);

                    if (!alertSinal.activeSelf)
                        alertSinal.SetActive(true);

                    countToView = 0;
                    onFreeWalk = false;
                    if (distancePlayer < distanceToAttack)
                    {
                        enemyAnim.SetTrigger("Attack");
                        count = 0;
                        attack = true;
                        agent.destination = transform.position;
                        return;
                    }
                    else
                    {
                        destiny_Ref.transform.position = destiny.transform.position;
                        agent.destination = destiny_Ref.transform.position;
                    }
                    enemyAnim.SetBool("Run", true);
                }
            }
        }

        if ((distancePlayer > normalDistance && !PlayerInfo.Instance.imScreaming && iamMoreClose || distancePlayer > distanceOnScreaming && PlayerInfo.Instance.imScreaming) 
        && iamMoreClose && EnemyGeneral.Instance.playerOnView )
        {
            EnemyGeneral.Instance.playerOnView = false;
            playerOnView = false;
            alertSinal.SetActive(false);
            destiny_Ref.transform.position = destiny_Ref.transform.position;
            agent.destination = destiny_Ref.transform.position;
        }

        count += Time.deltaTime ;

        if (countToView > 0 && Controller.Instance.playerState == PlayerState.IDLE)
        {
            if (Controller.Instance.crouch)
            {
                countToView -= Time.deltaTime * velocityReduceAttemption;
                countToView = Mathf.Clamp(countToView, 0, 10);
            }
            else
            {
                countToView -= Time.deltaTime * ( velocityReduceAttemption / 2 );
                countToView = Mathf.Clamp(countToView, 0, 10);
            }
        }

        #endregion

        if (onFire)
            countFire += Time.deltaTime;

    }

    public void SearchForPlayer()
    {
        if (PlayerInfo.Instance.imScreaming)
        {
            countToView += Time.deltaTime * 4;
            countToView = Mathf.Clamp(countToView, 0, 10);
            return;
        }
        else
        {
            if (Controller.Instance.crouch)
            {
                if(Controller.Instance.playerState == PlayerState.WALK)
                {
                    if (PlayerInfo.Instance.imHidden && !EnemyGeneral.Instance.foundInHide)
                    {
                        countToView += Time.deltaTime + 0.2f;
                    }
                    else if (!PlayerInfo.Instance.imHidden || EnemyGeneral.Instance.foundInHide)
                    {
                        countToView += Time.deltaTime + 0.4f;
                    }
                    return;
                }
            }
            else if (Controller.Instance.playerState == PlayerState.WALK)
            {
                if (PlayerInfo.Instance.imHidden && !EnemyGeneral.Instance.foundInHide)
                {
                    countOnView += Time.deltaTime + 0.2f;
                }
                else if (!PlayerInfo.Instance.imHidden || EnemyGeneral.Instance.foundInHide)
                {
                    countToView += Time.deltaTime + 2.5f;
                }
                return;
            }
            if(distancePlayer < normalDistance && playerOnView)
            {
                if (PlayerInfo.Instance.imHidden && !EnemyGeneral.Instance.foundInHide)
                {
                    countOnView += Time.deltaTime + 0.4f;
                }
                else if(!PlayerInfo.Instance.imHidden || EnemyGeneral.Instance.foundInHide)
                {
                    if (distancePlayer < 6)
                        countOnView += Time.deltaTime + 1.5f;
                }
            }
        }
    }

    public bool CheckView()
    {
        RaycastHit hit;

        if (Physics.Raycast(rayOriginView.position, (rayOriginView.position - direction.position) * -1, out hit, isPlayer))
        {
            return true;
        }
        else
        {
            print("Collidiu nada");
            return false;
        }
    }

    public void hitByExplosion(float dmgExplosion, float dmg, float timeToHit, float duration)
    {
        life -= dmgExplosion;
        if (life < 0)
        {
            WhenDie.Invoke();
        }
        else
        {
            dmgOnFire = dmg;
            timePerHit = timeToHit;
            fireDuration = duration;
            if (!onFire)
            {
                countFire = 0;
                onFire = true;
                fireControll = Instantiate(onFirePrefab);
            }
        }
        
    }

    public void hitByFire(float dmg, float timeToHit, float duration)
    {
        life -= dmg;
        if (life < 0)
        {
            WhenDie.Invoke();
        }
        else
        {
            dmgOnFire = dmg;
            timePerHit = timeToHit;
            fireDuration = duration;
            if (!onFire)
            {
                countFire = 0;
                onFire = true;
                fireControll = Instantiate(onFirePrefab);
            }
        }
    }

    public void SetDistraction(Vector3 point)
    {
        if (!EnemyGeneral.Instance.playerOnView)
        {
            distractionPoint = point;
            inDistraction = true;
        }
    }

    public void HitByArrow(float dmg)
    {
        life -= dmg;
        if (life < 0)
        {
            WhenDie.Invoke();
        }
        else
        {
            OnHit.Invoke();
        }
    }

    public void HitByShotgun(float dmg)
    {
        life -= dmg;
        if (life < 0)
        {
            WhenDie.Invoke();
        }
        else
        {
            OnHit.Invoke();
        }
    }

    public void HittedSW(float dmg)
    {
        life -= dmg;
        if (life < 0)
        {
            WhenDie.Invoke();               
        }
        else
        {
            OnHit.Invoke();
        }
    }

    public void MoreCloseEnemy(bool value)
    {
        iamMoreClose = value;
    }

}
