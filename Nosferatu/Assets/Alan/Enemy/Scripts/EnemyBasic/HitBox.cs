﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HitBox : MonoBehaviour
{
    
    public BoxCollider hitBoxObj;
    public UnityEvent onHit = new UnityEvent();

    float hitTimer;
    bool hitBoxControll = false;

    public bool bossHitBox;

    internal bool isBoss;

    private void Start()
    {

        hitBoxObj.enabled = false;

        onHit.AddListener(DisableHitBox);
        if (!bossHitBox)
        {
            onHit.AddListener(() => { PlayerInfo.Instance.Hitted(gameObject.GetComponent<Nosferatu_Basic>().dmgOnHit); });
        }
        else
        {
            onHit.AddListener(() => { PlayerInfo.Instance.Hitted(gameObject.GetComponent<Boss>().dmgMeleeAtk); });
        }
    }

    private void Update()
    {
        if (hitBoxControll)
        {
            hitTimer += Time.deltaTime;
            if(hitTimer > 1)
            {
                hitTimer = 0;
                hitBoxControll = false;
                hitBoxObj.enabled = false;
            }
        }
    }

    public void Hitted()
    {
        onHit.Invoke();
    }

    public void EnableHitBox()
    {
        if (isBoss && Boss.Instance != null)
        {
            if (Boss.Instance.life > (Boss.Instance.initialLife * 25) / 100)
            {
                GameObject wave = Instantiate(Boss.Instance.wave, Boss.Instance.spawnWaves);
            }
            else
            {
                GameObject wave = Instantiate(Boss.Instance.wavePlus, Boss.Instance.spawnWaves);
            }
            return;
        }
        hitBoxControll = true;
        hitTimer = 0;
        hitBoxObj.enabled = true;
    }

    public void DisableHitBox()
    {
        if(Boss.Instance != null)
            Boss.Instance.onAttack = false;
        isBoss = false;
        if (isBoss) return;
        hitBoxControll = false;
        hitTimer = 0;
        hitBoxObj.enabled = false;
    }

}
