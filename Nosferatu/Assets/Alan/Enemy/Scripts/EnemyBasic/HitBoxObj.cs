﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBoxObj : MonoBehaviour
{

    public GameObject enemyControll;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && gameObject.GetComponent<BoxCollider>().enabled)
        {
            enemyControll.SendMessage("Hitted");
        }
    }

}
