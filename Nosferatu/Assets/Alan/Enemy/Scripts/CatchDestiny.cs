﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatchDestiny : MonoBehaviour
{

    public LayerMask isBlocker;

    public Transform[] point;

    public Transform destiny;

    bool[] pointAvaible;

    // Start is called before the first frame update
    void Start()
    {
        pointAvaible = new bool[point.Length];
    }

    // Update is called once per frame
    void Update()
    {
        
        for(int i = 0; i < point.Length; i++)
        {
            RaycastHit hit;
            if (Physics.Raycast(point[i].transform.position, transform.position, out hit, isBlocker))
            {
                pointAvaible[i] = false;
            }
            else
            {
                pointAvaible[i] = true;
            }
        }

        Transform proximity;
        float distance = 0;

        for (int i = 0; i < point.Length; i++)
        {
            if (pointAvaible[i])
            {
                if(Vector3.Distance(transform.position, point[i].transform.position) > distance)
                {
                    proximity = point[i].transform;
                    distance = Vector3.Distance(transform.position, point[i].transform.position);
                }
            }
        }
    }
}
