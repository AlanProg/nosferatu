﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wave : MonoBehaviour
{

    public float velocity;
    public float dmg;
    public float timeLife;

    public float velocityToGrowUp;

    bool hittedPlayer;

    float count;

    private void Start()
    {
        transform.SetParent(null);
    }

    // Update is called once per frame
    void Update()
    {

        if(count >= timeLife)
        {
            Destroy(this.gameObject);
        }
        transform.Translate(Vector3.forward * velocity * Time.deltaTime);
        count += Time.deltaTime;
    }

    private void LateUpdate()
    {
        CapsuleCollider newScale = GetComponent<CapsuleCollider>();
        newScale.height += Time.deltaTime * velocityToGrowUp;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && !hittedPlayer)
        {
            PlayerInfo.Instance.Hitted(dmg);
            hittedPlayer = true;
        }
        if(other.tag == "Tower")
        {
            other.SendMessage("WaveHit");
        }
    }

}
