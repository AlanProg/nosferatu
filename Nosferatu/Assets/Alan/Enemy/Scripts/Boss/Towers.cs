﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Towers : MonoBehaviour
{

    public LayerMask isPlayer;

    public float dmg;
    public float rangeRadius;

    public float timeToUpCol;
    public float timeTurnOff;
    public float offWaveTime;

    public GameObject statueBlood_On, blood_Explode, statueBlood_Off, blood_Respawn, statue_Explosion;
    public GameObject TpStart, TpStartEnd, TpEnd;

    public UnityEvent onBloodOrbHit = new UnityEvent();
    public UnityEvent reativeStatue = new UnityEvent();
    public UnityEvent attackEvent = new UnityEvent();
    public UnityEvent disableEvent = new UnityEvent();
    public UnityEvent enableEvent = new UnityEvent();

    bool fullDisable = false;

    bool isEnable = true;

    bool onAttack = false;

    float count;
    float countEnable;



    private void Start()
    {
        onBloodOrbHit.AddListener(() => 
        {
            statueBlood_On.SetActive(false);
            blood_Explode.SetActive(true);
            statueBlood_Off.SetActive(true);
            isEnable = false;
        });

        reativeStatue.AddListener(() =>
        {
            statueBlood_Off.SetActive(false);
            statueBlood_On.SetActive(true);
            blood_Respawn.SetActive(true);
            isEnable = true;
        });

        attackEvent.AddListener(() =>
        {
            Collider[] col = Physics.OverlapSphere(transform.position, rangeRadius, isPlayer);
            foreach(Collider cols in col)
            {
                if(cols.tag == "Player")
                {
                    PlayerInfo.Instance.Hitted(dmg);
                }
            }
            statue_Explosion.SetActive(true);
        });

        disableEvent.AddListener(() =>
        {
            TpEnd.SetActive(true);
            statueBlood_On.SetActive(false);
            fullDisable = true;
            Invoke("OFF", 1);
        });

        enableEvent.AddListener(() =>
        {
            reativeStatue.Invoke();
            fullDisable = true;
        });

    }

    void OFF()
    {
        gameObject.SetActive(false);
    }

    public void Update()
    {

        count += Time.deltaTime;
        if(!isEnable)
            count += Time.deltaTime;

        if (onAttack)
        {
            if (count > timeTurnOff)
            {
                onAttack = false;
            }
        }

        if(countEnable >= offWaveTime && !isEnable)
        {
            reativeStatue.Invoke();
        }
    }

    public void WaveHit()
    {
        if (isEnable && !onAttack)
        {
            count = 0;
            onAttack = true;
            attackEvent.Invoke();
        }
    }

    public void TurnOFFOrb()
    {
        onBloodOrbHit.Invoke();
    }

    public void DisableTower ()
    {
        TpStart.SetActive(true);
        StartCoroutine("DisableEnum");
    }

    public void EnableTower()
    {
        enableEvent.Invoke();
    }

    IEnumerator DisableEnum()
    {
        yield return new WaitForSeconds(1);
        TpStartEnd.SetActive(true);
        yield return new WaitForSeconds(1);
        disableEvent.Invoke();
    }

}
