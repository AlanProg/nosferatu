﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Boss : MonoBehaviour
{

    public static Boss Instance;

    public float life;
    public GameObject heartBox;
    public float dmgMeleeAtk;
    public float dmgBullet;
    public float timePerAct;
    public float dmgReceivedToTeleport;

    public Transform[] teleportPoints;

    public GameObject wave;
    public GameObject wavePlus;
    public Transform spawnWaves;

    public GameObject fire;
    public Transform spawnFire;

    public GameObject towerConjunt_One;
    public GameObject towerConjunt_Two;

    [Header("Evento chamado quando Teletransportar")]
    public UnityEvent TeleportEvent;
    [Header("Evento chamado quando receber dano")]
    public UnityEvent OnHit;
    [Header("Evento chamado quando morrer")]
    public UnityEvent WhenDie;

    [HideInInspector]public float initialLife;

    bool isVunerable;

    int indice;

    GameObject[] towersOne;
    GameObject[] towersTwo;

    [HideInInspector]public bool onAttack;

    bool onFire;
    bool doTeleport;
    float count;
    float countToTeleport;
    float countHeartBox;

    Animator enemyAnim;

    float dmgReceived;

    bool hardMod;

    bool conjuntControll;

    // Start is called before the first frame update
    void Start()
    {
        if (towerConjunt_One != null)
        {
            towersOne = new GameObject[towerConjunt_One.transform.childCount];

            int children = towerConjunt_One.transform.childCount;
            for (int i = 0; i < children; ++i)
                towersOne[i] = towerConjunt_One.transform.GetChild(i).gameObject;
        }
        if(towerConjunt_Two != null)
        {
            towersTwo = new GameObject[towerConjunt_Two.transform.childCount];

            int children = towerConjunt_Two.transform.childCount;
            for (int i = 0; i < children; ++i)
                towersTwo[i] = towerConjunt_Two.transform.GetChild(i).gameObject;
        }

        Instance = this;
        enemyAnim = gameObject.GetComponent<Animator>();
        initialLife = life;


        TeleportEvent.AddListener(() =>
        {
            if (conjuntControll)
            {
                for(int i = 0; i < towersOne.Length; i++)
                {
                    towersOne[i].SendMessage("DisableTower");
                }
                for (int i = 0; i < towersTwo.Length; i++)
                {
                    towersTwo[i].SetActive(true);
                    towersTwo[i].SendMessage("EnableTower");
                }
            }
            else
            {
                for (int i = 0; i < towersTwo.Length; i++)
                {
                    towersTwo[i].SendMessage("DisableTower");
                }
                for (int i = 0; i < towersOne.Length; i++)
                {
                    towersOne[i].SetActive(true);
                    towersOne[i].SendMessage("EnableTower");
                }
            }
        });

        for (int i = 0; i < towersOne.Length; i++)
        {
            towersOne[i].SetActive(true);
            towersOne[i].SendMessage("EnableTower");
        }

        for (int i = 0; i < towersTwo.Length; i++)
        {
            towersTwo[i].SetActive(true);
            towersTwo[i].SendMessage("DisableTower");
        }

    }

    // Update is called once per frame
    void Update()
    {

        if (life <= 0)
        {
            GetComponent<Rigidbody>().isKinematic = true;
            GetComponent<CapsuleCollider>().enabled = false;
            enemyAnim.SetBool("Death", true);
            this.enabled = false;
            return;
        }

        if(life < initialLife / 2 && !hardMod)
        {
            hardMod = true;
            timePerAct = timePerAct / 2;
        }

        Vector3 destiny = PlayerInfo.Instance.transform.position;
        destiny.y = transform.position.y;
        transform.LookAt(destiny);

        countHeartBox += Time.deltaTime;
        if(!onAttack || onFire) count += Time.deltaTime;
        countToTeleport += Time.deltaTime;
        indice = (int)Random.Range(0, teleportPoints.Length - 1);

        if(countToTeleport >= 2 && doTeleport)
        {
            MakeTeleport();
        }

        if(dmgReceived >= dmgReceivedToTeleport && !doTeleport)
        {
            doTeleport = true;
            isVunerable = false;
            dmgReceived = 0;
            countToTeleport = 0;
            onAttack = true;
        }

        if (!isVunerable && !onAttack)
        {
            if(countHeartBox >= 6)
            {
                isVunerable = true;
            }
        }

        if (isVunerable)
        {
            heartBox.SetActive(true);
        }
        else
        {
            heartBox.SetActive(false);
        }

        if(onFire && count >= 5)
        {
            onFire = false;
            onAttack = false;
        }

        if(count >= timePerAct && !onAttack)
        {
            NextAct();
        }

    }

    void NextAct()
    {

        float dist;
        count = 0;
        dist = Vector3.Distance(PlayerInfo.Instance.transform.position, transform.position);

        if(dist <= 2)
        {
            enemyAnim.SetTrigger("Attack");
            onAttack = true;
            return;
        }

        if (dist > 2)
        {

            int chance = Random.Range(0, 50);
            if(chance % 2 == 0)
            {
                enemyAnim.SetTrigger("Attack");
                onAttack = true;
                gameObject.GetComponent<HitBox>().isBoss = true;
                return;
            }
            else
            {
                GameObject fire = Instantiate(this.fire, spawnFire);
                onFire = true;
                onAttack = true;
            }
            
        }

    }

    public void MakeTeleport()
    {
        TeleportEvent.Invoke();
        isVunerable = true;
        onAttack = false;
        doTeleport = false;
        transform.position = teleportPoints[indice].transform.position;
    }

    public void hitByArrow(float dmg)
    {
        life -= dmg;
        dmgReceived += dmg;
        if (life < 0)
        {
            WhenDie.Invoke();
        }
        else
        {
            OnHit.Invoke();
        }
    }

    public void hitByShotgun(float dmg)
    {
        life -= dmg;
        dmgReceived += dmg;
        if (life < 0)
        {
            WhenDie.Invoke();
        }
        else
        {
            OnHit.Invoke();
        }
    }

    public void HittedSW(float dmg)
    {
        life -= dmg;
        dmgReceived += dmg;
        if (life < 0)
        {
            WhenDie.Invoke();
        }
        else
        {
            OnHit.Invoke();
        }
    }

}
