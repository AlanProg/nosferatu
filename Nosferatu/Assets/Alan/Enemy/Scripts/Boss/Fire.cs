﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{

    public float bulletSpeed;

    private void Start()
    {
        transform.SetParent(null);
    }
    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * bulletSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            PlayerInfo.Instance.Hitted(Boss.Instance.dmgBullet);
        }
        Destroy(gameObject);
    }

}
