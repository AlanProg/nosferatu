﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : MonoBehaviour
{
    
    public void OnHit(float dmg)
    {
        Boss.Instance.life -= dmg;
        Boss.Instance.OnHit.Invoke();
    }

}
