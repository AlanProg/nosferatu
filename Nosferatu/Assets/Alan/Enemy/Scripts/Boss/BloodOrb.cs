﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodOrb : MonoBehaviour
{

    public GameObject father;

    public void TurnOFF()
    {
        father.SendMessage("TurnOFFOrb");
    }

    public void WaveHit()
    {
        father.SendMessage("WaveHit");
    }

}
