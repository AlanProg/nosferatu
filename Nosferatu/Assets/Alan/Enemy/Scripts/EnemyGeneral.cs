﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGeneral : MonoBehaviour
{

    public static EnemyGeneral Instance;

    public bool playerOnView; // se o player está no raio de visão
    public Transform playerPosition; // transform do player
    public List<GameObject> enemysReference = new List<GameObject>(); // todos os inimigos na cena

    GameObject moreClose = null; // inimigo mais próximo do player

    [HideInInspector] public bool foundInHide;

    void Awake()
    {
        Instance = this;
    }
    
    void Update()
    {

        if(CatchMoreClose() != moreClose)
        {
            if (moreClose != null)
            {
                moreClose.GetComponent<Nosferatu_Basic>().MoreCloseEnemy(false);
            }
            moreClose = CatchMoreClose();
            moreClose.GetComponent<Nosferatu_Basic>().MoreCloseEnemy(true);
        }


    }

    public GameObject CatchMoreClose()
    {
        GameObject more = enemysReference[0];

        for(int i = 1; i < enemysReference.Count; i++)
        {
            if (Vector3.Distance(enemysReference[i].transform.position, playerPosition.position) 
                < Vector3.Distance(more.transform.position, playerPosition.position))
            {
                more = enemysReference[i];
            }
        }

        return more;

    }

}
