%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: ATK MASK
  m_Mask: 00000000010000000100000000000000000000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Andrei:ANDREI_geo
    m_Weight: 0
  - m_Path: Andrei:rig_grp
    m_Weight: 0
  - m_Path: Andrei:rig_grp/Andrei:driver_root
    m_Weight: 1
  - m_Path: Andrei:rig_grp/Andrei:master_anim_space_switcher_follow
    m_Weight: 1
  - m_Path: Andrei:rig_grp/Andrei:master_anim_space_switcher_follow/Andrei:master_anim_space_switcher
    m_Weight: 1
  - m_Path: Andrei:rig_grp/Andrei:master_anim_space_switcher_follow/Andrei:master_anim_space_switcher/Andrei:master_anim
    m_Weight: 1
  - m_Path: Andrei:rig_grp/Andrei:master_anim_space_switcher_follow/Andrei:master_anim_space_switcher/Andrei:master_anim/Andrei:root_anim
    m_Weight: 1
  - m_Path: Andrei:root
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_l/Andrei:upperarm_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_l/Andrei:upperarm_l/Andrei:lowerarm_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_l/Andrei:upperarm_l/Andrei:lowerarm_l/Andrei:hand_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_l/Andrei:upperarm_l/Andrei:lowerarm_l/Andrei:hand_l/Andrei:index_01_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_l/Andrei:upperarm_l/Andrei:lowerarm_l/Andrei:hand_l/Andrei:index_01_l/Andrei:index_02_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_l/Andrei:upperarm_l/Andrei:lowerarm_l/Andrei:hand_l/Andrei:index_01_l/Andrei:index_02_l/Andrei:index_03_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_l/Andrei:upperarm_l/Andrei:lowerarm_l/Andrei:hand_l/Andrei:middle_01_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_l/Andrei:upperarm_l/Andrei:lowerarm_l/Andrei:hand_l/Andrei:middle_01_l/Andrei:middle_02_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_l/Andrei:upperarm_l/Andrei:lowerarm_l/Andrei:hand_l/Andrei:middle_01_l/Andrei:middle_02_l/Andrei:middle_03_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_l/Andrei:upperarm_l/Andrei:lowerarm_l/Andrei:hand_l/Andrei:pinky_01_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_l/Andrei:upperarm_l/Andrei:lowerarm_l/Andrei:hand_l/Andrei:pinky_01_l/Andrei:pinky_02_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_l/Andrei:upperarm_l/Andrei:lowerarm_l/Andrei:hand_l/Andrei:pinky_01_l/Andrei:pinky_02_l/Andrei:pinky_03_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_l/Andrei:upperarm_l/Andrei:lowerarm_l/Andrei:hand_l/Andrei:ring_01_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_l/Andrei:upperarm_l/Andrei:lowerarm_l/Andrei:hand_l/Andrei:ring_01_l/Andrei:ring_02_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_l/Andrei:upperarm_l/Andrei:lowerarm_l/Andrei:hand_l/Andrei:ring_01_l/Andrei:ring_02_l/Andrei:ring_03_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_l/Andrei:upperarm_l/Andrei:lowerarm_l/Andrei:hand_l/Andrei:thumb_01_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_l/Andrei:upperarm_l/Andrei:lowerarm_l/Andrei:hand_l/Andrei:thumb_01_l/Andrei:thumb_02_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_l/Andrei:upperarm_l/Andrei:lowerarm_l/Andrei:hand_l/Andrei:thumb_01_l/Andrei:thumb_02_l/Andrei:thumb_03_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_l/Andrei:upperarm_l/Andrei:lowerarm_l/Andrei:lowerarm_twist_01_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_l/Andrei:upperarm_l/Andrei:upperarm_twist_01_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_r/Andrei:upperarm_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_r/Andrei:upperarm_r/Andrei:lowerarm_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_r/Andrei:upperarm_r/Andrei:lowerarm_r/Andrei:hand_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_r/Andrei:upperarm_r/Andrei:lowerarm_r/Andrei:hand_r/Andrei:index_01_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_r/Andrei:upperarm_r/Andrei:lowerarm_r/Andrei:hand_r/Andrei:index_01_r/Andrei:index_02_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_r/Andrei:upperarm_r/Andrei:lowerarm_r/Andrei:hand_r/Andrei:index_01_r/Andrei:index_02_r/Andrei:index_03_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_r/Andrei:upperarm_r/Andrei:lowerarm_r/Andrei:hand_r/Andrei:middle_01_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_r/Andrei:upperarm_r/Andrei:lowerarm_r/Andrei:hand_r/Andrei:middle_01_r/Andrei:middle_02_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_r/Andrei:upperarm_r/Andrei:lowerarm_r/Andrei:hand_r/Andrei:middle_01_r/Andrei:middle_02_r/Andrei:middle_03_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_r/Andrei:upperarm_r/Andrei:lowerarm_r/Andrei:hand_r/Andrei:pinky_01_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_r/Andrei:upperarm_r/Andrei:lowerarm_r/Andrei:hand_r/Andrei:pinky_01_r/Andrei:pinky_02_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_r/Andrei:upperarm_r/Andrei:lowerarm_r/Andrei:hand_r/Andrei:pinky_01_r/Andrei:pinky_02_r/Andrei:pinky_03_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_r/Andrei:upperarm_r/Andrei:lowerarm_r/Andrei:hand_r/Andrei:ring_01_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_r/Andrei:upperarm_r/Andrei:lowerarm_r/Andrei:hand_r/Andrei:ring_01_r/Andrei:ring_02_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_r/Andrei:upperarm_r/Andrei:lowerarm_r/Andrei:hand_r/Andrei:ring_01_r/Andrei:ring_02_r/Andrei:ring_03_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_r/Andrei:upperarm_r/Andrei:lowerarm_r/Andrei:hand_r/Andrei:thumb_01_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_r/Andrei:upperarm_r/Andrei:lowerarm_r/Andrei:hand_r/Andrei:thumb_01_r/Andrei:thumb_02_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_r/Andrei:upperarm_r/Andrei:lowerarm_r/Andrei:hand_r/Andrei:thumb_01_r/Andrei:thumb_02_r/Andrei:thumb_03_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_r/Andrei:upperarm_r/Andrei:lowerarm_r/Andrei:lowerarm_twist_01_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:clavicle_r/Andrei:upperarm_r/Andrei:upperarm_twist_01_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:neck_01
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:neck_01/Andrei:neck_02
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:spine_01/Andrei:spine_02/Andrei:spine_03/Andrei:neck_01/Andrei:neck_02/Andrei:head
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:thigh_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:thigh_l/Andrei:calf_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:thigh_l/Andrei:calf_l/Andrei:calf_twist_01_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:thigh_l/Andrei:calf_l/Andrei:foot_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:thigh_l/Andrei:calf_l/Andrei:foot_l/Andrei:ball_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:thigh_l/Andrei:thigh_twist_01_l
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:thigh_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:thigh_r/Andrei:calf_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:thigh_r/Andrei:calf_r/Andrei:calf_twist_01_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:thigh_r/Andrei:calf_r/Andrei:foot_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:thigh_r/Andrei:calf_r/Andrei:foot_r/Andrei:ball_r
    m_Weight: 1
  - m_Path: Andrei:root/Andrei:pelvis/Andrei:thigh_r/Andrei:thigh_twist_01_r
    m_Weight: 1
